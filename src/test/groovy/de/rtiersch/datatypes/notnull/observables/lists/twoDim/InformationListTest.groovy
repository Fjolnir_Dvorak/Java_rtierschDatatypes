/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.DefICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICreateICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderInit
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderManipulation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.init.SerializedDataKt
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IFireListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.AInfoRow
import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.exception.ArrayNotRectangularException
import spock.lang.Specification

// DO NOT USE ANNOTATIONS! TYPE_USE is not supported. Never trust Groovy!

class InformationListTest extends Specification {
    static final EOrientation orientation = EOrientation.RowColumn

    static class Creator implements ICreateICell<String>,
        de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.ICreateAInfoColumn<String>,
        de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.ICreateAInfoRow<String> {

        @Override
        ICellSet<String> constructCell(
            final de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead<String> p_rowInformation,
            final de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead<String> p_columnInformation,
            final IFireListenerCellChanged<String> p_listener,
            final String p_element) {
            return new DefICell<String>(p_rowInformation,
                p_columnInformation, p_listener, p_element)
        }

        @Override
        ICellSet<String> copyCell(
            final de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead<String> p_rowInformation,
            final de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead<String> p_columnInformation,
            final ICell<String> p_element) {
            return p_element.copy(p_rowInformation, p_columnInformation)
        }

        @Override
        AInfoRow constructInfoRow(final IIntegerRead p_row) {
            return new AInfoRow<String>(p_row)
        }

        @Override
        de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.AInfoColumn constructInfoColumn(
            final IIntegerRead p_column) {
            return new de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.AInfoColumn<String>(p_column)
        }
    }
    static final creator = new Creator()

    void setup() {
    }

    void cleanup() {
    }

    def "Initilization and recreation of array."() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]

        when:
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None, EHeaderInit
            .None, EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(matrix, serializedTable.table)
        serializedTable.headerColumns.equals(SerializedDataKt
            .NO_HEADER)
        serializedTable.headerRows.equals(SerializedDataKt
            .NO_HEADER)
    }

    def "Constructor List contains null values."() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", null, "6"],
                             ["7", "8", "9"]]
        when:
        new InformationList<String>(matrix, orientation, creator,
            creator, creator, EHeaderInit.None, EHeaderInit.None, EHeaderManipulation.CustomHeader)

        then:
        thrown(NullPointerException)
    }

    def "Constructor Orientation of input array"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]

        String[][] matrixMirror = [["1", "4", "7"],
                                   ["2", "5", "8"],
                                   ["3", "6", "9"]]
        def mirroredOrientation = EOrientation.ColumnRow

        when:
        def table = new InformationList<String>(matrixMirror,
            mirroredOrientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.None, EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(matrix, serializedTable.table)
    }

    def "Constructor Unbalanced Array"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8"]]

        when:
        new InformationList<String>(matrix, orientation, creator,
            creator, creator, EHeaderInit.None, EHeaderInit.None, EHeaderManipulation.CustomHeader)

        then:
        thrown(ArrayNotRectangularException)
    }

    def "First row is header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["4", "5", "6"],
                                 ["7", "8", "9"]]
        String[] goalHeader = ["1", "2", "3"]

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .FirstElems, EHeaderInit.None, EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.equals(goalHeader, serializedTable.headerColumns)
    }

    def "First column is header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["2", "3"],
                                 ["5", "6"],
                                 ["8", "9"]]
        String[] goalHeader = ["1", "4", "7"]

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.FirstElems, EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.equals(goalHeader, serializedTable.headerRows)
    }

    def "First row and column are header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["5", "6"],
                                 ["8", "9"]]
        String[] goalHeaderRows = ["4", "7"]
        String[] goalHeaderColumns = ["2", "3"]

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .FirstElems, EHeaderInit.FirstElems, EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.equals(goalHeaderRows, serializedTable.headerRows)
        Arrays.equals(goalHeaderColumns, serializedTable.headerColumns)
    }

    def "Add a column at the end"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3", "A"],
                                 ["4", "5", "6", "B"],
                                 ["7", "8", "9", "C"]]
        String[] toAdd = ["A", "B", "C"]
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.addColumn(toAdd, "")
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
    }

    def "Add a column in between"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "A", "3"],
                                 ["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] toAdd = ["A", "B", "C"]
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.None, EHeaderManipulation.CustomHeader)
        def col0 = table.getColumn(0).getIndex()
        def col1 = table.getColumn(1).getIndex()
        def col2 = table.getColumn(2).getIndex()

        when:
        table.addColumn(toAdd, 2, "")
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        col0.getInt() == 0
        col1.getInt() == 1
        table.getColumn(2).getIndex().getInt() == 2
        col2.getInt() == 3
    }

    def "Add a column with header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[] goalHeader = ["1", "2", "A", "3"]
        String[][] goalMatrix = [["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] toAdd = ["B", "C"]
        String toAddHeader = "A"
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .FirstElems, EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.addColumn(toAdd, 2, toAddHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.equals(goalHeader, serializedTable.headerColumns)
    }

    def "Add a row at the end"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["4", "5", "6"],
                                 ["7", "8", "9"],
                                 ["A", "B", "C"]]
        String[] toAdd = ["A", "B", "C"]
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.addRow(toAdd, "")
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
    }

    def "Add a row in between"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["4", "5", "6"],
                                 ["A", "B", "C"],
                                 ["7", "8", "9"]]
        String[] toAdd = ["A", "B", "C"]
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.None, EHeaderManipulation.CustomHeader)
        def row0 = table.getRow(0).getIndex()
        def row1 = table.getRow(1).getIndex()
        def row2 = table.getRow(2).getIndex()

        when:
        table.addRow(toAdd, 2, "")
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        row0.getInt() == 0
        row1.getInt() == 1
        table.getRow(2).getIndex().getInt() == 2
        row2.getInt() == 3
    }

    def "Add a row with custom header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["2", "3"],
                                 ["5", "6"],
                                 ["B", "C"],
                                 ["8", "9"]]
        String[] goalHeader = ["1", "4", "A", "7"]
        String[] toAdd = ["B", "C"]
        String toAddHeader = "A"
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, EHeaderInit
            .None, EHeaderInit.FirstElems, EHeaderManipulation.CustomHeader)

        when:
        table.addRow(toAdd, 2, toAddHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerRows)
    }
    def "Create a table with generated column number-header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = matrix
        String[] goalHeader = ["1", "2", "3"]
        EHeaderInit headerType = EHeaderInit.NumericOne

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a table with generated column 0-number-header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = matrix
        String[] goalHeader = ["0", "1", "2"]
        EHeaderInit headerType = EHeaderInit.Numeric

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a table with generated column alphanum-header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = matrix
        String[] goalHeader = ["A", "B", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a table with generated column and row header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = matrix
        String[] goalHeader = ["A", "B", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric

        when:
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, headerType,
            EHeaderManipulation.CustomHeader)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
        Arrays.deepEquals(goalHeader, serializedTable.headerRows)
    }
    def "Create a new column with generated reassigning number-header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "A", "3"],
                                 ["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] column = ["A", "B", "C"]
        String[] goalHeader = ["1", "2", "3", "4"]
        EHeaderInit headerType = EHeaderInit.NumericOne
        EHeaderManipulation headerBH = EHeaderManipulation.Reassign
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.addColumn(column, 2, null)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a new column with generated reassigning alphanum-header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "A", "3"],
                                 ["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] column = ["A", "B", "C"]
        String[] goalHeader = ["A", "B", "C", "D"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.Reassign
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.addColumn(column, 2, null)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a new column with generated copy-marked header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "A", "3"],
                                 ["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] column = ["A", "B", "C"]
        String[] goalHeader = ["A", "B", "", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.MarkAsCopy
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.addColumn(column, 2, null)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Copy a column with generated copy-marked header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "2", "3"],
                                 ["4", "5", "5", "6"],
                                 ["7", "8", "8", "9"]]
        String[] goalHeader = ["A", "B", "B\u0000 (1)", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.MarkAsCopy
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.copyColumn(1, 2)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Create a new column with generated duplicating header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "A", "3"],
                                 ["4", "5", "B", "6"],
                                 ["7", "8", "C", "9"]]
        String[] column = ["A", "B", "C"]
        String[] goalHeader = ["A", "B", "", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.Duplicate
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.addColumn(column, 2, null)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Copy a column with generated duplicating header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "2", "3"],
                                 ["4", "5", "5", "6"],
                                 ["7", "8", "8", "9"]]
        String[] goalHeader = ["A", "B", "B", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.Duplicate
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.copyColumn(1, 2)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }
    def "Copy a column two times with generated copy-marked header"() {
        // TODO This does not make sense
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "2", "2", "3"],
                                 ["4", "5", "5", "5", "6"],
                                 ["7", "8", "8", "8", "9"]]
        String[] goalHeader = ["A", "B", "B\u0000 (1)", "B\u0000 (2)", "C"]
        EHeaderInit headerType = EHeaderInit.Alphanumeric
        EHeaderManipulation headerBH = EHeaderManipulation.MarkAsCopy
        def table = new InformationList<String>(matrix,
            orientation, creator, creator, creator, headerType, EHeaderInit.None,
            headerBH)

        when:
        table.copyColumn(1, 2)
        table.copyColumn(2, 3)

        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerColumns)
    }

    // TODO add a row/column with header where no header exists.
    // TODO test other header types
    // TODO test add for other header types.

    def "RemoveRow, the boring version"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["7", "8", "9"]]
        int row = 1
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None,
            EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.removeRow(row)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
    }
    def "RemoveColumn, the boring version"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "3"],
                                 ["4", "6"],
                                 ["7", "9"]]
        int row = 1
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None,
            EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.removeColumn(row)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
    }

    def "RemoveRow, with reassigning header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["7", "8", "9"]]
        String[] goalHeader = ["0", "1"]
        int row = 1
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None, EHeaderInit.Numeric,
            EHeaderManipulation.Reassign)

        when:
        table.removeRow(row)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerRows)
    }

    def "RemoveRow, with staying header"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["7", "8", "9"]]
        String[] goalHeader = ["0", "2"]
        int row = 1
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None, EHeaderInit.Numeric,
            EHeaderManipulation.CustomHeader)

        when:
        table.removeRow(row)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
        Arrays.deepEquals(goalHeader, serializedTable.headerRows)
    }

    def "Remove a non existing Row"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None,
            EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.removeRow(5)

        then:
        def serializedTable = table.serializeRawData()
        thrown(ArrayIndexOutOfBoundsException)
        Arrays.deepEquals(matrix, serializedTable.table)
    }

    def "SetColumnRow"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        int row = 1
        int column = 2
        String value = "A"
        String[][] goalMatrix = [["1", "2", "3"],
                                 ["4", "5", "A"],
                                 ["7", "8", "9"]]
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None,
            EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.setColumnRow(column, row, value)
        def serializedTable = table.serializeRawData()

        then:
        Arrays.deepEquals(goalMatrix, serializedTable.table)
    }
    def "SetColumnRow out of bounds"() {
        given:
        String[][] matrix = [["1", "2", "3"],
                             ["4", "5", "6"],
                             ["7", "8", "9"]]
        int row = 1
        int column = 7
        String value = "A"
        def table = new InformationList<String>(matrix, orientation,
            creator, creator, creator, EHeaderInit.None,
            EHeaderInit.None, EHeaderManipulation.CustomHeader)

        when:
        table.setColumnRow(column, row, value)

        then:
        def serializedTable = table.serializeRawData()
        thrown(IndexOutOfBoundsException)
        Arrays.deepEquals(matrix, serializedTable.table)
    }
}
