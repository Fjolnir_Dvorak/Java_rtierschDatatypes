/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.lists.array

import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import de.rtiersch.util.lists.ArrayUtils

/**
 * TODO Test this class.
 * Virtualized the indexing of a 2D array. The Array needs to be rectangular.
 *
 * @param matrix Array to virtualize. This array needs to be rectangular.
 * @param E Content type.
 *
 * @throws NullPointerException This type should only contain NotNull values.
 * Necessary for compatibility with Java.
 * @throws ArrayNotRectangularException All intern arrays should have the same length.
 */
@API
class VirtualArray2D<E>
@Throws(ArrayNotRectangularException::class,
    NullPointerException::class) constructor(private val matrix: Array<Array<E>>) {
    /**
     * Index wrapper for the first array-index.
     */
    private val indexFirst: IntArray
    /**
     * Index wrapper for the second array-index.
     */
    private val indexSecond: IntArray

    init {
        if (!ArrayUtils.isRectangular(matrix as Array<Array<Any>>)) {
            throw ArrayNotRectangularException()
        }
        if (ArrayUtils.testNull(matrix)) {
            throw NullPointerException("Array contains Nulls.")
        }
        val l_lengthA = matrix.size
        val l_lengthB = matrix[0].size
        this.indexFirst = IntArray(l_lengthA)
        this.indexSecond = IntArray(l_lengthB)

        for (l_i in this.indexFirst.indices) {
            this.indexFirst[l_i] = l_i
        }
        for (l_i in this.indexSecond.indices) {
            this.indexSecond[l_i] = l_i
        }
    }

    /**
     * Returns the element at mapped[first][second].
     *
     * @param first  First index.
     * @param second Second index.
     * @return element, if there is one.
     *
     * @throws IndexOutOfBoundsException Well. It is an Array. It has bounds.
     */
    @Throws(IndexOutOfBoundsException::class) operator fun get(first: Int,
                                                               second: Int): E {
        return this.matrix[this.indexFirst[first]][this.indexSecond[second]]
    }

    /**
     * Sets a new Value into the index shadowed array.
     *
     * @param first  First index.
     * @param second Second index.
     * @param element    New element.
     * @throws IndexOutOfBoundsException Array does not have that size.
     */
    @Throws(IndexOutOfBoundsException::class) operator fun set(first: Int,
                                                               second: Int,
                                                               element: E) {
        this.matrix[this.indexFirst[first]][this.indexSecond[second]] = element
    }

    /**
     * Moves the pointer p_currentPos to p_newPos from the first indice.
     *
     * @param p_currentPos Position pointer to be moved.
     * @param p_newPos     New position of pointer p_currentPos.
     * @throws IndexOutOfBoundsException Out of the bounds of first indice.
     */
    @API
    @Throws(IndexOutOfBoundsException::class)
    fun moveFirst(p_currentPos: Int,
                  p_newPos: Int) {
        VirtualArray2D.internalMove(p_currentPos, p_newPos, this.indexFirst)
    }

    /**
     * Moves the pointer p_currentPos to p_newPos from the second indice.
     *
     * @param p_currentPos Position pointer to be moved.
     * @param p_newPos     New position of pointer p_currentPos.
     * @throws IndexOutOfBoundsException Out of the bounds of second indice.
     */
    @API
    @Throws(IndexOutOfBoundsException::class)
    fun moveSecond(p_currentPos: Int,
                   p_newPos: Int) {
        VirtualArray2D.internalMove(p_currentPos, p_newPos, this.indexSecond)
    }

    /**
     * Returns the length of the first index.
     *
     * @return Length of first array parameter.
     */
    @API
    fun lengthFirst(): Int {
        return this.indexFirst.size
    }

    /**
     * Returns the length of the second index.
     *
     * @return Length of second array parameter.
     */
    @API
    fun lengthSecond(): Int {
        return this.indexSecond.size
    }

    companion object {

        /**
         * Moves the pointer currentPos to the position of newPos and shifts
         * all the values between those.
         *
         * @param currentPos Position pointer to be moved.
         * @param newPos     New position of pointer currentPos.
         * @param mapping    pointer array to manipulate.
         * @throws IndexOutOfBoundsException Out of the bounds of mapping.
         */
        @Throws(IndexOutOfBoundsException::class)
        fun internalMove(currentPos: Int,
                         newPos: Int,
                         mapping: IntArray) {
            if (currentPos == newPos) {
                return
            }
            if (currentPos < 0 || newPos < 0) {
                throw IndexOutOfBoundsException("Negative index.")
            }
            if (currentPos < newPos) {
                if (mapping.size < newPos) {
                    throw IndexOutOfBoundsException(
                        "New Position (" + newPos + ") is out of bounds (" + (mapping.size - 1) + ")")
                }
                // left shift and move
                /*
             * Save first element in P and overwrite with n+1.
             * Insert P in newPos
             */
                val l_p = mapping[currentPos]
                System.arraycopy(mapping, currentPos + 1, mapping, currentPos,
                    newPos - currentPos)
                mapping[newPos] = l_p
            } else {
                if (mapping.size < currentPos) {
                    throw IndexOutOfBoundsException()
                }
                // move and right shift
                /*
             * 1. Save current element in P.
             * 2. Insert currentPos into newPos.
             * 3. n starts at newPos + 1
             * 4. Save element from p into P' ans overwrite with P
             * 5. P' => P
             * 6. n+1
             * 7. continue at 4.
             */
                var l_p = mapping[newPos]
                mapping[newPos] = mapping[currentPos]
                for (l_i in newPos + 1..currentPos) {
                    val l_pt = mapping[l_i]
                    mapping[l_i] = l_p
                    l_p = l_pt
                }
            }
        }
    }
}
