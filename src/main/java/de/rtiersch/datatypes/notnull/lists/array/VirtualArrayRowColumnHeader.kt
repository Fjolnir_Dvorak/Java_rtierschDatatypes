/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.lists.array

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import de.rtiersch.util.lists.ArrayUtils
import de.rtiersch.util.lists.ICopy
import java.util.*

/**
 * TODO Test this class.
 * Index shadowed two dimensional rectified array extended by column and row
 * headers.
 *
 * @param headerLR Header from left to right. Header for columns.
 * @param headerTD Header from top down. Header for rows.
 * @param orientation Orientation of input array
 * @param inMatrix Input array.
 * @param E Type of Array.
 * @param H Type of Header.
 * @throws NullPointerException input array contains null values.
 */
@API
class VirtualArrayRowColumnHeader<E, H>
@Throws(ArrayNotRectangularException::class,
    NullPointerException::class) constructor(inMatrix: Array<Array<E>>,
                                             private var headerLR: Array<H>,
                                             private val headerTD: Array<H>,
                                             orientation: EOrientation) : VirtualArrayRowColumn<E>(
    inMatrix, orientation) {


    init {
        if (ArrayUtils.testNulls(headerLR, headerTD)) {
            throw NullPointerException("Input header contains null values")
        }
        if (super.amountColumns() != headerTD.size || super.amountRows() != headerLR.size) {
            throw ArrayNotRectangularException(
                "The table headers have a not valid size.")
        }
    }

    /**
     * Gets the header for a specific column.
     *
     * @param column Column index.
     * @return Header information.
     */
    @API
    fun getHeaderLR(column: Int): H {
        return this.headerLR[super.getMappingColumn(column)]
    }

    /**
     * Gets the header for a specific row.
     *
     * @param row Row index.
     * @return Header information.
     */
    @API
    fun getHeaderTD(row: Int): H {
        return this.headerTD[super.getMappingColumn(row)]
    }

    /**
     * Sets a new header information for that column.
     *
     * @param column Column index.
     * @param header New header.
     */
    @API
    fun setHeaderLR(column: Int,
                    header: H) {
        this.headerLR[super.getMappingColumn(column)] = header
    }

    /**
     * Sets a new header information for that row.
     *
     * @param row    Row index.
     * @param header New header.
     */
    @API
    fun setHeaderTD(row: Int,
                    header: H) {
        this.headerTD[super.getMappingColumn(row)] = header
    }

    override fun pExtendChildContentAfterCopy(indexStrgC: Int,
                                              indexStrgV: Int) {
        val l_newLength = this.headerLR.size + 1
        this.headerLR = Arrays.copyOf(this.headerLR, l_newLength)
        if (this.headerLR[0] is ICopy) {
            this.headerLR[l_newLength - 1] = (this.headerLR[indexStrgC] as ICopy).createCopy() as H
        } else {
            this.headerLR[l_newLength - 1] = this.headerLR[indexStrgC]
        }
    }
}
