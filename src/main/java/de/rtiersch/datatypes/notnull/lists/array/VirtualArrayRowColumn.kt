/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.lists.array

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import de.rtiersch.util.lists.ArrayUtils
import de.rtiersch.util.lists.ICopy
import java.util.*

/**
 * TODO Test this class.
 * Index shadowed two dimensional rectified array.
 * @param matrix  Array to virtualize. This array needs to be rectangular.
 * @param orientation How should this array be interpreted? Defines if the
 * first index is row or column.
 * @param E Type of array.
 * @throws NullPointerException This type should only contain NotNull values.
 * Necessary for compatibility with Java.
 * @throws ArrayNotRectangularException All intern arrays should have the same length.
 */
@API
open class VirtualArrayRowColumn<E>
@Throws(ArrayNotRectangularException::class,
        NullPointerException::class) constructor(matrix: Array<Array<E>>,
                                                 orientation: EOrientation) {
    /**
     * Index wrapper for the first array-index.
     */
    private val indexRow: IntArray
    /**
     * Content. [indexColumn][indexRow] or [which column][which row]
     */
    private var m_array: Array<Array<E>>
    /**
     * Index wrapper for the second array-index.
     */
    private var indexColumn: IntArray

    init {
        if (!ArrayUtils.isRectangular(matrix as Array<Array<Any>>)) {
            throw ArrayNotRectangularException()
        }
        if (ArrayUtils.testNull(matrix)) {
            throw NullPointerException("Input contains null values")
        }
        val l_array: Array<Array<E>> = when (orientation) {
            EOrientation.RowColumn -> ArrayUtils.changeXYtoYX(matrix) as Array<Array<E>>
            EOrientation.ColumnRow -> matrix
        }
        this.m_array = l_array
        val l_row = l_array[0].size
        val l_column = l_array.size
        this.indexColumn = IntArray(l_column)
        this.indexRow = IntArray(l_row)

        for (l_i in this.indexRow.indices) {
            this.indexRow[l_i] = l_i
        }
        for (l_i in this.indexColumn.indices) {
            this.indexColumn[l_i] = l_i
        }
    }

    /**
     * Returns the element at mapped[p_row][p_column].
     *
     * @param p_row    First index.
     * @param p_column Second index.
     * @return element, if there is one.
     *
     * @throws IndexOutOfBoundsException Well. It is an Array. It has bounds.
     */
    @Throws(IndexOutOfBoundsException::class) operator fun get(p_row: Int,
                                                               p_column: Int): E {
        return this.m_array[this.indexColumn[p_column]][this.indexRow[p_row]]
    }

    /**
     * Sets a new Value into the index shadowed array.
     *
     * @param p_row    First indice.
     * @param p_column Second indice.
     * @param p_new    New element.
     * @throws IndexOutOfBoundsException Array does not have that size.
     */
    @Throws(IndexOutOfBoundsException::class) operator fun set(p_row: Int,
                                                               p_column: Int,
                                                               p_new: E) {
        this.m_array[this.indexColumn[p_column]][this.indexRow[p_row]] = p_new
    }

    /**
     * Moves the pointer currentPos to newPos from the second indice.
     *
     * @param currentPos Position pointer to be moved.
     * @param newPos     New position of pointer currentPos.
     * @throws IndexOutOfBoundsException Out of the bounds of second indice.
     */
    @API
    @Throws(IndexOutOfBoundsException::class)
    fun moveRow(currentPos: Int,
                newPos: Int) {
        VirtualArray2D.internalMove(currentPos, newPos, this.indexRow)
    }

    /**
     * Returns the intern index for indexRow.
     *
     * @param index index to get mapping for.
     * @return mapped index.
     *
     * @throws ArrayIndexOutOfBoundsException Too high or negative index.
     */
    @API
    fun getMappingRow(index: Int): Int {
        return this.indexRow[index]
    }

    /**
     * Returns the intern index for indexColumn.
     *
     * @param index index to get mapping for.
     * @return mapped index.
     *
     * @throws ArrayIndexOutOfBoundsException Too high or negative index.
     */
    @API
    fun getMappingColumn(index: Int): Int {
        return this.indexColumn[index]
    }

    /**
     * Copy a column to another index. Soft copy if not extends (ICopy)
     *
     * @param indexStrgC Index of base column.
     * @param indexStrgV Index of new column.
     * @return false if the input indexes are not valid. Will do nothing.
     */
    @API
    fun copyColumn(indexStrgC: Int,
                   indexStrgV: Int): Boolean {
        // Input validation
        if (indexStrgC < 0 || indexStrgV < 0 || indexStrgC >= this.amountColumns() || indexStrgV > this.amountColumns()) {
            return false
        }
        // Input is valid.
        // Create copied column
        val l_newColumn = arrayOfNulls<Any>(this.amountRows()) as Array<E>
        if (this.m_array[0][0] is ICopy) {
            for (l_i in l_newColumn.indices) {
                l_newColumn[l_i] = (this.m_array[indexStrgC][l_i] as ICopy).createCopy() as E
            }
        } else {
            System.arraycopy(this.m_array[indexStrgC], 0, l_newColumn, 0,
                    l_newColumn.size)
        }

        // Create new Index arrays.
        val l_newAmountColumns = this.amountColumns() + 1
        val l_newIndexColumn = Arrays.copyOf(this.indexColumn,
                l_newAmountColumns)
        l_newIndexColumn[l_newAmountColumns - 1] = l_newAmountColumns - 1
        this.moveColumn(l_newAmountColumns - 1, indexStrgV)

        // Copy all Columns into the row array.
        val l_newColumns = Arrays.copyOf(this.m_array, l_newAmountColumns)
        l_newColumns[l_newAmountColumns - 1] = l_newColumn

        this.indexColumn = l_newIndexColumn
        this.m_array = l_newColumns
        this.pExtendChildContentAfterCopy(indexStrgC, indexStrgV)
        return true
    }

    /**
     * Returns the amount of the columns. It is the same as the amount of cells
     * in a row.
     *
     * @return Amount of columns.
     */
    @API
    fun amountColumns(): Int {
        return this.indexColumn.size
    }

    /**
     * Returns the amount of the rows. It is the same as the amount of cells
     * in a column.
     *
     * @return Amount of rows.
     */
    @API
    fun amountRows(): Int {
        return this.indexRow.size
    }

    /**
     * Moves the pointer currentPos to newPos from the first indice.
     *
     * @param currentPos Position pointer to be moved.
     * @param newPos     New position of pointer currentPos.
     * @throws IndexOutOfBoundsException Out of the bounds of first indice.
     */
    @API
    @Throws(IndexOutOfBoundsException::class)
    fun moveColumn(currentPos: Int,
                   newPos: Int) {
        VirtualArray2D.internalMove(currentPos, newPos, this.indexColumn)
    }

    /**
     * Child action after a column was copied. This method is intended to
     * enable adding the children's own data. The new Element copied to
     * indexStrgV was created at the last position of the Array this
     * .m_array. If you want add Array Data, extend your array with one field
     * and place the new data at that position.
     *
     * @param indexStrgC source to copy.
     * @param indexStrgV virtual destination.
     */
    protected open fun pExtendChildContentAfterCopy(indexStrgC: Int,
                                                    indexStrgV: Int) {

    }
}
