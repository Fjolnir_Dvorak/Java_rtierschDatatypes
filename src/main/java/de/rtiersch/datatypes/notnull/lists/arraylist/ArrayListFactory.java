/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.lists.arraylist;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Factory to generate an ArrayList. Useful for chain calls.
 *
 * @param <E> Type of the m_content for the ArrayList this class is generating.
 */
public final class ArrayListFactory<E>
{
    /**
     * intern arraylist. Can be returned with {@link this.generate()}.
     */
    private final @NotNull ArrayList<E> m_content;

    /**
     * Private Constructor.
     */
    private ArrayListFactory()
    {
        this.m_content = new ArrayList<>();
    }

    /**
     * Default generator without parameter.
     *
     * @param <T> Type of the ArrayList.
     * @return ArrayListFactory.
     */
    public static @NotNull <T> ArrayListFactory<T> start()
    {
        return new ArrayListFactory<>();
    }

    /**
     * Constructor with m_first add.
     *
     * @param p_firstElem First element to add.
     * @param <T>    Type of the ArrayList.
     * @return ArrayListFactory.
     */
    public static @NotNull <T> ArrayListFactory<T> start(final T p_firstElem)
    {
        final ArrayListFactory<T> l_toReturn = new ArrayListFactory<>();
        l_toReturn.add(p_firstElem);
        return l_toReturn;
    }

    /**
     * Adds another element to the ArrayList.
     *
     * @param p_element New element.
     * @return ArrayListFactory.
     */
    public @NotNull ArrayListFactory<E> add(final E p_element)
    {
        this.m_content.add(p_element);
        return this;
    }

    /**
     * Constructor with m_first add.
     *
     * @param p_collection First element to add.
     * @param <T>          Type of the ArrayList.
     * @return ArrayListFactory.
     */
    public static @NotNull <T> ArrayListFactory<T> start(
        final @NotNull Collection<? extends T> p_collection)
    {
        final ArrayListFactory<T> l_toReturn = new ArrayListFactory<>();
        l_toReturn.addAll(p_collection);
        return l_toReturn;
    }

    /**
     * Adds another element to the ArrayList.
     *
     * @param p_collection New elements.
     * @return ArrayListFactory.
     */
    public @NotNull ArrayListFactory<E> addAll(
        final @NotNull Collection<? extends E> p_collection)
    {
        this.m_content.addAll(p_collection);
        return this;
    }

    /**
     * Adds another element to the ArrayList.
     *
     * @param p_index   Index the new element will be inserted into.
     * @param p_element New element.
     * @return ArrayListFactory.
     */
    public @NotNull ArrayListFactory<E> add(
        final int p_index, final E p_element)
    {
        this.m_content.add(p_index, p_element);
        return this;
    }

    /**
     * Adds another element to the ArrayList.
     *
     * @param p_index      Index the new element will be inserted into.
     * @param p_collection New elements.
     * @return ArrayListFactory.
     */
    public @NotNull ArrayListFactory<E> addAll(
        final int p_index, final @NotNull Collection<? extends E> p_collection)
    {
        this.m_content.addAll(p_index, p_collection);
        return this;
    }

    /**
     * Returns the m_content of the factory.
     *
     * @return ArrayList m_content.
     */
    public @NotNull ArrayList<E> generate()
    {
        return this.m_content;
    }
}
