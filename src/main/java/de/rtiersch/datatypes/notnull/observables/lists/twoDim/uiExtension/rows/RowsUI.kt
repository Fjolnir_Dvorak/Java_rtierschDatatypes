package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Row
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Rows
import java.util.*

class RowsUI<E>(m_rows: ArrayList<Row<E>>) : Rows<E>(m_rows), IRowsUI<E> {
    override fun getRow(p_row: Int): RowUI<E> {
        return super.getRow(p_row) as RowUI<E>
    }

    override fun iterator(): Iterator<IRowUI<E>> {
        return RowsIteratorUI(this) as Iterator<IRowUI<E>>
    }


    protected open inner class RowsIteratorUI(m_iterateThrough: RowsUI<E>) :
        RowsIterator(
        m_iterateThrough) {
        override fun next(): IRowUI<E> {
            return super.next() as IRowUI<E>
        }
    }
}
