/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists;

import de.rtiersch.datatypes.enums.EDirection;
import de.rtiersch.datatypes.notnull.container.IIndexedSetDoubleSet;
import de.rtiersch.datatypes.notnull.observables.Listener.EListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IDoubleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener.IListAddListener;
import de.rtiersch.datatypes.notnull.observables.Listener.IListRemoveListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .ListenerContainerEXTTable;
import de.rtiersch.datatypes.notnull.observables.ObservableValue2D;
import de.rtiersch.datatypes.notnull.shared.IIntegerRead;
import de.rtiersch.datatypes.notnull.shared.IntegerContainer;
import de.rtiersch.util.lists.ArrayUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Creates a wrapper for two dimensional ObservableArrayLists.
 *
 * @param <E> Type.
 */
public class ObservableArrayList2D<E>
    implements IList2DFixedSize<E>
{

    /**
     * Main table used for first initialisation. Should make no difference
     * how the array is allocated. Java has no column / row oriented array
     * design.
     * Intern method structure is trying to guarantee containing lists of
     * equal length.
     */
    @SuppressWarnings("CanBeFinal")
    protected final @NotNull
    ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
        m_tableY;
    /**
     * Table accessing the the columns instead of rows. "Transformed" matrix
     * of m_columns. This is only copying the references of the content, not
     * the content itself.
     * Intern method structure is trying to guarantee containing lists of
     * equal length.
     */
    @SuppressWarnings("CanBeFinal")
    protected final @NotNull
    ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
        m_tableX;

    /**
     * Shared listener container for table and content.
     */
    protected final @NotNull ListenerContainerEXTTable<E> m_listener;

    protected final @NotNull ArrayList<IntegerContainer> m_indexX;
    protected final @NotNull ArrayList<IntegerContainer> m_indexY;

    /**
     * Creates a new two dimensional arrayList with normed row- and column
     * length. Missing fields will be filled with null.
     *
     * @param p_elements array to observe (do not observe the array, only the
     *                   objects itself).
     */
    public ObservableArrayList2D(
        final @NotNull E[][] p_elements, final @NotNull EIndexType p_indices)
    {

        // TODO Row Column. Is this correct????
        // TODO I think the constructor must be switched...
        final E[][] l_elements;
        switch (p_indices) {
            case XY:
                l_elements = (E[][]) ArrayUtils.changeXYtoYX(p_elements);
                break;
            case YX:
                l_elements = p_elements;
                break;
            default:
                // Why the bloody hell should I write this default thing
                // which is completely unneccessary...!
                // Do not delete: This line is here to let the compiler
                // compile. Without it Java is saying that the final variable
                // l_elements will not be initialized. Just write nonesense code
                // to get it to compile. Unreachable statements.
                l_elements = p_elements;
        }

        this.m_listener =
            new ListenerContainerEXTTable<>(EListener.DOUBLE_INDEXED_CHANGE);

        final int l_maxY = l_elements.length;
        final int l_maxX;
        final IntegerContainer[] l_indexY = new IntegerContainer[l_maxY];
        final IntegerContainer[] l_indexX;

        if (l_maxY == 0) {
            l_maxX = 0;
        } else {
            /*
             * Find the longest X.
             */
            int l_longestX = l_elements[0].length;
            for (final E[] l_element : l_elements) {
                if (l_element.length > l_longestX) {
                    l_longestX = l_element.length;
                }
            }
            l_maxX = l_longestX;
        }
        /*
         * Initialize l_indexY.
         */
        for (int l_i = 0; l_i < l_maxY; l_i++) {
            l_indexY[l_i] = new IntegerContainer(l_i);
        }
        /*
         * Initialize l_indexX.
         */
        l_indexX = new IntegerContainer[l_maxX];
        for (int l_i = 0; l_i < l_maxX; l_i++) {
            l_indexX[l_i] = new IntegerContainer(l_i);
        }

        /*
         * Initialize l_listX.
         */
        final ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
            l_listX;
        l_listX = new ObservableArrayList<>(l_maxX);
        for (int l_x = 0; l_x < l_maxX; l_x++) {
            l_listX.add(new ObservableArrayList<>(l_maxY));
        }
        // Iterate through y
        for (int l_y = 0; l_y < l_maxY; l_y++) {
            // Iterate through x
            int l_x;
            for (l_x = 0; l_x < l_elements[l_y].length; l_x++) {
                l_listX.get(l_x)
                       .add(l_y, new ObservableValue2D<>(l_indexX[l_x],
                                                         l_indexY[l_y],
                                                         l_elements[l_y][l_x],
                                                         this.m_listener));
                if (l_elements[l_y][l_x] instanceof IIndexedSetDoubleSet) {
                    ((IIndexedSetDoubleSet) l_elements[l_y][l_x])
                        .setXY(l_indexX[l_x], l_indexY[l_y]);
                }
            }
            /*
             * fill up all missing elements of x.
             */
            for (; l_x < l_maxX; l_x++) {
                l_listX.get(l_x)
                       .add(l_y, new ObservableValue2D<>(l_indexX[l_x],
                                                         l_indexY[l_y], null,
                                                         this.m_listener));
            }
        }

        /*
         * Now copy all the intern references to l_listY.
         */
        final ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
            l_listY;
        l_listY = new ObservableArrayList<>(l_maxY);
        for (int l_y = 0; l_y < l_maxY; l_y++) {
            final ObservableArrayList<ObservableValue2D<E>> l_fixY;
            l_fixY = new ObservableArrayList<>(l_maxX);
            l_listY.add(l_fixY);
            for (int l_x = 0; l_x < l_maxX; l_x++) {
                l_fixY.add(l_listX.get(l_x).get(l_y));
            }
        }

        this.m_tableX = l_listX;
        this.m_tableY = l_listY;
        this.m_indexX = new ArrayList<>(Arrays.asList(l_indexX));
        this.m_indexY = new ArrayList<>(Arrays.asList(l_indexY));
    }

    //<editor-fold desc="Getter">

    public final @NotNull ListenerContainerEXTTable<E> getListener()
    {
        return this.m_listener;
    }

    /**
     * Adds a single row at the end ob the table. (row starts at column index
     * 0. Missing elements will be filled with null).
     *
     * @param p_newRow new Row
     * @return success.
     */
    public boolean addRow(final @NotNull E[] p_newRow)
    {
        return this.addRow(p_newRow, this.m_tableY.size());
    }

    /**
     * Adds a single row at the defined index position. row starts at column
     * index 0. Missing elements will be filled with null).
     *
     * @param p_newRow new Row.
     * @param p_index  position.
     * @return success.
     */
    public boolean addRow(final @NotNull E[] p_newRow, final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_newList;
        l_newList = this.addA(this.m_tableY, this.m_tableX, this.m_indexY,
                              this.m_indexX, p_newRow, p_index,
                              EDirection.Y);
        /*
         * Fire Listener.
         */
        this.m_listener.fireRowAddedListener(l_newList, p_index);
        return true;
    }

    protected @NotNull ObservableArrayList<ObservableValue2D<E>> addA(
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listA,
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listB,
        final @NotNull ArrayList<IntegerContainer> p_indexA,
        final @NotNull ArrayList<IntegerContainer> p_indexB,
        final @NotNull E[] p_newA,
        final int p_index,
        final @NotNull EDirection p_dir)
    {
        final int l_b = p_listB.size();
        final int l_a = p_listA.size() + 1;
        final int l_lengthInput = p_newA.length;
        int l_j;

        /*
         * At first create the single A.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_newListA;
        l_newListA = new ObservableArrayList<>(l_b);
        final IntegerContainer l_indexNewA = new IntegerContainer(p_index);
        /*
         * Add existing elements.
         */
        for (l_j = 0; l_j < l_lengthInput; l_j++) {
            l_newListA.add(
                new ObservableValue2D<>(l_indexNewA, p_indexB.get(l_j), p_dir,
                                        p_newA[l_j], this.m_listener));
            if (p_newA[l_j] instanceof IIndexedSetDoubleSet) {
                ((IIndexedSetDoubleSet) p_newA[l_j]).setIndices(l_indexNewA,
                                                           p_indexB.get(l_j),
                                                           p_dir);
            }
        }
        /*
         * Add missing elements.
         */
        // TODO I do not want to have NULL values in this datatype.
        for (; l_j < l_b; l_j++) {
            l_newListA.add(
                new ObservableValue2D<>(l_indexNewA, p_indexB.get(l_j), p_dir,
                                        null, this.m_listener));
        }
        /*
         * At second insert the new A into the p_listB.
         */
        for (int l_i = 0; l_i < l_b; l_i++) {
            final ObservableArrayList<ObservableValue2D<E>> l_y;
            l_y = p_listB.get(l_i);
            l_y.add(p_index, l_newListA.get(l_i));
        }
        /*
         * At third insert the new A.
         */
        p_listA.add(p_index, l_newListA);
        p_indexA.add(p_index, l_indexNewA);
        /*
         * At last update all wrong A Indices.
         */
        for (int l_i = p_index + 1; l_i < l_a; l_i++) {
            p_indexA.get(l_i).setInt(l_i);
        }
        return l_newListA;
    }

    public boolean copyRow(
        final int p_rowToCopy, final int p_destination)
    {
        /*
         * Generate an array and pass it as to addRow.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_oldRow;
        l_oldRow = this.m_tableY.get(p_rowToCopy);
        final E[] l_newRow = (E[]) new Object[l_oldRow.size()];
        for (int l_i = 0; l_i < l_oldRow.size(); l_i++) {
            l_newRow[l_i] = l_oldRow.get(l_i).getValue();
        }
        return this.addRow(l_newRow, p_destination);
    }

    /**
     * Adds a single column at the end ob the table. (column starts at column
     * index 0. Missing elements will be filled with null).
     *
     * @param p_newColumn new Column.
     * @return success.
     */
    public boolean addColumn(final @NotNull E[] p_newColumn)
    {
        return this.addColumn(p_newColumn, this.m_tableX.size());
    }

    /**
     * Adds a single column at the defined index position. Column starts at
     * row index 0. Missing elements will be filled with null).
     *
     * @param p_newColumn new Column.
     * @param p_index     position.
     * @return success.
     */
    public boolean addColumn(
        final @NotNull E[] p_newColumn, final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_newList;
        l_newList = this.addA(this.m_tableX, this.m_tableY, this.m_indexX,
                              this.m_indexY, p_newColumn, p_index,
                              EDirection.X);
        /*
         * Fire Listener.
         */
        this.m_listener.fireColumnAddedListener(l_newList, p_index);
        return true;
    }

    public boolean copyColumn(
        final int p_columnToCopy, final int p_destination)
    {
        /*
         * Generate an array and pass it as to addColumn.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_oldColumn;
        l_oldColumn = this.m_tableX.get(p_columnToCopy);
        final E[] l_newColumn = (E[]) new Object[l_oldColumn.size()];
        for (int l_i = 0; l_i < l_oldColumn.size(); l_i++) {
            l_newColumn[l_i] = l_oldColumn.get(l_i).getValue();
        }
        return this.addColumn(l_newColumn, p_destination);
    }

    //</editor-fold>
    public void setXY(final int p_x, final int p_y, final E p_new)
    {
        this.getColumn(p_x).get(p_y).setValue(p_new);
        if (p_new instanceof IIndexedSetDoubleSet) {
            ((IIndexedSetDoubleSet) p_new).setXY(this.m_indexX.get(p_x),
                                                 this.m_indexY.get(p_y));
        }
    }

    //</editor-fold>
    //<editor-fold desc="Add">

    /**
     * Returns a column.
     *
     * @param p_index column to return.
     * @return Column.
     */
    @Override
    public IListFixedSize<ObservableValue2D<E>> getColumn(
        final int p_index)
    {
        return this.m_tableX.get(p_index);
    }

    /**
     * Returns a row.
     *
     * @param p_index row to return.
     * @return Row.
     */
    @Override
    public IListFixedSize<ObservableValue2D<E>> getRow(
        final int p_index)
    {
        return this.m_tableY.get(p_index);
    }

    /**
     * Returns all columns.
     *
     * @return Column.
     */
    @SuppressWarnings("SuppressionAnnotation")
    @Override
    public @NotNull IListFixedSize<IListFixedSize<ObservableValue2D<E>>>
    getColumns()
    {
        //noinspection unchecked
        return (IListFixedSize<IListFixedSize<ObservableValue2D<E>>>)
            (Object) this.m_tableX;
    }

    /**
     * Returns all rows.
     *
     * @return Row.
     */
    @SuppressWarnings("SuppressionAnnotation")
    @Override
    public @NotNull IListFixedSize<IListFixedSize<ObservableValue2D<E>>>
    getRows()
    {
        //noinspection unchecked
        return (IListFixedSize<IListFixedSize<ObservableValue2D<E>>>)
            (Object) this.m_tableY;
    }

    /**
     * Returns a single element.
     *
     * @param p_indexRow    row index.
     * @param p_indexColumn column index.
     * @return content.
     */
    @Override
    public ObservableValue2D<E> getXY(
        final int p_indexColumn, final int p_indexRow)
    {
        return this.m_tableX.get(p_indexColumn).get(p_indexRow);
    }

    /**
     * Returns a single element.
     *
     * @param p_indexColumn column index.
     * @param p_indexRow    row index.
     * @return content.
     */
    @Override
    public ObservableValue2D<E> getYX(
        final int p_indexRow, final int p_indexColumn)
    {
        return this.m_tableX.get(p_indexColumn).get(p_indexRow);
    }

    @Override
    public final IIntegerRead getIndexX(final int p_index)
    {
        return this.m_indexX.get(p_index);
    }

    @Override
    public final IIntegerRead getIndexY(final int p_index)
    {
        return this.m_indexY.get(p_index);
    }
    //<editor-fold desc="Remove">

    /**
     * Removes a single row at the defined index position. (Row starts at
     * column index 0).
     *
     * @param p_index position.
     * @return success.
     */
    public boolean removeRow(final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_oldList;
        l_oldList =
            this.removeA(this.m_tableY, this.m_tableX, this.m_indexY, p_index);
        /*
         * Fire Listener.
         */
        this.m_listener.fireRowRemovedListener(l_oldList, p_index);
        return true;
    }

    protected @NotNull ObservableArrayList<ObservableValue2D<E>> removeA(
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listA,
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listB,
        final @NotNull ArrayList<IntegerContainer> p_indexA,
        final int p_index)
    {
        /*
         * At first remove A out of p_listB.
         */
        for (final ObservableArrayList<ObservableValue2D<E>> l_a : p_listB) {
            l_a.remove(p_index);
        }
        /*
         * At second remove A out of p_listA.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_oldList;
        l_oldList = p_listA.get(p_index);
        p_listA.remove(p_index);
        p_indexA.remove(p_index);
        /*
         * At last update all wrong A Indices.
         */
        for (int l_i = p_index; l_i < p_listA.size(); l_i++) {
            p_indexA.get(l_i).setInt(l_i);
        }

        return l_oldList;
    }

    /**
     * Removes a single column at the defined index position. (Column starts at
     * row index 0).
     *
     * @param p_index position.
     * @return success.
     */
    public boolean removeColumn(final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_oldList;
        l_oldList =
            this.removeA(this.m_tableX, this.m_tableY, this.m_indexX, p_index);
        /*
         * Fire Listener.
         */
        this.m_listener.fireColumnRemovedListener(l_oldList, p_index);
        return true;
    }

    //</editor-fold>
    //<editor-fold desc="IHasDoubleIndexedChangeListener">
    @Override
    public void addChangeListener(
        final IDoubleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.addChangeListener(p_listener);
    }

    @Override
    public void removeChangeListener(
        final IDoubleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.removeChangeListener(p_listener);
    }

    //</editor-fold>
    //<editor-fold desc="IHasTableChangeListener">

    @Override
    public void addRowAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_listener.addRowAddedListener(p_listener);
    }

    @Override
    public void removeRowAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_listener.removeRowAddedListener(p_listener);
    }

    @Override
    public void addRowRemovedListener(final IListRemoveListener<E> p_listener)
    {
        this.m_listener.addRowRemovedListener(p_listener);
    }

    @Override
    public void removeRowRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_listener.removeRowRemovedListener(p_listener);
    }

    @Override
    public void addColumnAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_listener.addColumnAddedListener(p_listener);
    }

    @Override
    public void removeColumnAddedListener(
        final IListAddListener<E> p_listener)
    {
        this.m_listener.removeColumnAddedListener(p_listener);
    }

    @Override
    public void addColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_listener.addColumnRemovedListener(p_listener);
    }

    @Override
    public void removeColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_listener.removeColumnRemovedListener(p_listener);
    }

    //</editor-fold>
}
