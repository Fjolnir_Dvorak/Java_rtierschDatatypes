/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.init

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.Listener
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICreateICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.AInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Column
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Columns
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.ICreateAInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderInit
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.AInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.ICreateAInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Row
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Rows
import de.rtiersch.datatypes.notnull.shared.IntegerContainer
import de.rtiersch.datatypes.notnull.shared.StringContainer
import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.lists.ArrayUtilsKt
import java.util.*
import kotlin.collections.ArrayList

/**
 * All the things needed to initialize and validate the things from
 * InformationList. This is a static helper class.
 */
/**
 * Default Constructor. Make it non-static to make it overridable.
 */
open class InitList {

    /**
     * Validates the input array. That means it checks if it is NotNull and
     * rectangular. It also normalizes the orientation to RowColumn.
     *
     * @param p_elements    The input that will be rotated and checked.ArrayUtilsKt.
     * @param p_orientation The initial orientation of the input arrayArrayUtilsKt.
     * @param <E>           Content type.ArrayUtilsKt.
     * @return Array p_elements rotated to RowColumn.ArrayUtilsKt.ArrayUtilsKt.
     * @throws ArrayNotRectangularException p_element has different lengths.ArrayUtilsKt.
     * @throws NullPointerException         p_element contains null values.
     */
    @Throws(ArrayNotRectangularException::class,
        NullPointerException::class) open fun <E> validateData(p_elements: Array<Array<E>>,
                                                               p_orientation: EOrientation): Array<Array<E>> {
        val l_elements: Array<Array<E>>
        if (!ArrayUtilsKt.isRectangular(p_elements as Array<Array<Any>>)) {
            throw ArrayNotRectangularException(
                "Input array is not " + "rectangular.")
        }
        if (ArrayUtilsKt.testNull(p_elements as Array<Array<Any?>?>?)) {
            throw NullPointerException(
                "Input array contains null values." + " Not allowed in a not null " + "datatype.")
        }
        l_elements = when (p_orientation) {
            EOrientation.ColumnRow -> ArrayUtilsKt.changeXYtoYX(p_elements as Array<Array<Any>>) as Array<Array<E>>
            EOrientation.RowColumn -> p_elements
        }
        return l_elements
    }

    /**
     * Returns the header and the rest of the array after eventually stripping
     * it apart.
     *
     * @param p_elem    Already validated content array.ArrayUtilsKt.
     * @param p_headerH What for a horizontal header should there be?ArrayUtilsKt.
     * @param p_headerV What for a vertical header should there be?ArrayUtilsKt.
     * @param <E>       Content type.ArrayUtilsKt.
     * @return Both header and the input array to work with further.
     */
    open fun <E> createHeader(p_elem: Array<Array<E>>,
                              p_headerH: EHeaderInit,
                              p_headerV: EHeaderInit): SerializedData<E> {
        var elem = p_elem
        val l_headerV: Array<String>
        val l_headerH: Array<String>
        val l_horizontalNoFirst: Boolean
        val l_elemLength: Int
        if (p_headerH === EHeaderInit.FirstElems) {
            l_horizontalNoFirst = true
            l_elemLength = elem.size - 1
        } else {
            l_horizontalNoFirst = false
            l_elemLength = elem.size
        }
        when (p_headerV) {
            EHeaderInit.FirstElems -> {
                val l_headerC = ArrayUtilsKt.cutSecondIndex(elem, 0)
                elem = l_headerC.m_rest
                l_headerV = if (l_horizontalNoFirst) {
                    // Throw away the first Cell
                    val l_temp = ArrayUtilsKt.toString(l_headerC.m_extracted as Array<Any>)
                    Arrays.copyOfRange(l_temp, 1, l_temp.size)
                } else {
                    ArrayUtilsKt.toString(l_headerC.m_extracted as Array<Any>)
                }
            }
            EHeaderInit.Numeric -> l_headerV = ArrayUtilsKt.createNumberedArray(
                Locale.GERMANY, 0.0, 1.0, 0, l_elemLength)
            EHeaderInit.NumericOne -> l_headerV = ArrayUtilsKt.createNumberedArray(
                Locale.GERMANY, 1.0, 1.0, 0, l_elemLength)
            EHeaderInit.Alphanumeric -> l_headerV = ArrayUtilsKt.createAlphaNumericArray(
                l_elemLength)
            EHeaderInit.None -> l_headerV = Array(size = l_elemLength, init = {_ -> ""})
        }
        when (p_headerH) {
            EHeaderInit.FirstElems -> {
                val l_headerC = ArrayUtilsKt.cutFirstIndex(elem, 0)
                elem = l_headerC.m_rest
                l_headerH = ArrayUtilsKt.toString(
                    l_headerC.m_extracted as Array<Any>)
            }
            EHeaderInit.Numeric -> l_headerH = ArrayUtilsKt.createNumberedArray(
                Locale.GERMANY, 0.0, 1.0, 0, elem[0].size)
            EHeaderInit.NumericOne -> l_headerH = ArrayUtilsKt.createNumberedArray(
                Locale.GERMANY, 1.0, 1.0, 0, elem[0].size)
            EHeaderInit.Alphanumeric -> l_headerH = ArrayUtilsKt.createAlphaNumericArray(
                elem[0].size)
            EHeaderInit.None ->l_headerH = Array(size = elem[0].size, init = {_ -> ""})
        }
        return SerializedData(l_headerH, l_headerV, elem)
    }

    /**
     * Creates the final table with all the pre-validated values.
     * @param p_data Header and dataTable.ArrayUtilsKt.
     * @param p_createCell Cell creation class.ArrayUtilsKt.
     * @param p_createInfoRow Row based information creation class.ArrayUtilsKt.
     * @param p_createInfoColumn Column based information creation class.ArrayUtilsKt.
     * @param p_listener Listener.ArrayUtilsKt.
     * @param <E> Content type.ArrayUtilsKt.
     * @return Table, ready to use by InformationList.
     */
    open fun <E> createTable(table: Array<Array<E>>,
                             headerH: ArrayList<StringContainer>,
                             headerV: ArrayList<StringContainer>,
                             p_createCell: ICreateICell<E>,
                             p_createInfoRow: ICreateAInfoRow<E>,
                             p_createInfoColumn: ICreateAInfoColumn<E>,
                             p_listener: Listener<E>): InitList.Table<E> {

        val l_maxRows = table.size
        val l_indexRows = arrayOfNulls<IntegerContainer>(l_maxRows)
        val l_infoRows = arrayOfNulls<AInfoRow<E>>(l_maxRows)
        /*
         * Initialize l_indexRows and l_infoRows.
         */
        for (l_i in 0 until l_maxRows) {
            val container = IntegerContainer(l_i)
            l_indexRows[l_i] = container
            l_infoRows[l_i] = p_createInfoRow.constructInfoRow(container)
        }

        val l_maxColumns = table[0].size
        val l_indexColumns = arrayOfNulls<IntegerContainer>(l_maxColumns)
        val l_infoColumns = arrayOfNulls<AInfoColumn<E>>(l_maxColumns)

        /*
         * Initialize l_indexColumns.
         */
        for (l_i in 0 until l_maxColumns) {
            val container = IntegerContainer(l_i)
            l_indexColumns[l_i] = container
            l_infoColumns[l_i] = p_createInfoColumn.constructInfoColumn(
                container)
        }

        /*
         * Initialize rows.
         */
        val l_rowsArray = ArrayList<Row<E>>(l_maxRows)
        for (l_iRow in 0 until l_maxRows) {
            val l_current = ArrayList<ICellSet<E>>(l_maxColumns)
            val currentRow = createRow(cells = l_current,
                header = headerV,
                index = l_indexRows[l_iRow] as IntegerContainer,
                shadowedInfo = l_infoRows[l_iRow] as AInfoRow<E>)
            l_rowsArray.add(l_iRow, currentRow)
            var l_iCol: Int = 0
            while (l_iCol < table[l_iRow].size) {
                l_current.add(l_iCol, p_createCell.constructCell(
                    l_infoRows[l_iRow] as AInfoRow<E>,
                    l_infoColumns[l_iCol] as AInfoColumn<E>, p_listener,
                    table[l_iRow][l_iCol]))
                l_iCol++
            }
        }
        val l_rows = createRows(rows = l_rowsArray)

        /*
         * Now copy all the intern references to l_columns.
         */
        val l_columnsArray = ArrayList<Column<E>>(l_maxColumns)
        for (l_iCol in 0 until l_maxColumns) {
            val l_current = ArrayList<ICellSet<E>>(l_maxRows)
            val currentColumn = createColumn(cells = l_current,
                header = headerH,
                index = l_indexColumns[l_iCol] as IntegerContainer,
                shadowedInfo = l_infoColumns[l_iCol] as AInfoColumn<E>)
            l_columnsArray.add(l_iCol, currentColumn)
            (0 until l_maxRows).mapTo(l_current) {
                l_rows.getRow(it).getCell(l_iCol)
            }
        }
        val l_columns = createColumns(columns = l_columnsArray)
        return InitList.Table(l_indexColumns as Array<IntegerContainer>,
            l_columns, l_indexRows as Array<IntegerContainer>, l_rows)
    }

    open fun <E> createRow(cells: ArrayList<ICellSet<E>>,
                           header: ArrayList<StringContainer>,
                           index: IntegerContainer,
                           shadowedInfo: AInfoRow<E>): Row<E> {
        return Row(cells = cells, headerInt = header, index = index,
            shadowedInfo = shadowedInfo)
    }

    open fun <E> createColumn(cells: ArrayList<ICellSet<E>>,
                              header: ArrayList<StringContainer>,
                              index: IntegerContainer,
                              shadowedInfo: AInfoColumn<E>): Column<E> {
        return Column(cells = cells, headerInt = header, index = index,
            shadowedInfo = shadowedInfo)
    }

    open fun <E> createRows(rows: ArrayList<Row<E>>): Rows<E> {
        return Rows(m_rows = rows)
    }

    open fun <E> createColumns(columns: ArrayList<Column<E>>): Columns<E> {
        return Columns(m_columns = columns)
    }

    /**
     * Contains the table created by createTable().
     *
     * @param p_indexColumns Index list for the columns.ArrayUtilsKt.
     * @param m_columns      ColumnsUI.ArrayUtilsKt.
     * @param p_indexRows    Index list for the rows.ArrayUtilsKt.
     * @param m_rows         Rows.
     * @param <E> Content type.
     */
    class Table<E>
    (p_indexColumns: Array<IntegerContainer>,
     val m_columns: Columns<E>,
     p_indexRows: Array<IntegerContainer>,
     val m_rows: Rows<E>) {
        /**
         * Column index list. This is stored separately to ease index
         * manipulation on Add/remove events.
         */
        val m_indexColumns: ArrayList<IntegerContainer> = ArrayList(
            Arrays.asList(*p_indexColumns))
        /**
         * Row index list. This is stored separately to ease index
         * manipulation on Add/remove events.
         */
        val m_indexRows: ArrayList<IntegerContainer> = ArrayList(
            Arrays.asList(*p_indexRows))

    }

    fun createSharedHeader(header: Array<String>): ArrayList<StringContainer> {
        val headerReturn = ArrayList<StringContainer>(header.size)
        header.iterator().forEach { headerReturn.add(StringContainer(it)) }
        return headerReturn
    }
}
