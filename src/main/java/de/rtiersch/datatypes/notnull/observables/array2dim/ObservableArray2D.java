/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.array2dim;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Raphael Tiersch on 05.08.2016.
 */
public class ObservableArray2D<E>
    extends AObservableArrayBase2D<E>
{

    private final E[][] m_wrappedArray;

    public ObservableArray2D(final E[][] p_arrayToObserve)
    {
        this.m_wrappedArray = p_arrayToObserve;
    }

    public ObservableArray2D(final @NotNull ObservableArray2D<E> p_copy)
    {
        this.m_wrappedArray =
            (E[][]) Array.newInstance(p_copy.get(0, 0).getClass(),
                                      p_copy.getLengthX(), p_copy.getLengthY());
        System.arraycopy(p_copy.m_wrappedArray, 0, this.m_wrappedArray, 0,
                         p_copy.m_wrappedArray.length);

        final ArrayList<String> l_test = new ArrayList<>();
        l_test.stream();
    }

    @Override
    public final int getLengthX()
    {
        return this.m_wrappedArray.length;
    }

    @Override
    public final int getLengthY()
    {
        return this.m_wrappedArray[0].length;
    }

    @Override
    public E get(final int p_indexX, final int p_indexY)
    {
        return this.m_wrappedArray[p_indexX][p_indexY];
    }

    @Override
    public void set(final int p_indexX, final int p_indexY, final E p_new)
    {
        final E l_old = this.m_wrappedArray[p_indexX][p_indexY];
        this.m_wrappedArray[p_indexX][p_indexY] = p_new;
        this.fireChange(p_indexX, p_indexY, l_old, p_new);
    }

    /**
     * Returns a hash code value for the object. This method is supported for
     * the benefit of hash tables such as those provided by {@link
     * java.util.HashMap}.
     * <p>
     * The general contract of {@code hashCode} is: <ul> <li>Whenever it is
     * invoked on the same object more than once during an execution of a Java
     * application, the {@code hashCode} method must consistently return the
     * same integer, provided no information used in {@code equals} comparisons
     * on the object is modified. This integer need not remain consistent from
     * one execution of an application to another execution of the same
     * application. <li>If two objects are equal according to the {@code
     * equals(Object)} method, then calling the {@code hashCode} method on each
     * of the two objects must produce the same integer result. <li>It is
     * <em>not</em> required that if two objects are unequal according to the
     * {@link Object#equals(Object)} method, then calling the {@code hashCode}
     * method on each of the two objects must produce distinct integer results.
     * However, the programmer should be aware that producing distinct integer
     * results for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by class
     * {@code Object} does return distinct integers for distinct objects. (This
     * is typically implemented by converting the internal address of the object
     * into an integer, but this implementation technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     *
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public final int hashCode()
    {
        return Arrays.hashCode(this.m_wrappedArray);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null
     * reference value {@code x}, {@code x.equals(x)} should return {@code
     * true}. <li>It is <i>symmetric</i>: for any non-null reference values
     * {@code x} and {@code y}, {@code x.equals(y)} should return {@code true}
     * if and only if {@code y.equals(x)} returns {@code true}. <li>It is
     * <i>transitive</i>: for any non-null reference values {@code x}, {@code
     * y}, and {@code z}, if {@code x.equals(y)} returns {@code true} and {@code
     * y.equals(z)} returns {@code true}, then {@code x.equals(z)} should return
     * {@code true}. <li>It is <i>consistent</i>: for any non-null reference
     * values {@code x} and {@code y}, multiple invocations of {@code
     * x.equals(y)} consistently return {@code true} or consistently return
     * {@code false}, provided no information used in {@code equals} comparisons
     * on the objects is modified. <li>For any non-null reference value {@code
     * x}, {@code x.equals(null)} should return {@code false}. </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements the most
     * discriminating possible equivalence relation on objects; that is, for any
     * non-null reference values {@code x} and {@code y}, this method returns
     * {@code true} if and only if {@code x} and {@code y} refer to the same
     * object ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the general
     * contract for the {@code hashCode} method, which states that equal objects
     * must have equal hash codes.
     *
     * @param p_object the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     * {@code false} otherwise.
     *
     * @see #hashCode()
     * @see java.util.HashMap
     */
    @Override
    public final boolean equals(final Object p_object)
    {
        if (p_object instanceof ObservableArray2DString) {
            return Arrays.equals(this.m_wrappedArray,
                                 ((ObservableArray2D) p_object).m_wrappedArray);
        }
        if (p_object instanceof String[][]) {
            return Arrays.equals(this.m_wrappedArray, (String[][]) p_object);
        }
        return false;
    }

    @Override
    public final @NotNull String toString()
    {
        return Arrays.toString(this.m_wrappedArray);
    }

//    public Stream<E> stream() {
//        return StreamSupport.stream(this.spliterator(), false);
//    }
//
//    private Spliterator<E> spliterator() {
//        return new ObservableArray2D.ArraySpliterator<>(this, 0, this
// .m_wrappedArray.length, Spliterator.ORDERED |
// Spliterator.IMMUTABLE);
//    }
//
//
//    /**
//     * A Spliterator designed for use by sources that traverse and split
//     * elements maintained in an unmodifiable {@code Object[]} array.
//     */
//    static final class ArraySpliterator<T> implements Spliterator<T> {
//        /**
//         * The array, explicitly typed as Object[]. Unlike in some other
//         * classes (see for example CR 6260652), we do not need to
//         * screen arguments to ensure they are exactly of type Object[]
//         * so long as no methods write into the array or serialize it,
//         * which we ensure here by defining this class as final.
//         */
//        private final ObservableArray2D<T> array;
//        private int index;        // current index, modified on advance/split
//        private final int fence;  // one past last index
//        private final int characteristics;
//
//        /**
//         * Creates a spliterator covering all of the given array.
//         * @param pArray the array, assumed to be unmodified during use
//         * @param pAdditionalCharacteristics Additional spliterator
// characteristics
//         * of this spliterator's source or elements beyond {@code SIZED} and
//         * {@code SUBSIZED} which are are always reported
//         */
//        public ArraySpliterator(final ObservableArray2D<T> pArray, final
// int pAdditionalCharacteristics) {
//            this(pArray, 0, pArray.getLengthX(), pAdditionalCharacteristics);
//        }
//
//        /**
//         * Creates a spliterator covering the given array and range
//         * @param pArray the array, assumed to be unmodified during use
//         * @param pOrigin the least index (inclusive) to cover
//         * @param pFence one past the greatest index to cover
//         * @param pAdditionalCharacteristics Additional spliterator
// characteristics
//         * of this spliterator's source or elements beyond {@code SIZED} and
//         * {@code SUBSIZED} which are are always reported
//         */
//        public ArraySpliterator(final ObservableArray2D<T> pArray, final
// int pOrigin, final int pFence,
//                                final int pAdditionalCharacteristics) {
//            this.array = pArray;
//            this.index = pOrigin;
//            this.fence = pFence;
//            this.characteristics = pAdditionalCharacteristics | Spliterator
// .SIZED | Spliterator.SUBSIZED;
//        }
//
//        @Override
//        public Spliterator<T> trySplit() {
//            final int lo = this.index;
//            final int mid = (lo + this.fence) >>> 1;
//            if (lo >= mid) {
//                return null;
//            } else {
//                this.index = mid;
//                return new ObservableArray2D.ArraySpliterator<>(this.array,
// lo, mid, this.characteristics);
//            }
//        }
//
//        @SuppressWarnings("unchecked")
//        @Override
//        public void forEachRemaining(final Consumer<? super T> pAction) {
//            Object[] a;
//            int i = this.index;
//            final int hi = this.fence; // hoist accesses and checks from loop
//            this.index = this.fence;
//            if (pAction == null) {
//                throw new NullPointerException();
//            }
//            if (this.array.m_wrappedArray.length >= hi && i >= 0 && i < hi) {
//                do {
//                    pAction.accept(this.array.get(i));
//                } while (++i < hi);
//            }
//        }
//
//        @Override
//        public boolean tryAdvance(final Consumer<? super T> pAction) {
//            if (pAction == null) {
//                throw new NullPointerException();
//            }
//            if (this.index >= 0 && this.index < this.fence) {
//                @SuppressWarnings("unchecked")
//                final T[] e = this.array.;
//                pAction.accept(e);
//                return true;
//            }
//            return false;
//        }
//
//        @Override
//        public long estimateSize() {
//            return (long) (this.fence - this.index);
//        }
//
//        @Override
//        public int characteristics() {
//            return this.characteristics;
//        }
//
//        @Override
//        public Comparator<? super T> getComparator() {
//            if (this.hasCharacteristics(Spliterator.SORTED)) {
//                return null;
//            }
//            throw new IllegalStateException();
//        }
//    }
}
