/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists;

import de.rtiersch.datatypes.notnull.observables.Listener
    .IHasDoubleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IHasTableChangeListener;
import de.rtiersch.datatypes.notnull.observables.ObservableValue2D;
import de.rtiersch.datatypes.notnull.shared.IIntegerRead;

/**
 * Created by Raphael Tiersch on 31.08.2016.
 */
public interface IList2DFixedSize<E>
    extends IHasDoubleIndexedChangeListener<E>, IHasTableChangeListener<E>
{
    /**
     * Returns a column.
     *
     * @param p_index column to return.
     * @return Column.
     */
    IListFixedSize<ObservableValue2D<E>> getColumn(int p_index);

    /**
     * Returns a row.
     *
     * @param p_index row to return.
     * @return Row.
     */
    IListFixedSize<ObservableValue2D<E>> getRow(int p_index);

    /**
     * Returns all columns.
     *
     * @return Column.
     */
    @SuppressWarnings("SuppressionAnnotation")
    IListFixedSize<IListFixedSize<ObservableValue2D<E>>> getColumns();

    /**
     * Returns all rows.
     *
     * @return Row.
     */
    @SuppressWarnings("SuppressionAnnotation")
    IListFixedSize<IListFixedSize<ObservableValue2D<E>>> getRows();

    /**
     * Returns a single element.
     *
     * @param p_indexRow    row index.
     * @param p_indexColumn column index.
     * @return content.
     */
    ObservableValue2D<E> getXY(int p_indexRow, int p_indexColumn);

    /**
     * Returns a single element.
     *
     * @param p_indexColumn column index.
     * @param p_indexRow    row index.
     * @return content.
     */
    ObservableValue2D<E> getYX(int p_indexColumn, int p_indexRow);

    IIntegerRead getIndexX(int p_index);

    IIntegerRead getIndexY(int p_index);
}
