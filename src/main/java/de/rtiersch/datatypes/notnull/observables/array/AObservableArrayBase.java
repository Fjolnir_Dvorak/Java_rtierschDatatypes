/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.array;

import de.rtiersch.datatypes.notnull.observables.Listener.EListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .ISingleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener.ListenerContainer;

/**
 * Created by Raphael Tiersch on 07.08.2016.
 */
public abstract class AObservableArrayBase<T>
    implements IObservableArray<T, ISingleIndexedChangeListener<T>>
{

    private final ListenerContainer<T> m_listenerHelper =
        new ListenerContainer<>(EListener.CHANGE);

    @Override
    public final void addListener(
        final ISingleIndexedChangeListener<T> p_listener)
    {
        this.m_listenerHelper.addChangeListener(p_listener);
    }

    @Override
    public final void removeListener(
        final ISingleIndexedChangeListener<T> p_listener)
    {
        this.m_listenerHelper.removeChangeListener(p_listener);
    }

    /**
     * Notifies all listeners of a change
     *
     * @param change
     */
    protected final void fireChange(
        final int p_index, final T p_old, final T p_new)
    {
        this.m_listenerHelper.fireChangeListener(p_index, p_old, p_new);
    }

    public abstract T set(final int p_indexX, final T p_new);
}
