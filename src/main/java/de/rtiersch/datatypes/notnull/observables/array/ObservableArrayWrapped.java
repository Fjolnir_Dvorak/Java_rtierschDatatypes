/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.array;

import de.rtiersch.datatypes.notnull.observables.Listener.EListener;
import de.rtiersch.datatypes.notnull.observables.Listener.ListenerContainer;
import de.rtiersch.datatypes.notnull.observables.ObservableValue1D;
import de.rtiersch.datatypes.notnull.shared.IntegerContainer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by Raphael Tiersch on 05.08.2016.
 */
public class ObservableArrayWrapped<E>
    extends AObservableArrayBaseWrapped<E>
{
    private final @NotNull ObservableValue1D<E>[] m_wrappedArray;
    private final @NotNull ListenerContainer<E>          m_listener;


    public ObservableArrayWrapped(final @NotNull E[] p_arrayToObserve)
    {
        final ObservableValue1D<E> l_init =
            new ObservableValue1D<>(new IntegerContainer(0), p_arrayToObserve[0]);
        this.m_wrappedArray =
            (ObservableValue1D<E>[]) new Object[p_arrayToObserve.length];
        this.m_listener =
            new ListenerContainer<>(EListener.SINGLE_INDEXED_CHANGE);
        for (int l_i = 0; l_i < p_arrayToObserve.length; l_i++) {
            this.m_wrappedArray[l_i] =
                new ObservableValue1D<>(new IntegerContainer(l_i), p_arrayToObserve[l_i],
                                               this.m_listener);
        }
    }

    public ObservableArrayWrapped(final @NotNull ObservableArray<E>
                                      p_arrayToObserve)
    {
        final ObservableValue1D<E> l_init =
            new ObservableValue1D<>(new IntegerContainer(0), p_arrayToObserve.get(0));
        this.m_wrappedArray =
            (ObservableValue1D<E>[]) new Object[p_arrayToObserve
                .getLengthX()];
        this.m_listener =
            new ListenerContainer<>(EListener.SINGLE_INDEXED_CHANGE);
        for (int l_i = 0; l_i < p_arrayToObserve.getLengthX(); l_i++) {
            this.m_wrappedArray[l_i] =
                new ObservableValue1D<>(new IntegerContainer(l_i), p_arrayToObserve.get(l_i),
                                               this.m_listener);
        }
    }

    @Override
    public final int getLengthX()
    {
        return this.m_wrappedArray.length;
    }

    @Override
    public ObservableValue1D<E> get(final int p_indexX)
    {
        return this.m_wrappedArray[p_indexX];
    }

    /**
     * Returns a hash code value for the object. This method is supported for
     * the benefit of hash tables such as those provided by {@link
     * java.util.HashMap}.
     * <p>
     * The general contract of {@code hashCode} is: <ul> <li>Whenever it is
     * invoked on the same object more than once during an execution of a Java
     * application, the {@code hashCode} method must consistently return the
     * same integer, provided no information used in {@code equals} comparisons
     * on the object is modified. This integer need not remain consistent from
     * one execution of an application to another execution of the same
     * application. <li>If two objects are equal according to the {@code
     * equals(Object)} method, then calling the {@code hashCode} method on each
     * of the two objects must produce the same integer result. <li>It is
     * <em>not</em> required that if two objects are unequal according to the
     * {@link Object#equals(Object)} method, then calling the {@code hashCode}
     * method on each of the two objects must produce distinct integer results.
     * However, the programmer should be aware that producing distinct integer
     * results for unequal objects may improve the performance of hash tables.
     * </ul>
     * <p>
     * As much as is reasonably practical, the hashCode method defined by class
     * {@code Object} does return distinct integers for distinct objects. (This
     * is typically implemented by converting the internal address of the object
     * into an integer, but this implementation technique is not required by the
     * Java&trade; programming language.)
     *
     * @return a hash code value for this object.
     *
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public final int hashCode()
    {
        return Arrays.hashCode(this.m_wrappedArray);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation on non-null
     * object references: <ul> <li>It is <i>reflexive</i>: for any non-null
     * reference value {@code x}, {@code x.equals(x)} should return {@code
     * true}. <li>It is <i>symmetric</i>: for any non-null reference values
     * {@code x} and {@code y}, {@code x.equals(y)} should return {@code true}
     * if and only if {@code y.equals(x)} returns {@code true}. <li>It is
     * <i>transitive</i>: for any non-null reference values {@code x}, {@code
     * y}, and {@code z}, if {@code x.equals(y)} returns {@code true} and {@code
     * y.equals(z)} returns {@code true}, then {@code x.equals(z)} should return
     * {@code true}. <li>It is <i>consistent</i>: for any non-null reference
     * values {@code x} and {@code y}, multiple invocations of {@code
     * x.equals(y)} consistently return {@code true} or consistently return
     * {@code false}, provided no information used in {@code equals} comparisons
     * on the objects is modified. <li>For any non-null reference value {@code
     * x}, {@code x.equals(null)} should return {@code false}. </ul>
     * <p>
     * The {@code equals} method for class {@code Object} implements the most
     * discriminating possible equivalence relation on objects; that is, for any
     * non-null reference values {@code x} and {@code y}, this method returns
     * {@code true} if and only if {@code x} and {@code y} refer to the same
     * object ({@code x == y} has the value {@code true}).
     * <p>
     * Note that it is generally necessary to override the {@code hashCode}
     * method whenever this method is overridden, so as to maintain the general
     * contract for the {@code hashCode} method, which states that equal objects
     * must have equal hash codes.
     *
     * @param p_object the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj argument;
     * {@code false} otherwise.
     *
     * @see #hashCode()
     * @see java.util.HashMap
     */
    @Override
    public final boolean equals(final Object p_object)
    {
        if (p_object instanceof ObservableArrayString) {
            return Arrays.equals(this.m_wrappedArray,
                                 ((ObservableArrayWrapped) p_object)
                                     .m_wrappedArray);
        }
        if (p_object instanceof String[]) {
            return Arrays.equals(this.m_wrappedArray, (String[]) p_object);
        }
        return false;
    }

    @Override
    public final @NotNull String toString()
    {
        return Arrays.toString(this.m_wrappedArray);
    }

    public @NotNull Stream<E> stream()
    {
        return StreamSupport.stream(this.spliterator(), false);
    }

    private @NotNull Spliterator<E> spliterator()
    {
        return new ObservableArrayWrapped.ArraySpliterator<>(this, 0,
                                                             this.m_wrappedArray.length,
                                                             Spliterator.ORDERED
                                                                 |
                                                                 Spliterator
                                                                     .IMMUTABLE);
    }

    /**
     * A Spliterator designed for use by sources that traverse and split
     * elements maintained in an unmodifiable {@code Object[]} m_array.
     */
    static final class ArraySpliterator<T>
        implements Spliterator<T>
    {
        /**
         * The m_array, explicitly typed as Object[]. Unlike in some other
         * classes (see for example CR 6260652), we do not need to screen
         * arguments to ensure they are exactly of type Object[] so long as no
         * methods write into the m_array or serialize it, which we ensure here
         * by defining this class as final.
         */
        private final ObservableArrayWrapped<T> m_array;
        private final int                       m_fence;
            // one past last m_index
        private final int                       m_characteristics;
        private       int                       m_index;
        // current m_index, modified on advance/split

        /**
         * Creates a spliterator covering all of the given m_array.
         *
         * @param p_array                     the m_array, assumed to be
         *                                    unmodified during use
         * @param p_additionalCharacteristics Additional spliterator
         *                                    m_characteristics
         *                                    of this spliterator's source or
         *                                    elements beyond {@code SIZED} and
         *                                    {@code SUBSIZED} which are are
         *                                    always reported
         */
        public ArraySpliterator(
            final ObservableArrayWrapped<T> p_array,
            final int p_additionalCharacteristics)
        {
            this(p_array, 0, p_array.getLengthX(), p_additionalCharacteristics);
        }

        /**
         * Creates a spliterator covering the given m_array and range
         *
         * @param p_array                     the m_array, assumed to be
         *                                    unmodified during use
         * @param p_origin                    the least m_index (inclusive) to
         *                                    cover
         * @param p_fence                     one past the greatest m_index to
         *                                    cover
         * @param p_additionalCharacteristics Additional spliterator
         *                                    m_characteristics
         *                                    of this spliterator's source or
         *                                    elements beyond {@code SIZED} and
         *                                    {@code SUBSIZED} which are are
         *                                    always reported
         */
        public ArraySpliterator(
            final ObservableArrayWrapped<T> p_array,
            final int p_origin,
            final int p_fence,
            final int p_additionalCharacteristics)
        {
            this.m_array = p_array;
            this.m_index = p_origin;
            this.m_fence = p_fence;
            this.m_characteristics =
                p_additionalCharacteristics | Spliterator.SIZED
                    | Spliterator.SUBSIZED;
        }

        @Override
        public boolean tryAdvance(final Consumer<? super T> p_action)
        {
            if (p_action == null) {
                throw new NullPointerException();
            }
            if ((this.m_index >= 0) && (this.m_index < this.m_fence)) {
                @SuppressWarnings("unchecked") final T l_e =
                    (T) this.m_array.get(this.m_index);
                this.m_index++;
                p_action.accept(l_e);
                return true;
            }
            return false;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void forEachRemaining(final Consumer<? super T> p_action)
        {
            Object[] l_a;
            int l_i = this.m_index;
            final int l_hi =
                this.m_fence; // hoist accesses and checks from loop
            this.m_index = this.m_fence;
            if (p_action == null) {
                throw new NullPointerException();
            }
            if ((this.m_array.m_wrappedArray.length >= l_hi) && (l_i >= 0) && (
                l_i < l_hi))
            {
                ++l_i;
                do {
                    p_action.accept((T) this.m_array.get(l_i));
                    ++l_i;
                } while (l_i < l_hi);
            }
        }

        @Override
        public @Nullable Spliterator<T> trySplit()
        {
            final int l_lo = this.m_index;
            final int l_mid = (l_lo + this.m_fence) >>> 1;
            if (l_lo >= l_mid) {
                return null;
            } else {
                this.m_index = l_mid;
                return new ObservableArrayWrapped.ArraySpliterator<>(
                    this.m_array, l_lo, l_mid, this.m_characteristics);
            }
        }

        @Override
        public long estimateSize()
        {
            return (long) (this.m_fence - this.m_index);
        }

        @Override
        public int characteristics()
        {
            return this.m_characteristics;
        }

        @Override
        public @Nullable Comparator<? super T> getComparator()
        {
            if (this.hasCharacteristics(Spliterator.SORTED)) {
                return null;
            }
            throw new IllegalStateException();
        }
    }
}
