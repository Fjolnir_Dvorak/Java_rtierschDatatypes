package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.AInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Row
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellG
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellSetG
import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.datatypes.notnull.shared.StringContainer
import java.util.*

open class RowUI<E>(cells: ArrayList<ICellSet<E>>,
                    headerInt: ArrayList<StringContainer>,
                    shadowedInfo: AInfoRow<E>,
                    index: IIntegerRead) : Row<E>(cells = cells,
    headerInt = headerInt, shadowedInfo = shadowedInfo,
    index = index), IRowUI<E> {
    override fun getCell(p_column: Int): ICellSetG<E> {
        return super.getCell(p_column) as ICellSetG<E>
    }

    override fun iterator(): Iterator<ICellG<E>> {
        return RowIteratorUI(this) as Iterator<ICellG<E>>
    }

    protected open inner class RowIteratorUI(m_iterateThrough: RowUI<E>) : RowIterator(
        m_iterateThrough) {
        override fun next(): ICellG<E> {
            return super.next() as ICellG<E>
        }
    }
}
