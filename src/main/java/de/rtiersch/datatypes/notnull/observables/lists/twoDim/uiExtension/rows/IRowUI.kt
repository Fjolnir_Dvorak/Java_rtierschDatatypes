package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellG

interface IRowUI<E> : IRow<E> {
    override fun getCell(p_column: Int): ICellG<E>
    override fun iterator(): Iterator<ICellG<E>>
}
