/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists;

import de.rtiersch.datatypes.notnull.container.IndexedItemIM;
import de.rtiersch.datatypes.notnull.observables.Listener.EListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IAddRemoveManyListener;
import de.rtiersch.datatypes.notnull.observables.Listener.IHasAddRemoveListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IHasSingleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .ISingleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener.ListenerContainer;
import org.apache.commons.lang3.NotImplementedException;
import org.jetbrains.annotations.NotNull;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * Resizable-array implementation of the <tt>List</tt> interface.  Implements
 * all optional list operations, and permits all elements, including
 * <tt>null</tt>.  In addition to implementing the <tt>List</tt> interface, this
 * class provides methods to manipulate the size of the array that is used
 * internally to store the list.  (This class is roughly equivalent to
 * <tt>Vector</tt>, except that it is unsynchronized.)
 * <p>
 * <p>The <tt>size</tt>, <tt>isEmpty</tt>, <tt>get</tt>, <tt>set</tt>,
 * <tt>iterator</tt>, and <tt>listIterator</tt> operations run in constant time.
 * The <tt>add</tt> operation runs in <i>amortized constant time</i>, that is,
 * adding n elements requires O(n) time.  All of the other operations run in
 * linear time (roughly speaking).  The constant factor is low compared to that
 * for the <tt>LinkedList</tt> implementation.
 * <p>
 * <p>Each <tt>ArrayList</tt> instance has a <i>capacity</i>.  The capacity is
 * the size of the array used to store the elements in the list.  It is always
 * at least as large as the list size.  As elements are added to an ArrayList,
 * its capacity grows automatically.  The details of the growth policy are not
 * specified beyond the fact that adding an element has constant amortized time
 * cost.
 * <p>
 * <p>An application can increase the capacity of an <tt>ArrayList</tt> instance
 * before adding a large number of elements using the <tt>ensureCapacity</tt>
 * operation.  This may reduce the amount of incremental reallocation.
 * <p>
 * <p><strong>Note that this implementation is not synchronized.</strong> If
 * multiple threads access an <tt>ArrayList</tt> instance concurrently, and at
 * least one of the threads modifies the list structurally, it <i>must</i> be
 * synchronized externally.  (A structural modification is any operation that
 * adds or deletes one or more elements, or explicitly resizes the backing
 * array; merely setting the value of an element is not a structural
 * modification.)  This is typically accomplished by synchronizing on some
 * object that naturally encapsulates the list.
 * <p>
 * If no such object exists, the list should be "wrapped" using the {@link
 * java.util.Collections#synchronizedList Collections.synchronizedList} method.
 * This is best done at creation time, to prevent accidental
 * unsynchronized access to the list:<pre>
 *   List list = Collections.synchronizedList(new ArrayList(...));</pre>
 * <p>
 * <p><a name="fail-fast"> The iterators returned by this class's {@link
 * #iterator() iterator} and {@link #listIterator(int) listIterator} methods are
 * <em>fail-fast</em>:</a> if the list is structurally modified at any time
 * after the iterator is created, in any way except through the iterator's own
 * {@link ListIterator#remove() remove} or {@link ListIterator#add(Object) add}
 * methods, the iterator will throw a
 * {@link java.util.ConcurrentModificationException}.
 * Thus, in the face of concurrent modification, the iterator fails quickly and
 * cleanly, rather than risking arbitrary, non-deterministic behavior at an
 * undetermined time in the future.
 * <p>
 * <p>Note that the fail-fast behavior of an iterator cannot be guaranteed as it
 * is, generally speaking, impossible to make any hard guarantees in the
 * presence of unsynchronized concurrent modification.  Fail-fast iterators
 * throw {@code ConcurrentModificationException} on a best-effort basis.
 * Therefore, it would be wrong to write a program that depended on this
 * exception for its correctness:  <i>the fail-fast behavior of iterators should
 * be used only to detect bugs.</i>
 * <p>
 * <p>This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @param <E> type of ArrayList.
 * @author Josh Bloch
 * @author Neal Gafter
 * @see Collection
 * @see List
 * @see java.util.LinkedList
 * @see java.util.Vector
 * @since 1.2
 */
public class ObservableArrayList<E>
    extends ArrayList<E>
    implements IHasSingleIndexedChangeListener<E>, IHasAddRemoveListener<E>,
               IListFixedSize<E>
{

    /**
     * Contains all listeners registered in this class.
     */
    private final @NotNull ListenerContainer<E> m_listener;

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param p_initialCapacity the initial capacity of the list
     * @throws IllegalArgumentException if the specified initial capacity is
     *                                  negative
     */
    public ObservableArrayList(final int p_initialCapacity)
    {
        super(p_initialCapacity);
        this.m_listener =
            new ListenerContainer<>(EListener.SINGLE_INDEXED_CHANGE,
                                    EListener.ADD_REMOVE_MANY);
    }

    /**
     * Constructs an empty list with an initial capacity of ten.
     */
    public ObservableArrayList()
    {
        this.m_listener =
            new ListenerContainer<>(EListener.SINGLE_INDEXED_CHANGE,
                                    EListener.ADD_REMOVE_MANY);
    }

    /**
     * Constructs a list containing the elements of the specified collection, in
     * the order they are returned by the collection's iterator.
     *
     * @param p_collection the collection whose elements are to be placed into
     *                     this list
     * @throws NullPointerException if the specified collection is null
     */
    public ObservableArrayList(final @NotNull Collection<? extends E>
                                   p_collection)
    {
        super(p_collection);
        this.m_listener =
            new ListenerContainer<>(EListener.SINGLE_INDEXED_CHANGE,
                                    EListener.ADD_REMOVE_MANY);
    }

    /**
     * Trims the capacity of this <tt>ArrayList</tt> instance to be the list's
     * current size.  An application can use this operation to minimize the
     * storage of an <tt>ArrayList</tt> instance.
     */
    @Override
    public void trimToSize()
    {
        super.trimToSize();
    }

    /**
     * Increases the capacity of this <tt>ArrayList</tt> instance, if necessary,
     * to ensure that it can hold at least the number of elements specified by
     * the minimum capacity argument.
     *
     * @param p_minCapacity the desired minimum capacity
     */
    @Override
    public void ensureCapacity(final int p_minCapacity)
    {
        super.ensureCapacity(p_minCapacity);
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size()
    {
        return super.size();
    }

    /**
     * Returns <tt>true</tt> if this list isOfChar no elements.
     *
     * @return <tt>true</tt> if this list isOfChar no elements
     */
    @Override
    public boolean isEmpty()
    {
        return super.isEmpty();
    }

    /**
     * Returns <tt>true</tt> if this list isOfChar the specified element. More
     * formally, returns <tt>true</tt> if and only if this list isOfChar at
     * least one element <tt>e</tt> such that <tt>(p_object==null&nbsp;?&nbsp;
     * e==null&nbsp; :&nbsp;p_object.equals(e))</tt>.
     *
     * @param p_object element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list isOfChar the specified element
     */
    @Override
    public boolean contains(final Object p_object)
    {
        return super.contains(p_object);
    }

    /**
     * Returns the index of the m_first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element. More
     * formally, returns the lowest index <tt>i</tt> such that
     * <tt>(p_object==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;p_object
     * .equals(get(i)))</tt>, or -1 if there is no such index.
     *
     * @param p_object object to search in list.
     */
    @Override
    public int indexOf(final Object p_object)
    {
        return super.indexOf(p_object);
    }

    /**
     * Returns the index of the last occurrence of the specified element in this
     * list, or -1 if this list does not contain the element. More formally,
     * returns the highest index <tt>i</tt> such that <tt>
     * (p_object==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;p_object
     * .equals(get(i))) </tt>, or -1 if there is no such index.
     *
     * @param p_object object to search in list.
     */
    @Override
    public int lastIndexOf(final Object p_object)
    {
        return super.lastIndexOf(p_object);
    }

    /**
     * Returns a shallow copy of this <tt>ArrayList</tt> instance.  (The
     * elements themselves are not copied.)
     *
     * @return a clone of this <tt>ArrayList</tt> instance
     */
    @Override
    public Object clone()
    {
        return super.clone();
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from m_first to last element).
     * <p>
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this list.  (In other words, this method must allocate a
     * new array).  The caller is thus free to modify the returned array.
     * <p>
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this list in proper
     * sequence
     */
    @Override
    public @NotNull Object[] toArray()
    {
        return super.toArray();
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from m_first to last element); the runtime type of the returned
     * array is that of the specified array.  If the list fits in the specified
     * array, it is returned therein.  Otherwise, p_array new array is allocated
     * with the runtime type of the specified array and the size of this list.
     * <p>
     * <p>If the list fits in the specified array with room to spare (i.e., the
     * array has more elements than the list), the element in the array
     * immediately following the end of the collection is set to <tt>null</tt>.
     * (This is useful in determining the length of the list <i>only</i> if the
     * caller knows that the list does not contain any null elements.)
     *
     * @param p_array the array into which the elements of the list are to be
     *                stored, if it is big enough; otherwise, p_array new array
     *                of the same runtime type is allocated for this purpose.
     * @return an array containing the elements of the list
     *
     * @throws ArrayStoreException  if the runtime type of the specified array
     *                              is not p_array supertype of the runtime type
     *                              of every element in this list
     * @throws NullPointerException if the specified array is null
     */
    @Override
    public @NotNull <T> T[] toArray(final T[] p_array)
    {
        return super.toArray(p_array);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param p_index index of the element to return
     * @return the element at the specified position in this list
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public E get(final int p_index)
    {
        return super.get(p_index);
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param p_index   index of the element to replace
     * @param p_element element to be stored at the specified position
     * @return the element previously at the specified position
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public E set(final int p_index, final E p_element)
    {
        final E l_old = super.set(p_index, p_element);
        this.m_listener.fireChangeListener(p_index, l_old, p_element);
        return l_old;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param p_element element to be appended to this list
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     */
    @Override
    public boolean add(final @NotNull E p_element)
    {
        final ArrayList<IndexedItemIM<E>> l_list = new ArrayList<>(1);
        l_list.add(new IndexedItemIM<>(super.size(), p_element));
        final boolean l_isAdded = super.add(p_element);
        this.m_listener.fireAddRemoveListener(l_list, true);
        return l_isAdded;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any subsequent
     * elements to the m_right (adds one to their indices).
     *
     * @param p_index   index at which the specified element is to be inserted
     * @param p_element element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public void add(final int p_index, final @NotNull E p_element)
    {
        final ArrayList<IndexedItemIM<E>> l_list = new ArrayList<>(1);
        l_list.add(new IndexedItemIM<>(p_index, p_element));
        super.add(p_index, p_element);
        this.m_listener.fireAddRemoveListener(l_list, true);
    }

    /**
     * Removes the element at the specified position in this list. Shifts any
     * subsequent elements to the m_left (subtracts one from their indices).
     *
     * @param p_index the index of the element to be removed
     * @return the element that was removed from the list
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public E remove(final int p_index)
    {
        final E l_old = super.remove(p_index);
        final ArrayList<IndexedItemIM<E>> l_list = new ArrayList<>(1);
        l_list.add(new IndexedItemIM<>(p_index, l_old));
        this.m_listener.fireAddRemoveListener(l_list, false);
        return l_old;
    }

    /**
     * Removes the first occurrence of the specified element from this list, if
     * it is present.  If the list does not contain the element, it is
     * unchanged.  More formally, removes the element with the lowest index
     * <tt>i</tt> such that <tt>(p_object==null&nbsp;?&nbsp;get(i)
     * ==null&nbsp;:&nbsp;p_object.equals(get(i)))</tt> (if such an element
     * exists). Returns <tt>true</tt> if this list contained the specified
     * element (or equivalently, if this list changed as a result of the call).
     *
     * @param p_old element to be removed from this list, if present
     * @return <tt>true</tt> if this list contained the specified element
     */
    @Override
    public boolean remove(final Object p_old)
    {
        final int l_index = super.indexOf(p_old);
        final E l_old = super.get(l_index);
        final ArrayList<IndexedItemIM<E>> l_list = new ArrayList<>(1);
        final boolean l_isRemoved = super.remove(p_old);
        l_list.add(new IndexedItemIM<>(l_index, l_old));
        this.m_listener.fireAddRemoveListener(l_list, false);
        return l_isRemoved;
    }

    /**
     * Removes all of the elements from this list.  The list will be empty after
     * this call returns.
     */
    @Override
    public void clear()
    {
        final ArrayList<IndexedItemIM<E>> l_list = new ArrayList<>(super.size());
        int l_counter = 0;
        for (final E l_currentElement : this) {
            l_list.add(new IndexedItemIM<>(l_counter, l_currentElement));
            l_counter++;
        }
        this.m_listener.fireAddRemoveListener(l_list, false);
        super.clear();
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's Iterator.  The behavior of this operation is undefined if
     * the specified collection is modified while the operation is in progress.
     * (This implies that the behavior of this call is undefined if the
     * specified collection is this list, and this list is nonempty.)
     *
     * @param p_collection collection containing elements to be added to this
     *                     list
     * @return <tt>true</tt> if this list changed as a result of the call
     *
     * @throws NullPointerException if the specified collection is null
     */
    @Override
    public boolean addAll(final Collection<? extends E> p_collection)
    {
        final ArrayList<IndexedItemIM<E>> l_list;
        l_list = new ArrayList<>(p_collection.size());
        final int l_startIndex = super.size();
        final boolean l_sucessAdding = super.addAll(p_collection);
        final E[] l_collection = (E[]) p_collection.toArray();
        for (int l_i = 0; l_i < l_collection.length; l_i++) {
            final int l_currentIndex = l_startIndex + l_i;
            l_list.add(new IndexedItemIM<>(l_currentIndex, l_collection[l_i]));
        }
        this.m_listener.fireAddRemoveListener(l_list, true);
        return l_sucessAdding;
    }

    /**
     * Inserts all of the elements in the specified collection into this list,
     * starting at the specified position. Shifts the element currently at that
     * position (if any) and any subsequent elements to the m_right (increases
     * their indices).  The new elements will appear in the list in the order
     * that they are returned by the specified collection's iterator.
     *
     * @param p_index      index at which to insert the m_first element from the
     *                     specified collection
     * @param p_collection collection containing elements to be added to this
     *                     list
     * @return <tt>true</tt> if this list changed as a result of the call
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws NullPointerException      if the specified collection is null
     */
    @Override
    public boolean addAll(
        final int p_index,
        final Collection<? extends E> p_collection)
    {
        final ArrayList<IndexedItemIM<E>> l_list;
        l_list = new ArrayList<>(p_collection.size());
        final boolean l_sucessAdding = super.addAll(p_index, p_collection);
        final E[] l_collection = (E[]) p_collection.toArray();
        for (int l_i = 0; l_i < l_collection.length; l_i++) {
            final int l_currentIndex = p_index + l_i;
            l_list.add(new IndexedItemIM<>(l_currentIndex, l_collection[l_i]));
        }
        this.m_listener.fireAddRemoveListener(l_list, true);
        return l_sucessAdding;
    }

    /**
     * Removes from this list all of its elements that are contained in the
     * specified collection.
     *
     * @param p_collection collection containing elements to be removed from
     *                     this list
     * @return {@code true} if this list changed as a result of the call
     *
     * @throws ClassCastException   if the class of an element of this list is
     *                              incompatible with the specified collection
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this list isOfChar a null element and the
     *                              specified collection does not permit null
     *                              elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>),
     *                              or if the specified collection is null
     * @see Collection#contains(Object)
     */
    @Override
    public boolean removeAll(final Collection<?> p_collection)
    {
        /**
         * Copied batchRemove and tried to get it working.
         * Why trying? Well, because everything important to do this task is
         * private.
         * Well, it is not possible without reflections.
         *
         */
        final ArrayList<IndexedItemIM<E>> l_elementData;
        l_elementData = new ArrayList<>(super.size());
        int l_superIndex = 0;
        try {
            for (; l_superIndex < super.size(); l_superIndex++) {
                if (p_collection.contains(super.get(l_superIndex))) {
                    l_elementData.add(new IndexedItemIM<>(l_superIndex, super.get(
                        l_superIndex)));
                }
            }
        } finally {

        }

        final boolean l_toReturn = super.removeAll(p_collection);
        if (!l_elementData.isEmpty()) {
            this.m_listener.fireAddRemoveListener(l_elementData, false);
        }

        return l_toReturn;
    }

    /**
     * Retains only the elements in this list that are contained in the
     * specified collection.  In other words, removes from this list all of its
     * elements that are not contained in the specified collection.
     *
     * @param p_collection collection containing elements to be retained in this
     *                     list
     * @return {@code true} if this list changed as a result of the call
     *
     * @throws ClassCastException   if the class of an element of this list is
     *                              incompatible with the specified collection
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if this list isOfChar a null element and the
     *                              specified collection does not permit null
     *                              elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>),
     *                              or if the specified collection is null
     * @see Collection#contains(Object)
     */
    @Override
    public boolean retainAll(final Collection<?> p_collection)
    {
        /**
         * Copied batchRemove and tried to get it working.
         * Why trying? Well, because everything important to do this task is
         * private.
         * Well, it is not possible without reflections.
         *
         */
        final ArrayList<IndexedItemIM<E>> l_elementData;
        l_elementData = new ArrayList<>(super.size());
        int l_superIndex = 0;
        try {
            for (; l_superIndex < super.size(); l_superIndex++) {
                if (!p_collection.contains(super.get(l_superIndex))) {
                    l_elementData.add(new IndexedItemIM<>(l_superIndex, super.get(
                        l_superIndex)));
                }
            }
        } finally {

        }

        final boolean l_toReturn = super.retainAll(p_collection);
        if (!l_elementData.isEmpty()) {
            this.m_listener.fireAddRemoveListener(l_elementData, false);
        }

        return l_toReturn;
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list. The specified
     * index indicates the m_first element that would be returned by an initial
     * call to {@link ListIterator#next next}. An initial call to {@link
     * ListIterator#previous previous} would return the element with the
     * specified index minus one.
     * <p>
     * <p>The returned list iterator is
     * <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @param p_index start position.
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @Override
    public @NotNull ListIterator<E> listIterator(final int p_index)
    {
        if ((p_index < 0) || (p_index > super.size())) {
            throw new IndexOutOfBoundsException("Index: " + p_index);
        }
        return new ListItr(p_index);
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     * <p>
     * <p>The returned list iterator is
     * <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @see #listIterator(int)
     */
    @Override
    public @NotNull ListIterator<E> listIterator()
    {
        return new ListItr(0);
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     * <p>
     * <p>The returned iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public @NotNull Iterator<E> iterator()
    {
        return new Itr();
    }

    /**
     * Returns a view of the portion of this list between the specified {@code
     * p_fromIndex}, inclusive, and {@code p_toIndex}, exclusive.  (If {@code
     * p_fromIndex} and {@code p_toIndex} are equal, the returned list is
     * empty.) The returned list is backed by this list, so non-structural
     * changes in the returned list are reflected in this list, and vice-versa.
     * The returned list supports all of the optional list operations.
     * <p>
     * <p>This method eliminates the need for explicit range operations (of the
     * sort that commonly exist for arrays). Any operation that expects a list
     * can be used as a range operation by passing a subList view instead of a
     * whole list.  For example, the following idiom removes a range of elements
     * from a list:
     * <pre>
     *      list.subList(from, to).clear();
     * </pre>
     * Similar idioms may be constructed for {@link #indexOf(Object)} and {@link
     * #lastIndexOf(Object)}, and all of the algorithms in the {@link
     * java.util.Collections} class can be applied to a subList.
     * <p>
     * <p>The semantics of the list returned by this method become undefined if
     * the backing list (i.e., this list) is <i>structurally modified</i> in any
     * way other than via the returned list.  (Structural modifications are
     * those that change the size of this list, or otherwise perturb it in such
     * a fashion that iterations in progress may yield incorrect results.)
     *
     * @param p_fromIndex m_first element
     * @param p_toIndex   last element
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws IllegalArgumentException  {@inheritDoc}
     */
    @Override
    public @NotNull List<E> subList(final int p_fromIndex, final int p_toIndex)
    {
        ObservableArrayList.subListRangeCheck(p_fromIndex, p_toIndex,
                                              super.size());
        return new SubList(this, 0, p_fromIndex, p_toIndex);
    }

    static void subListRangeCheck(
        final int p_fromIndex,
        final int p_toIndex,
        final int p_size)
    {
        if (p_fromIndex < 0) {
            throw new IndexOutOfBoundsException("fromIndex = " + p_fromIndex);
        }
        if (p_toIndex > p_size) {
            throw new IndexOutOfBoundsException("toIndex = " + p_toIndex);
        }
        if (p_fromIndex > p_toIndex) {
            throw new IllegalArgumentException(
                "fromIndex(" + p_fromIndex + ") > toIndex(" + p_toIndex + ")");
        }
    }

    @Override
    public void forEach(final Consumer<? super E> p_action)
    {
        super.forEach(p_action);
    }

    /**
     * Creates a <em><a href="Spliterator.html#binding">late-binding</a></em>
     * and <em>fail-fast</em> {@link Spliterator} over the elements in this
     * list.
     * <p>
     * <p>The {@code Spliterator} reports {@link Spliterator#SIZED}, {@link
     * Spliterator#SUBSIZED}, and {@link Spliterator#ORDERED}. Overriding
     * implementations should document the reporting of additional
     * characteristic values.
     *
     * @return a {@code Spliterator} over the elements in this list
     *
     * @since 1.8
     */
    @Override
    public Spliterator<E> spliterator()
    {
        return super.spliterator();
    }

    @Override
    public boolean removeIf(final Predicate<? super E> p_filter)
    {
        Objects.requireNonNull(p_filter);
        // figure out which elements are to be removed
        // any exception thrown from the filter predicate at this stage
        // will leave the collection unmodified
        final int l_expectedModCount = this.modCount;
        final int l_size = super.size();
        final ArrayList<IndexedItemIM<E>> l_removed = new ArrayList<>(l_size);
        for (int l_i = 0; l_i < l_size; l_i++) {
            @SuppressWarnings("unchecked") final E l_element = this.get(l_i);
            if (p_filter.test(l_element)) {
                l_removed.add(new IndexedItemIM<E>(l_i, this.get(l_i)));
            }
        }
        final boolean l_removeIfParent = super.removeIf(p_filter);

        if (l_removeIfParent) {
            this.m_listener.fireAddRemoveListener(l_removed, false);
        }

        return l_removeIfParent;
    }

    @Override
    public void replaceAll(final UnaryOperator<E> p_operator)
    {

        Objects.requireNonNull(p_operator);
        final int l_size = super.size();
        for (int l_i = 0; l_i < l_size; l_i++) {
            this.set(l_i, p_operator.apply(this.get(l_i)));
        }
    }

    @Override
    public void sort(final Comparator<? super E> p_comparator)
    {
//        final int l_expectedModCount = this.modCount;
//        Arrays.sort((E[]) this.elementData, 0, this.size, c);
//        if (this.modCount != l_expectedModCount) {
//            throw new ConcurrentModificationException();
//        }
//        this.modCount++;
        throw new NotImplementedException("Not implemented yet");
        // Todo implement removeAll.
//        super.sort(p_comparator);
    }

    public final @NotNull ListenerContainer<E> getListener()
    {
        return this.m_listener;
    }

    /**
     * Returns a sequential {@code Stream} with this collection as its source.
     * <p>
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE}, {@code
     * CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()} for
     * details.)
     *
     * @return a sequential {@code Stream} over the elements in this collection
     *
     * @since 1.8
     */
    @Override
    public Stream<E> stream()
    {
        return super.stream();
    }

    /**
     * Returns a possibly parallel {@code Stream} with this collection as its
     * source.  It is allowable for this method to return a sequential stream.
     * <p>
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE}, {@code
     * CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()} for
     * details.)
     *
     * @return a possibly parallel {@code Stream} over the elements in this
     * collection
     *
     * @since 1.8
     */
    @Override
    public Stream<E> parallelStream()
    {
        return super.parallelStream();
    }    /**
     * {@inheritDoc}
     * <p>
     * <p>This implementation iterates over the specified collection, checking
     * each element returned by the iterator in turn to see if it's contained in
     * this collection.  If all elements are so contained <tt>true</tt> is
     * returned, otherwise <tt>false</tt>.
     *
     * @param p_collection
     * @throws ClassCastException   {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see #contains(Object)
     */
    @Override
    public boolean containsAll(final @NotNull Collection<?> p_collection)
    {
        return super.containsAll(p_collection);
    }

    @Override
    public void addListener(final IAddRemoveManyListener<E> p_listener)
    {
        this.m_listener.addListener(p_listener);
    }

    @Override
    public void removeListener(final IAddRemoveManyListener<E> p_listener)
    {
        this.m_listener.removeListener(p_listener);
    }

    @Override
    public void addChangeListener(
        final ISingleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.addChangeListener(p_listener);
    }

    @Override
    public void removeChangeListener(
        final ISingleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.removeChangeListener(p_listener);
    }

    /**
     * An optimized version of AbstractList.Itr
     */
    private class Itr
        implements Iterator<E>
    {
        int m_cursor;       // index of next element to return
        int m_lastRet = -1;// index of last element returned; -1 if no such
        int m_expectedModCount = ObservableArrayList.super.modCount;

        @Override
        public boolean hasNext()
        {
            return this.m_cursor != ObservableArrayList.this.size();
        }

        @Override
        @SuppressWarnings("unchecked")
        public E next()
        {
            this.checkForComodification();
            final int l_i = this.m_cursor;
            if (l_i >= ObservableArrayList.this.size()) {
                throw new NoSuchElementException();
            }
            this.m_cursor = l_i + 1;
            return ObservableArrayList.this.get(this.m_lastRet = l_i);
        }

        @Override
        public void remove()
        {
            if (this.m_lastRet < 0) {
                throw new IllegalStateException();
            }
            this.checkForComodification();

            try {
                ObservableArrayList.this.remove(this.m_lastRet);
                this.m_cursor = this.m_lastRet;
                this.m_lastRet = -1;
                this.m_expectedModCount = ObservableArrayList.super.modCount;
            } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public void forEachRemaining(final Consumer<? super E> p_consumer)
        {
            Objects.requireNonNull(p_consumer);
            final int l_size = ObservableArrayList.super.size();
            int l_i = this.m_cursor;
            if (l_i >= l_size) {
                return;
            }
            while ((l_i != l_size) && (ObservableArrayList.this.modCount
                == this.m_expectedModCount)) {
                p_consumer.accept(ObservableArrayList.this.get(l_i));
                l_i++;
            }
            // update once at end of iteration to reduce heap write traffic
            this.m_cursor = l_i;
            this.m_lastRet = l_i - 1;
            this.checkForComodification();
        }

        final void checkForComodification()
        {
            if (ObservableArrayList.this.modCount != this.m_expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * An optimized version of AbstractList.ListItr
     */
    private class ListItr
        extends ObservableArrayList<E>.Itr
        implements ListIterator<E>
    {
        ListItr(final int p_index)
        {
            this.m_cursor = p_index;
        }

        @Override
        public E next()
        {
            return super.next();
        }

        @Override
        public boolean hasPrevious()
        {
            return this.m_cursor != 0;
        }

        @Override
        public int nextIndex()
        {
            return this.m_cursor;
        }

        @Override
        public int previousIndex()
        {
            return this.m_cursor - 1;
        }

        @Override
        @SuppressWarnings("unchecked")
        public E previous()
        {
            this.checkForComodification();
            final int l_i = this.m_cursor - 1;
            if (l_i < 0) {
                throw new NoSuchElementException();
            }
            this.m_cursor = l_i;
            return ObservableArrayList.this.get(this.m_lastRet = l_i);
        }

        @Override
        public void set(final E p_e)
        {
            if (this.m_lastRet < 0) {
                throw new IllegalStateException();
            }
            this.checkForComodification();

            try {
                ObservableArrayList.this.set(this.m_lastRet, p_e);
            } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(final @NotNull E p_e)
        {
            this.checkForComodification();

            try {
                final int l_i = this.m_cursor;
                ObservableArrayList.this.add(l_i, p_e);
                this.m_cursor = l_i + 1;
                this.m_lastRet = -1;
                this.m_expectedModCount = ObservableArrayList.this.modCount;
            } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                throw new ConcurrentModificationException();
            }
        }
    }

    private class SubList
        extends AbstractList<E>
        implements RandomAccess
    {
        private final AbstractList<E> m_parent;
        private final int             m_parentoffset;
        private final int             m_offset;
        int m_size;

        SubList(
            final AbstractList<E> p_parent,
            final int p_offset,
            final int p_fromIndex,
            final int p_toIndex)
        {
            this.m_parent = p_parent;
            this.m_parentoffset = p_fromIndex;
            this.m_offset = p_offset + p_fromIndex;
            this.m_size = p_toIndex - p_fromIndex;
        }

        @Override
        public E get(final int p_index)
        {
            this.rangeCheck(p_index);
            this.checkForComodification();
            return ObservableArrayList.this.get(this.m_offset + p_index);
        }

        @Override
        public E set(final int p_index, final E p_element)
        {
            this.rangeCheck(p_index);
            this.checkForComodification();
            return this.m_parent.set(this.m_offset + p_index, p_element);
        }

        @Override
        public void add(final int p_index, final E p_element)
        {
            this.rangeCheckForAdd(p_index);
            this.checkForComodification();
            this.m_parent.add(this.m_parentoffset + p_index, p_element);
            this.m_size++;
        }

        @Override
        public E remove(final int p_index)
        {
            this.rangeCheck(p_index);
            this.checkForComodification();
            final E l_result =
                this.m_parent.remove(this.m_parentoffset + p_index);
            this.m_size--;
            return l_result;
        }

        @Override
        public boolean addAll(
            final int p_index,
            final Collection<? extends E> p_collection)
        {
            this.rangeCheckForAdd(p_index);
            final int l_cSize = p_collection.size();
            if (l_cSize == 0) {
                return false;
            }

            this.checkForComodification();
            this.m_parent.addAll(this.m_parentoffset + p_index, p_collection);
            this.m_size += l_cSize;
            return true;
        }

        @Override
        public @NotNull Iterator<E> iterator()
        {
            return this.listIterator();
        }

        @Override
        public @NotNull ListIterator<E> listIterator(final int p_index)
        {
            this.checkForComodification();
            this.rangeCheckForAdd(p_index);
            final int l_offset = this.m_offset;

            return new ListIterator<E>()
            {
                int m_cursor = p_index;
                int m_lastRet = -1;

                @Override
                public boolean hasNext()
                {
                    return this.m_cursor
                        != ObservableArrayList.SubList.this.m_size;
                }

                @Override
                @SuppressWarnings("unchecked")
                public E next()
                {
                    this.checkForComodification();
                    final int l_i = this.m_cursor;
                    if (l_i >= ObservableArrayList.SubList.this.m_size) {
                        throw new NoSuchElementException();
                    }
                    this.m_cursor = l_i + 1;
                    return ObservableArrayList.SubList.this.get(
                        l_offset + (this.m_lastRet = l_i));
                }

                void checkForComodification()
                {
//                    if (this.m_expectedModCount != ObservableArrayList
// .super.modCount) {
//                        throw new ConcurrentModificationException();
//                    }
                }

                @Override
                public boolean hasPrevious()
                {
                    return this.m_cursor != 0;
                }

                @Override
                @SuppressWarnings("unchecked")
                public E previous()
                {
                    this.checkForComodification();
                    final int l_i = this.m_cursor - 1;
                    if (l_i < 0) {
                        throw new NoSuchElementException();
                    }
                    this.m_cursor = l_i;
                    return ObservableArrayList.SubList.this.get(
                        l_offset + (this.m_lastRet = l_i));
                }

                @Override
                @SuppressWarnings("unchecked")
                public void forEachRemaining(
                    final Consumer<? super E> p_consumer)
                {
                    Objects.requireNonNull(p_consumer);
                    final int l_size = ObservableArrayList.SubList.this.m_size;
                    int l_i = this.m_cursor;
                    if (l_i >= l_size) {
                        return;
                    }
                    while (l_i != l_size) {
                        p_consumer.accept(ObservableArrayList.SubList.this.get(
                            l_offset + (l_i)));
                        l_i++;
                    }
                    // update once at end of iteration to reduce heap write
                    // traffic
                    this.m_lastRet = this.m_cursor = l_i;
                    this.checkForComodification();
                }

                @Override
                public int nextIndex()
                {
                    return this.m_cursor;
                }

                @Override
                public int previousIndex()
                {
                    return this.m_cursor - 1;
                }

                @Override
                public void remove()
                {
                    if (this.m_lastRet < 0) {
                        throw new IllegalStateException();
                    }
                    this.checkForComodification();

                    try {
                        ObservableArrayList.SubList.this.remove(this.m_lastRet);
                        this.m_cursor = this.m_lastRet;
                        this.m_lastRet = -1;
                    } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                        throw new ConcurrentModificationException();
                    }
                }

                @Override
                public void set(final E p_e)
                {
                    if (this.m_lastRet < 0) {
                        throw new IllegalStateException();
                    }
                    this.checkForComodification();

                    try {
                        ObservableArrayList.super.set(l_offset + this.m_lastRet,
                                                      p_e);
                    } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                        throw new ConcurrentModificationException();
                    }
                }

                @Override
                public void add(final E p_e)
                {
                    this.checkForComodification();

                    try {
                        final int l_i = this.m_cursor;
                        ObservableArrayList.SubList.this.add(l_i, p_e);
                        this.m_cursor = l_i + 1;
                        this.m_lastRet = -1;
                    } catch (final @NotNull IndexOutOfBoundsException l_ex) {
                        throw new ConcurrentModificationException();
                    }
                }


            };
        }

        @Override
        public @NotNull List<E> subList(final int fromIndex, final int toIndex)
        {
            ObservableArrayList.subListRangeCheck(fromIndex, toIndex,
                                                  this.m_size);
            return new ObservableArrayList.SubList(this, this.m_offset,
                                                   fromIndex, toIndex);
        }        @Override
        public boolean addAll(final Collection<? extends E> p_collection)
        {
            return this.addAll(this.m_size, p_collection);
        }

        @Override
        protected void removeRange(final int fromIndex, final int toIndex)
        {
//            this.checkForComodification();
//            this.m_parent.removeRange(this.m_parentoffset + fromIndex,
//                    this.m_parentoffset + toIndex);
//            this.m_size -= toIndex - fromIndex;
            throw new NotImplementedException("This simply does not work "
                                                  + "because of stupid java "
                                                  + "access restriction...");
        }

        private void rangeCheck(final int index)
        {
            if ((index < 0) || (index >= this.m_size)) {
                throw new IndexOutOfBoundsException(this.outOfBoundsMsg(index));
            }
        }

        private void checkForComodification()
        {
        }

        private String outOfBoundsMsg(final int index)
        {
            return "Index: " + index + ", Size: " + this.m_size;
        }

        @Override
        public int size()
        {
            this.checkForComodification();
            return this.m_size;
        }

        @Override
        public @NotNull Spliterator<E> spliterator()
        {
            // TODO well, just that what's standing inside the error...
            throw new NotImplementedException(
                "Not implemented yet. Need to " + "implement "
                    + "ArrayListSpliterator" + " because of javas "
                    + "creat wonderful " + "beautiful well "
                    + "thought visibiltx " + "thingies...");
//            this.checkForComodification();
//            return new ObservableArrayList.ArrayListSpliterator<E>
//                    (ObservableArrayList.super,
//                    this.m_offset, this.m_offset + this.m_size, this
// .modCount);
        }

        private void rangeCheckForAdd(final int index)
        {
            if ((index < 0) || (index > this.m_size)) {
                throw new IndexOutOfBoundsException(this.outOfBoundsMsg(index));
            }
        }




    }




}
