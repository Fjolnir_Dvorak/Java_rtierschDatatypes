/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead
import de.rtiersch.datatypes.notnull.shared.IIntegerRead

/**
 * Getter for all the informations avaiable from a cell.
 *
 * @param <E> Content type
</E> */
interface ICell<E> {

    /**
     * The index of the row the cell is part of.
     *
     * @return Row index.
     */
    val indexRow: IIntegerRead

    /**
     * The index of the column the cell is part of.
     *
     * @return Column index.
     */
    val indexColumn: IIntegerRead

    /**
     * All the informations of the row.
     *
     * @return Row information container.
     */
    val rowInformation: IInfoRowRead<E>

    /**
     * All the informations of the column.
     *
     * @return Column information container.
     */
    val columnInformation: IInfoColumnRead<E>

    /**
     * Returns the content of the cell.
     *
     * @return Content this cell is managing.
     */
    val content: E

    /**
     * Copies a column with new indices.
     * @param columnInfo column index.
     * @param rowInfo row index.
     */
    fun copy(rowInfo: IInfoRowRead<E>,
             columnInfo: IInfoColumnRead<E>): ICellSet<E>
}
