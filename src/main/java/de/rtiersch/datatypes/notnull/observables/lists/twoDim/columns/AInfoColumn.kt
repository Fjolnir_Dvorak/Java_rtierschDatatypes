/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns

import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.exception.AlreadyInitializedException
import de.rtiersch.exception.NotYetInitializedException

/**
 * Makes sure that the ColumnInfo knows its row index and its parent to be able
 * to access the cells if needed (Column specific data validation).

 * @param <E> The content type of the column.
</E> */
open class AInfoColumn<E>
protected constructor(
        /**
         * Index of this column.
         */
        override val index: IIntegerRead) : IInfoColumnRead<E> {
    /**
     * Parent opbject. Can be used by children to access the cells for
     * column-specific validation purposes.
     */
    private var columnNullable: IColumn<E>? = null

    /**
     * Lazy final init of columnNullable.

     * @param p_column The parent column.
     */
    fun registerColumn(p_column: IColumn<E>) {
        if (this.columnNullable != null) {
            throw AlreadyInitializedException()
        }
        this.columnNullable = p_column
    }

    override val column: IColumn<E>
        get() {
            if (this.columnNullable == null) {
                throw NotYetInitializedException()
            }
            return this.columnNullable as IColumn<E>
        }
}
