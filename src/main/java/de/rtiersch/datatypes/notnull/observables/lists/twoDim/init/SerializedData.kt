/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.init

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.util.annotation.API
import java.util.*

/**
 * Raw data, extracted into a arrays. The table data has the orientation
 * RowColumn.
 * @param <E> Content type.
</E> */
data class SerializedData<E>(val headerColumns: Array<String>,
                             val headerRows: Array<String>,
                             val table: Array<Array<E>>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SerializedData<*>

        if (!Arrays.equals(headerColumns, other.headerColumns)) return false
        if (!Arrays.equals(headerRows, other.headerRows)) return false
        if (!Arrays.equals(table, other.table)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = Arrays.hashCode(headerColumns)
        result = 31 * result + Arrays.hashCode(headerRows)
        result = 31 * result + Arrays.hashCode(table)
        return result
    }
}

@JvmField val NO_HEADER = emptyArray<String>()
@API
@JvmField val ORIENTATION = EOrientation.RowColumn
