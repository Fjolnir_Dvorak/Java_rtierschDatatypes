/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IHasListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead
import de.rtiersch.datatypes.notnull.shared.IIntegerRead

/**
 * Default Cell which can be used for InformationList.

 * @param <E> Content Type. Only String is tested.
</E> */
open class DefICell<E>(p_infoRow: IInfoRowRead<E>,
                       p_infoColumn: IInfoColumnRead<E>,
                       p_listener: IHasListenerCellChanged<E>,
                       p_content: E) : ICellSet<E> {

    /**
     * Copies a column with new indices.
     * @param columnInfo column index.
     * @param rowInfo row index.
     */
    override fun copy(rowInfo: IInfoRowRead<E>,
                      columnInfo: IInfoColumnRead<E>): ICellSet<E> {
        return DefICell(rowInfo, columnInfo, m_listener, content)
    }

    /**
     * Information container for the row containing the index.
     */
    override val rowInformation = p_infoRow
    /**
     * Information container for the column containing the index.
     */
    override val columnInformation = p_infoColumn
    /**
     * Shared listener container for all cells. Only stored here to keep the
     * reference for adding new objects that want to listen.
     */
    protected val m_listener = p_listener
    /**
     * The content.
     */
    override var content = p_content

    override val indexRow: IIntegerRead
        get() = this.rowInformation.index

    override val indexColumn: IIntegerRead
        get() = this.columnInformation.index
}
