package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.AInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Column
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellG
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellSetG
import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.datatypes.notnull.shared.StringContainer
import java.util.*

open class ColumnUI<E>(cells: ArrayList<ICellSet<E>>,
                       headerInt: ArrayList<StringContainer>,
                       shadowedInfo: AInfoColumn<E>,
                       index: IIntegerRead) : Column<E>(cells = cells,
    headerInt = headerInt, shadowedInfo = shadowedInfo,
    index = index), IColumnUI<E> {
    override fun getCell(p_row: Int): ICellSetG<E> {
        return super.getCell(p_row) as ICellSetG<E>
    }

    override fun iterator(): Iterator<ICellG<E>> {
        return ColumnIteratorUI(this) as Iterator<ICellG<E>>
    }


    protected open inner class ColumnIteratorUI(m_iterateThrough:
                                                 ColumnUI<E>) : ColumnIterator(
        m_iterateThrough) {
        override fun next(): ICellG<E> {
            return super.next() as ICellG<E>
        }
    }
}
