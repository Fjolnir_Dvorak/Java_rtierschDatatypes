package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Column
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Columns
import java.util.*

open class ColumnsUI<E>(m_columns: ArrayList<Column<E>>) : Columns<E>(
    m_columns), IColumnsUI<E> {
    override fun getColumn(p_column: Int): ColumnUI<E> {
        return super.getColumn(p_column) as ColumnUI<E>
    }

    override fun iterator(): Iterator<IColumnUI<E>> {
        return ColumnsIteratorUI(this) as Iterator<IColumnUI<E>>
    }

    protected open inner class ColumnsIteratorUI(m_iterateThrough:
                                                 ColumnsUI<E>) : ColumnsIterator(
        m_iterateThrough) {
        override fun next(): IColumnUI<E> {
            return super.next() as IColumnUI<E>
        }
    }
}
