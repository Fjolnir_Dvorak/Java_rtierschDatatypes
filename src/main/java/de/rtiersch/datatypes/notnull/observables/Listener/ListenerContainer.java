/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.Listener;

import de.rtiersch.datatypes.notnull.container.IndexedItemIM;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ListenerContainer<T>
    implements IHasChangeListener<T>, IHasSingleIndexedChangeListener<T>,
               IHasDoubleIndexedChangeListener<T>, IHasAddRemoveListener<T>
{

    private final @Nullable ArrayList<IChangeListener<T>> m_changeListeners;
    private final @Nullable ArrayList<ISingleIndexedChangeListener<T>>
                                                          m_singleIndexedChangeListeners;
    private final @Nullable ArrayList<IDoubleIndexedChangeListener<T>>
                                                          m_doubleIndexedChangeListeners;
    private final @Nullable ArrayList<IAddRemoveManyListener<T>>
                                                          m_addRemoveManyListeners;


    public ListenerContainer(final EListener... p_listener)
    {
        boolean l_hasChange = false;
        boolean l_hasSingle = false;
        boolean l_hasDouble = false;
        boolean l_hasAddRemoveMany = false;
        for (final EListener l_listener : p_listener) {
            switch (l_listener) {
                case CHANGE:
                    l_hasChange = true;
                    break;
                case SINGLE_INDEXED_CHANGE:
                    l_hasSingle = true;
                    break;
                case DOUBLE_INDEXED_CHANGE:
                    l_hasDouble = true;
                    break;
                case ADD_REMOVE_MANY:
                    l_hasAddRemoveMany = true;
                    break;
            }
        }
        if (l_hasChange) {
            this.m_changeListeners = new ArrayList<>(2);
        } else {
            this.m_changeListeners = null;
        }
        if (l_hasSingle) {
            this.m_singleIndexedChangeListeners = new ArrayList<>(2);
        } else {
            this.m_singleIndexedChangeListeners = null;
        }
        if (l_hasDouble) {
            this.m_doubleIndexedChangeListeners = new ArrayList<>(2);
        } else {
            this.m_doubleIndexedChangeListeners = null;
        }
        if (l_hasAddRemoveMany) {
            this.m_addRemoveManyListeners = new ArrayList<>(2);
        } else {
            this.m_addRemoveManyListeners = null;
        }
    }


    //<editor-fold desc="IChangeListener">
    @Override
    public void addChangeListener(final IChangeListener<T> p_changeListener)
    {
        if (this.m_changeListeners != null) {
            this.m_changeListeners.add(p_changeListener);
        }
    }

    @Override
    public void removeChangeListener(final IChangeListener<T> p_changeListener)
    {
        if (this.m_changeListeners != null) {
            this.m_changeListeners.remove(p_changeListener);
        }
    }

    public void fireChangeListener(final T p_old, final T p_new)
    {
        if (this.m_changeListeners != null) {
            for (final IChangeListener<T> l_listener : this.m_changeListeners) {
                l_listener.onChange(p_old, p_new);
            }
        }
    }

    //</editor-fold>
    //<editor-fold desc="ISingleIndexedChangeListener">
    @Override
    public void addChangeListener(
        final ISingleIndexedChangeListener<T> p_changeListener)
    {
        if (this.m_singleIndexedChangeListeners != null) {
            this.m_singleIndexedChangeListeners.add(p_changeListener);
        }
    }

    @Override
    public void removeChangeListener(
        final ISingleIndexedChangeListener<T> p_changeListener)
    {
        if (this.m_singleIndexedChangeListeners != null) {
            this.m_singleIndexedChangeListeners.remove(p_changeListener);
        }
    }

    public void fireChangeListener(
        final int p_index, final T p_old, final T p_new)
    {
        if (this.m_singleIndexedChangeListeners != null) {
            for (final ISingleIndexedChangeListener<T> l_listener : this
                .m_singleIndexedChangeListeners) {
                l_listener.onChange(p_index, p_old, p_new);
            }
        }
    }

    //</editor-fold>
    //<editor-fold desc="IDoubleIndexedChangeListener">
    @Override
    public void addChangeListener(
        final IDoubleIndexedChangeListener<T> p_changeListener)
    {
        if (this.m_doubleIndexedChangeListeners != null) {
            this.m_doubleIndexedChangeListeners.add(p_changeListener);
        }
    }

    @Override
    public void removeChangeListener(
        final IDoubleIndexedChangeListener<T> p_changeListener)
    {
        if (this.m_doubleIndexedChangeListeners != null) {
            this.m_doubleIndexedChangeListeners.remove(p_changeListener);
        }
    }

    /**
     * @param p_x
     * @param p_y
     * @param p_old
     * @param p_new
     */
    public void fireChangeListener(
        final int p_x, final int p_y, final T p_old, final T p_new)
    {
        if (this.m_doubleIndexedChangeListeners != null) {
            for (final IDoubleIndexedChangeListener<T> l_listener : this
                .m_doubleIndexedChangeListeners) {
                l_listener.onChange(p_x, p_y, p_old, p_new);
            }
        }
    }

    //</editor-fold>
    //<editor-fold desc="IAddRemoveManyListener">
    @Override
    public void addListener(final IAddRemoveManyListener<T> p_removeListener)
    {
        if (this.m_addRemoveManyListeners != null) {
            this.m_addRemoveManyListeners.add(p_removeListener);
        }
    }

    @Override
    public void removeListener(
        final IAddRemoveManyListener<T> p_removeListener)
    {
        if (this.m_addRemoveManyListeners != null) {
            this.m_addRemoveManyListeners.remove(p_removeListener);
        }
    }

    public void fireAddRemoveListener(
        final @NotNull List<IndexedItemIM<T>> p_elements, final boolean p_isNew)
    {
        if (this.m_addRemoveManyListeners != null) {
            for (final IAddRemoveManyListener<T> l_listener : this
                .m_addRemoveManyListeners) {
                l_listener.onChange(p_elements, p_isNew);
            }
        }
    }
    //</editor-fold>
}
