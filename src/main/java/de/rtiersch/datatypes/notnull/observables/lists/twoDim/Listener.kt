/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IFireListenerAddRemoveColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IFireListenerAddRemoveRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IFireListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerAddRemoveColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerAddRemoveRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.listener.IFireListenerCellChangedColour
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.listener.IListenerCellChangedColour
import javafx.scene.paint.Color
import java.util.*

class Listener<E> : IFireListenerAddRemoveColumn<E>, IFireListenerAddRemoveRow<E>, IFireListenerCellChanged<E>, IFireListenerCellChangedColour<E> {

    internal val m_listenerCellChanged: ArrayList<IListenerCellChanged<E>> = ArrayList(2)
    internal val m_listenerCellChangedColour: ArrayList<IListenerCellChangedColour<E>> = ArrayList(2)
    internal val m_listenerRow: ArrayList<IListenerAddRemoveRow<E>> = ArrayList(2)
    internal val m_listenerColumn: ArrayList<IListenerAddRemoveColumn<E>> = ArrayList(2)

    //<editor-fold desc="Listener">
    //<editor-fold desc="IHasListenerCellChanged">
    override fun addListenerCellChanged(p_listener: IListenerCellChanged<E>) {
        this.m_listenerCellChanged.add(p_listener)
    }

    override fun removeListenerCellChanged(p_listener: IListenerCellChanged<E>) {
        this.m_listenerCellChanged.remove(p_listener)
    }

    //</editor-fold>
    //<editor-fold desc="IHasListenerCellChangedColour">
    override fun addListenerCellChangedColour(p_listener: IListenerCellChangedColour<E>) {
        this.m_listenerCellChangedColour.add(p_listener)
    }

    override fun removeListenerCellChangedColour(p_listener: IListenerCellChangedColour<E>) {
        this.m_listenerCellChangedColour.remove(p_listener)
    }

    //</editor-fold>
    //<editor-fold desc="IHasListenerAddRemoveRow">
    override fun addListenerAddRemoveRow(p_listener: IListenerAddRemoveRow<E>) {
        this.m_listenerRow.add(p_listener)
    }

    override fun removeListenerAddRemoveRow(p_listener: IListenerAddRemoveRow<E>) {
        this.m_listenerRow.remove(p_listener)
    }

    //</editor-fold>
    //<editor-fold desc="IHasListenerAddRemoveColumn">
    override fun addListenerAddRemoveColumn(p_listener: IListenerAddRemoveColumn<E>) {
        this.m_listenerColumn.add(p_listener)
    }

    override fun removeListenerAddRemoveColumn(p_listener: IListenerAddRemoveColumn<E>) {
        this.m_listenerColumn.remove(p_listener)
    }

    //</editor-fold>
    //</editor-fold>
    //<editor-fold desc="FireEvents">
    override fun fireIListenerCellChanged(p_cell: ICell<E>,
                                          p_old: E,
                                          p_new: E) {
        for (l_listener in this.m_listenerCellChanged) {
            l_listener.doOnIListenerCellChanged(p_cell, p_old, p_new)
        }
    }

    override fun fireIListenerCellChangedColour(p_cell: ICell<E>,
                                                p_old: Color,
                                                p_new: Color) {
        for (l_listener in this.m_listenerCellChangedColour) {
            l_listener.doOnIListenerCellChangedColour(p_cell, p_old, p_new)
        }
    }

    override fun fireIListenerAddRemoveRow(p_row: IRow<E>,
                                           p_index: Int,
                                           p_operation: EStatus) {
        for (l_listener in this.m_listenerRow) {
            l_listener.doOnIListenerAddRemoveRow(p_row, p_index, p_operation)
        }

    }

    override fun fireIListenerAddRemoveColumn(p_column: IColumn<E>,
                                              p_index: Int,
                                              p_operation: EStatus) {
        for (l_listener in this.m_listenerColumn) {
            l_listener.doOnIListenerAddRemoveColumn(p_column, p_index,
                    p_operation)
        }
    }
    //</editor-fold>

}
