/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.Listener;

import de.rtiersch.datatypes.notnull.observables.ObservableValue2D;
import de.rtiersch.datatypes.notnull.observables.lists.IListFixedSize;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ListenerContainerEXTTable<E>
    extends ListenerContainer<E>
    implements IHasTableChangeListener<E>
{

    private final @NotNull ArrayList<IListAddListener<E>>    m_rowAddListener;
    private final @NotNull ArrayList<IListRemoveListener<E>>
                                                             m_rowRemoveListener;
    private final @NotNull ArrayList<IListAddListener<E>>
                                                             m_columnAddListener;
    private final @NotNull ArrayList<IListRemoveListener<E>>
                                                             m_columnRemoveListener;


    public ListenerContainerEXTTable(final EListener... p_listener)
    {
        super(p_listener);
        this.m_columnAddListener = new ArrayList<>(2);
        this.m_columnRemoveListener = new ArrayList<>(2);
        this.m_rowAddListener = new ArrayList<>(2);
        this.m_rowRemoveListener = new ArrayList<>(2);
    }

    //<editor-fold desc="IRowAddedListener">
    @Override
    public void addRowAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_rowAddListener.add(p_listener);
    }

    @Override
    public void removeRowAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_rowAddListener.remove(p_listener);
    }

    //</editor-fold>
    //<editor-fold desc="IRowRemovedListener">
    @Override
    public void addRowRemovedListener(final IListRemoveListener<E> p_listener)
    {
        this.m_rowRemoveListener.add(p_listener);
    }

    @Override
    public void removeRowRemovedListener(final IListRemoveListener<E>
                                                 p_listener)
    {
        this.m_rowRemoveListener.remove(p_listener);
    }

    @Override
    public void addColumnAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_columnAddListener.add(p_listener);
    }

    @Override
    public void removeColumnAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_columnAddListener.remove(p_listener);
    }

    //</editor-fold>
    //<editor-fold desc="IColumnAddedListener">

    @Override
    public void addColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_columnRemoveListener.add(p_listener);
    }

    @Override
    public void removeColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_columnRemoveListener.add(p_listener);
    }

    public void fireRowAddedListener(
        final @NotNull IListFixedSize<ObservableValue2D<E>> p_items,
        final int p_index)
    {
        for (final IListAddListener<E> l_listener : this.m_rowAddListener) {
            l_listener.onAdd(p_items, p_index);
        }
    }

    //</editor-fold>
    //<editor-fold desc="IColumnRemovedListener">

    public void fireRowRemovedListener(
        final @NotNull IListFixedSize<ObservableValue2D<E>> p_items,
        final int p_index)
    {
        for (final IListRemoveListener<E> l_listener : this
            .m_rowRemoveListener) {
            l_listener.onRemove(p_items, p_index);
        }
    }

    public void fireColumnAddedListener(
        final @NotNull IListFixedSize<ObservableValue2D<E>> p_items,
        final int p_index)
    {
        for (final IListAddListener<E> l_listener : this.m_columnAddListener) {
            l_listener.onAdd(p_items, p_index);
        }
    }

    public void fireColumnRemovedListener(
        final @NotNull IListFixedSize<ObservableValue2D<E>> p_items,
        final int p_index)
    {
        for (final IListRemoveListener<E> l_listener : this.m_columnRemoveListener) {
            l_listener.onRemove(p_items, p_index);
        }
    }

    //</editor-fold>
}
