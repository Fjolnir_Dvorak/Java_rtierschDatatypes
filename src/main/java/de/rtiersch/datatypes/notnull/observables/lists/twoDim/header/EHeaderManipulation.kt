/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.header

/**
 * These are the possible implementation how to handle headers on new
 * inserted or copied rows and columns.
 */
enum class EHeaderManipulation {
    /**
     * Only uses the by the user provided header.
     */
    CustomHeader,
    /**
     * Reassign automatic given headers, shifting the header to the left if
     * removed and shifting all following headers to the right if added. Does
     * not work with only user given headers. Will be deactivated if the user
     * sets header manually.
     */
    Reassign,
    /**
     * Creates a "copy-counter".
     * Example:
     * A | B | C  <-- Insert after B
     * A | B | B-2 | C
     */
    MarkAsCopy,
    /**
     * Duplicates the header from the original if no other heade is given. If
     * not a copy, Header will be equal to configruation CustomHeader.
     */
    Duplicate
}
