package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IColumns

interface IColumnsUI<E> : IColumns<E> {
    override fun getColumn(p_column: Int): IColumnUI<E>
    override fun iterator(): Iterator<IColumnUI<E>>
}
