/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows

/**
 * A named collection of Rows.

 * @param <E> The content type of the row.
</E> */
interface IRows<E> : Iterable<IRow<E>> {
    /**
     * Returns the Row number p_row.

     * @param p_row The index of the row.
     * *
     * @return The row at index p_row.
     */
    fun getRow(p_row: Int): IRow<E>

    /**
     * The size of the row, alias the amount of columns.
     * @return Cells in the row.
     */
    fun size(): Int
}
