/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.InformationList
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.ICreateAInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderInit
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderManipulation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.ICreateAInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellG
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellSetG
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICreateICellG
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns.IColumnUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns.IColumnsUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.init.InitListUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.listener.IHasListenerCellChangedColour
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.listener.IListenerCellChangedColour
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows.IRowUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows.IRowsUI
import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import javafx.scene.paint.Color

/**
 * InformationList is a Table (two dimensional arraylists with synchronized
 * length). The cells with type E are wrapped by ICellG (default implementation
 * DefICellGui) which need to be created by the user using the constructor-class
 * ICreateICell. The tabledata is accessible through row-based and column-based
 * indexing. To prevent a painfull braindeath caused by missunderstood
 * XY-indexing systems the data is no longer accessible as XY or YX based table.
 * Java and math have a quite opposit understanding on how to index a table. The
 * input array used to create a new table are not further used, modificated or
 * referred to. The containing data will be wrapped into ICellG to let the user
 * decide on how to handle data copying. IMPORTANT: Not tested against multi
 * threading/processing There are three registerable listeners:
 * IListenerAddRemoveColumn, IListenerAddRemoveRow and IListenerCellChanged.
 *
 * Creates a new two dimensional arrayList with normed row- and column
 * length. Missing fields and null values will throw an error. I am really
 * sorry 'bout that, but did you ever tried to display a null?

 * @param p_elements array to observe (do not observe the array,
 *                               only the objects itself).
 * @param p_orientation The index orientation of p_elements.
 * @param p_createCell Cell Constructor. Returns a unique cell for
 *                               each element.
 * @param p_createInfoRow Information container for the rows.
 * @param p_createInfoColumn Information container for the columns.
 * @param p_firstLineAsHeaderH Is the first row of the input a header?
 * @param p_firstLineAsHeaderV Is the first column of the input a header?
 * @throws ArrayNotRectangularException Input array is not rectangular.
 *                                        Please use something like
 *                                        ArrayUtils.rectify()
 * @throws NullPointerException NotNull datatype. Array contains
 *                                      null values.
 */
@API
class InformationListUI<E>
@Throws(ArrayNotRectangularException::class,
    NullPointerException::class) constructor(p_elements: Array<Array<E>>,
                                             p_orientation: EOrientation,
                                             p_createCell: ICreateICellG<E>,
                                             p_createInfoRow: ICreateAInfoRow<E>,
                                             p_createInfoColumn: ICreateAInfoColumn<E>,
                                             p_firstLineAsHeaderH: EHeaderInit,
                                             p_firstLineAsHeaderV: EHeaderInit,
                                             headerManipulation: EHeaderManipulation) :
    InformationList<E>(
    p_elements, p_orientation, p_createCell, p_createInfoRow,
    p_createInfoColumn, p_firstLineAsHeaderH, p_firstLineAsHeaderV, headerManipulation,
    InitListUI()), IHasListenerCellChangedColour<E> {

    /**
     * Sets the colour of a single cell.
     *
     * @param p_column Column the cell is in.
     * @param p_row Row the cell is in.
     * @param p_new New colour of the cell.
     */
    @API
    fun setColumnRowColour(p_column: Int,
                           p_row: Int,
                           p_new: Color) {
        val l_cell = this.m_columns.getColumn(p_column).getCell(
            p_row) as ICellSetG<E>
        val l_old = l_cell.backgroundColour
        if (l_old !== p_new) {
            l_cell.backgroundColour = p_new
            this.listener.fireIListenerCellChangedColour(l_cell, l_old, p_new)
        }
    }

    //<editor-fold desc="IHasListenerCellChangedColour">
    override fun addListenerCellChangedColour(p_listener: IListenerCellChangedColour<E>) {
        this.listener.addListenerCellChangedColour(p_listener)
    }

    override fun removeListenerCellChangedColour(p_listener: IListenerCellChangedColour<E>) {
        this.listener.removeListenerCellChangedColour(p_listener)

    }
    //</editor-fold>

    override fun getColumn(p_index: Int): IColumnUI<E> {
        return super.getColumn(p_index) as IColumnUI<E>
    }

    override fun getRow(p_index: Int): IRowUI<E> {
        return super.getRow(p_index) as IRowUI<E>
    }

    override val columns: IColumnsUI<E>
        get() = super.columns as IColumnsUI<E>
    override val rows: IRowsUI<E>
        get() = super.rows as IRowsUI<E>

    override fun getColumnRow(p_indexColumn: Int,
                              p_indexRow: Int): ICellG<E> {
        return super.getColumnRow(p_indexColumn, p_indexRow) as ICellG<E>
    }

    override fun getRowColumn(p_indexRow: Int,
                              p_indexColumn: Int): ICellG<E> {
        return super.getRowColumn(p_indexRow, p_indexColumn) as ICellG<E>
    }
}
