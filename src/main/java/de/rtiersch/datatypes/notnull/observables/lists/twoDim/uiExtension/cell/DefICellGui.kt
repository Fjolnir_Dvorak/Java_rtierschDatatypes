/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.DefICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IHasListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead
import de.rtiersch.util.annotation.API
import javafx.scene.paint.Color

/**
 * Default Cell which can be used for InformationList and as a base for
 * graphical implementations for InformationList.
 * Creates a new Cell that is useable for displaying its data in a UI.

 * @param p_infoRow Row information for this cell.
 * *
 * @param p_infoColumn Column information for this cell.
 * *
 *
 * @param p_listener The shared listener for all cells.
 * *
 * @param p_content The content for the new cell.
 */
@API
open class DefICellGui<E>(p_infoRow: IInfoRowRead<E>,
                          p_infoColumn: IInfoColumnRead<E>,
                          p_listener: IHasListenerCellChanged<E>,
                          p_content: E,
                          backgroundColour: Color = Color.WHITE) : DefICell<E>(
    p_infoRow, p_infoColumn, p_listener, p_content), ICellSetG<E>, ICellHasGui {

    /**
     * Sets the Colour property of the cell.
     */
    override var backgroundColour: Color = backgroundColour
        set(value) {
            m_cellGui?.guiBackgroundColourUpdateEvent()
        }

    /**
     * The GuiCell from the user interface that is currently displaying this
     * cell. This is constructed for a dynamic table view.
     */
    private var m_cellGui: IGuiCell? = null

    override var content: E
        get() = super.content
        set(p_content) {
            super.content = p_content
            if (this.m_cellGui != null) {
                this.m_cellGui!!.guiTextUpdateEvent()
            }
        }

    override fun setGuiCell(p_cell: IGuiCell) {
        this.m_cellGui = p_cell
    }

    override fun unsetGuiCell(p_cell: IGuiCell) {
        if (p_cell === this.m_cellGui) {
            this.m_cellGui = null
        }
    }

    /**
     * Copies a column with new indices.
     * @param columnInfo column index.
     * @param rowInfo row index.
     */
    override fun copy(rowInfo: IInfoRowRead<E>,
                      columnInfo: IInfoColumnRead<E>): ICellSet<E> {
        return DefICellGui(rowInfo, columnInfo, m_listener, content,
            backgroundColour)
    }
}
