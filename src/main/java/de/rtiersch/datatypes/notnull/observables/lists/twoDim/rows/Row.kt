/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.datatypes.notnull.shared.StringContainer
import java.util.*

/**
 * This is a row based access of the cells with an additional row based
 * Information/Validation container.
 *
 * @param cells All the cells. Also accessible over Column.java
 * @param header The header of this row. If it is empty, it is an empty
 * string, not null.
 * @param shadowedInfo THe user information and validation class for all the cells
 * in this row.
 * @param index The index of this row.
 * @param <E> The content type of the row.
</E> */
open class Row<E>
(
    protected val headerInt: ArrayList<StringContainer>,
    protected val cells: ArrayList<ICellSet<E>>,
    protected val shadowedInfo: AInfoRow<E>,
    override val index: IIntegerRead) : IRow<E> {

    override val header: String
        get() {
            return headerInt[index.int].string
        }

    init {
        this.shadowedInfo.registerRow(this)
    }

    override fun getCell(p_column: Int): ICellSet<E> {
        return this.cells[p_column]
    }

    override val info: IInfoRowRead<E>
        get() = this.shadowedInfo

    override fun size(): Int {
        return this.cells.size
    }

    /**
     * Provides the possibility to add new cells to this row.
     * No validation checks are made!

     * @param p_column Column at where to insert the new cell.
     * *
     * @param p_cell Cell to insert.
     */
    fun addCellIntern(p_column: Int,
                      p_cell: ICellSet<E>) {
        this.cells.add(p_column, p_cell)
    }

    /**
     * Provides the possibility to remove a cell in this row.
     * No validation checks are made!

     * @param p_column Cell to remove.
     */
    fun removeCellIntern(p_column: Int) {
        this.cells.removeAt(p_column)
    }

    override fun iterator(): Iterator<ICell<E>> {
        return RowIterator(this)
    }

    /**
     * Do never make this private! I had to handle private iterators when I
     * wanted to extend the functionality of an ArrayList. Never make
     * anything private!
     * This is the iteration manager for the cells in the row. Please use a
     * ForI whenever possible.
     */
    protected open inner class RowIterator
    (
            /**
             * Parent the cells of this iterator belongs to.
             * Needed for sizeModification checks.
             */
            protected val m_iterateThrough: Row<E>) : Iterator<ICell<E>> {
        /**
         * The size of Row<E> this iterator was created with. If this
         * diverges with the actual size a ConcurrentModificationException
         * will be thrown. It is ugly, yes, very ugly. But it is kind of
         * standard. Better would be to let the user decide on how he want to
         * react to changes.
        </E> */
        protected val m_initSize: Int = m_iterateThrough.size()
        /**
         * Iterator counter. Saves the actual position.
         */
        protected var m_counter: Int = 0

        override fun hasNext(): Boolean {
            this.isModified()
            return this.m_counter < this.m_initSize
        }

        override fun next(): ICell<E> {
            this.isModified()
            if (!this.hasNext()) {
                throw IndexOutOfBoundsException("Index has grown to big.")
            }
            this.m_counter++
            return this.m_iterateThrough.getCell(this.m_counter - 1)
        }

        /**
         * Hmm, this is very ugly. Default size check for concurrent
         * modification. But concurrency is a really beautiful thing. Need to
         * find a beatiful nice way to enable concurrentModifucation.
         * TODO make concurrentModification work.
         */
        protected fun isModified() {
            if (this.m_iterateThrough.size() != this.m_initSize) {
                throw ConcurrentModificationException(
                        "The size of the list is no longer valid. Do not know " + "where to continue...")
            }
        }
    }
}
