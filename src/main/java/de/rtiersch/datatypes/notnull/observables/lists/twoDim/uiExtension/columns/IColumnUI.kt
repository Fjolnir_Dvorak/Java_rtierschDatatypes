package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell.ICellG

interface IColumnUI<E> : IColumn<E> {
    override fun getCell(p_row: Int): ICellG<E>
    override fun iterator(): Iterator<ICellG<E>>
}
