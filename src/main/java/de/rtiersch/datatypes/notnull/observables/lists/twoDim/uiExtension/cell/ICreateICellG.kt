/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.cell

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IInfoColumnRead
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IInfoRowRead
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICreateICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IFireListenerCellChanged

/**
 * Cell Creation Utility if the user want's to add further informations without
 * extending the class InformationList and all it's "children".

 * @param <E> Content type.
</E> */
interface ICreateICellG<E> : ICreateICell<E> {

    /**
     * Creates a new Cell Container wrapping p_element.

     * @param p_rowInformation Row information for the row of  the cell.
     * *
     * @param p_columnInformation Column informatio nfor the column of the
     * *                            cell.
     * *
     * @param p_listener Shared listener used by the cell.
     * *
     * @param p_element Element to wrap.
     * *
     * @return A baked frish new cell.
     */
    override fun constructCell(p_rowInformation: IInfoRowRead<E>,
                               p_columnInformation: IInfoColumnRead<E>,
                               p_listener: IFireListenerCellChanged<E>,
                               p_element: E): ICellSetG<E>

    /**
     * Creates a new Cell Container wrapping p_element.

     * @param p_rowInformation Row information for the row of  the cell.
     * *
     * @param p_columnInformation Column informatio nfor the column of the
     * *                            cell.
     * *
     * @param p_element The cell that shall be copied.
     * *
     * @return A baked frish new cell.
     */
    override fun copyCell(p_rowInformation: IInfoRowRead<E>,
                          p_columnInformation: IInfoColumnRead<E>,
                          p_element: ICell<E>): ICellSetG<E>
}
