/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns

import java.util.*

/**
 * @param <E> Type of cells which are in the columns.
</E> */
open class Columns<E>
(
        /**
         * All the columns.  Also accessible over Rows.java
         */
        protected val m_columns: ArrayList<Column<E>>) : IColumns<E> {

    override fun getColumn(p_column: Int): Column<E> {
        return this.m_columns[p_column]
    }

    override fun size(): Int {
        return this.m_columns.size
    }

    /**
     * Dangerous! This is not intended for use, but visible for extensibility.
     * Please do not use, use the functionality provided in InformationList
     * instead. There is no functionality like package-private and protected
     * and protected-package private, so to keep it simple: it is public.
     * TODO could be solved via a wrapper object shadowing the container.
     *
     *
     * Adds a new column to the column container

     * @param p_index The index where to insert the new column.
     * *
     * @param p_column The new column.
     */
    fun addColumnIntern(p_index: Int,
                        p_column: Column<E>) {
        this.m_columns.add(p_index, p_column)
    }

    /**
     * Dangerous! This is not intended for use, but visible for extensibility.
     * Please do not use, use the functionality provided in InformationList
     * instead. There is no functionality like package-private and protected
     * and protected-package private, so to keep it simple: it is public.
     * TODO could be solved via a wrapper object shadowing the container.
     *
     *
     * Removes a column from the column container.

     * @param p_index The index of the column that will be removed.
     */
    fun removeColumnIntern(p_index: Int) {
        this.m_columns.removeAt(p_index)
    }

    override fun iterator(): Iterator<IColumn<E>> {
        return ColumnsIterator(this)
    }

    /**
     * Do never make this private! I had to handle private iterators when I
     * wanted to extend the functionality of an ArrayList. Never make
     * anything private!
     * This is the iteration manager for the columns in the column-container.
     * Please use a ForI whenever possible.
     */
    protected open inner class ColumnsIterator
    (
            /**
             * Parent the columns of this iterator belongs to.
             * Needed for sizeModification checks.
             */
            private val m_iterateThrough: Columns<E>) : Iterator<IColumn<E>> {
        /**
         * The size of ColumnsUI<E> this iterator was created with. If this
         * diverges with the actual size a ConcurrentModificationException
         * will be thrown. It is ugly, yes, very ugly. But it is kind of
         * standard. Better would be to let the user decide on how he want to
         * react to changes.
        </E> */
        private val m_initSize: Int = m_iterateThrough.size()
        /**
         * Iterator counter. Saves the actual position.
         */
        private var m_counter: Int = 0

        override fun hasNext(): Boolean {
            this.isModified()
            return this.m_counter < this.m_initSize
        }

        override fun next(): IColumn<E> {
            this.isModified()
            if (!this.hasNext()) {
                throw IndexOutOfBoundsException("Index has grown to big.")
            }
            this.m_counter++
            return this.m_iterateThrough.getColumn(this.m_counter - 1)
        }

        /**
         * Hmm, this is very ugly. Default size check for concurrent
         * modification. But concurrency is a really beautiful thing. Need to
         * find a beatiful nice way to enable concurrentModifucation.
         * TODO make concurrentModification work.
         */
        protected fun isModified() {
            if (this.m_iterateThrough.size() != this.m_initSize) {
                throw ConcurrentModificationException(
                        "The size of the " + "list is no " + "longer valid" + ". Do not " + "know where to " + "continue...")
            }
        }
    }
}
