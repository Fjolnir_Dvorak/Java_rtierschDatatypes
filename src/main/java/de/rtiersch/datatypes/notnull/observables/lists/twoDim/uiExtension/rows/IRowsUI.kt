package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IRows

interface IRowsUI<E> : IRows<E> {
    override fun getRow(p_row: Int): IRowUI<E>
    override fun iterator(): Iterator<IRowUI<E>>
}
