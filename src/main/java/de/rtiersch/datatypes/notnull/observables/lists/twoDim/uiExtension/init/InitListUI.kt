package de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.init

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.AInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Column
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Columns
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.init.InitList
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.AInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Row
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Rows
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns.ColumnUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.columns.ColumnsUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows.RowUI
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.uiExtension.rows.RowsUI
import de.rtiersch.datatypes.notnull.shared.IntegerContainer
import de.rtiersch.datatypes.notnull.shared.StringContainer
import java.util.*

class InitListUI : InitList() {
    override fun <E> createRow(cells: ArrayList<ICellSet<E>>,
                               header: ArrayList<StringContainer>,
                               index: IntegerContainer,
                               shadowedInfo: AInfoRow<E>): Row<E> {
        return RowUI(cells = cells, headerInt = header, index = index,
            shadowedInfo = shadowedInfo)
    }

    override fun <E> createColumn(cells: ArrayList<ICellSet<E>>,
                                  header: ArrayList<StringContainer>,
                                  index: IntegerContainer,
                                  shadowedInfo: AInfoColumn<E>): Column<E> {
        return ColumnUI(cells = cells, headerInt = header, index = index,
            shadowedInfo = shadowedInfo)
    }

    override fun <E> createRows(rows: ArrayList<Row<E>>): Rows<E> {
        return RowsUI(rows)
    }

    override fun <E> createColumns(columns: ArrayList<Column<E>>): Columns<E> {
        return ColumnsUI(columns)
    }
}
