/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICellSet
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICreateICell
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.Columns
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.IColumns
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.columns.ICreateAInfoColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderInit
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderInit.None
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.EHeaderManipulation
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.init.InitList
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.init.NO_HEADER
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.init.SerializedData
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IHasListenerAddRemoveColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IHasListenerAddRemoveRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IHasListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerAddRemoveColumn
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerAddRemoveRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.listener.IListenerCellChanged
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.ICreateAInfoRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IRow
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.IRows
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows.Rows
import de.rtiersch.datatypes.notnull.shared.IIntegerRead
import de.rtiersch.datatypes.notnull.shared.IntegerContainer
import de.rtiersch.datatypes.notnull.shared.StringContainer
import de.rtiersch.exception.ArrayNotRectangularException
import de.rtiersch.util.annotation.API
import de.rtiersch.util.lists.ArrayUtilsKt
import org.apache.commons.lang3.StringUtils

/**
 * InformationList is a Table (two dimensional ArrayLists with synchronized
 * length). The cells with type E are wrapped by ICellG (default
 * implementation DefICellGui) which need to be created by the user using the
 * constructor-class ICreateICell. The data of the table is accessible through
 * row-based and column-based indexing. To prevent a painful brain death
 * caused by misunderstood XY-indexing systems the data is no longer
 * accessible as XY or YX based table. Java and math have a quite opposite
 * understanding on how to index a table.
 * The input array used to create a new table are not further used,
 * modified or referred to. The containing data will be wrapped into ICellG
 * to let the user decide on how to handle data copying.
 * IMPORTANT: Not tested against multi threading/processing
 * There are three registrable listeners: IListenerAddRemoveColumn,
 * IListenerAddRemoveRow and IListenerCellChanged.

 * @param <E> Content Type.
</E> */
open class InformationList<E>
/**
 * Creates a new two dimensional arrayList with normed row- and column
 * length. Missing fields and null values will throw an error.
 * I am really sorry 'bout that, but did you ever tried to display a null?

 * @param elements array to observe (do not observe the array,
 * *                             only the objects itself).
 * *
 * @param orientation The index orientation of elements.
 * @param createCell Creator class for cells.
 * @param createInfoRow Creator class for row information container.
 * @param createInfoColumn Creator class for column information container.
 * @param headerInitH Is there a horizontal header and what type does it have?
 * @param headerInitV Is there a vertical header and what type does it have?
 * @param headerManipulation What should be done when a new header is needed?
 * @param initList Initialization container for inheritance.
 * @throws ArrayNotRectangularException Input array is not rectangular.
 * *                                      Please use something like
 * *                                      ArrayUtils.rectify()
 * *
 * @throws NullPointerException NotNull datatype. Array contains
 * *                                      null values.
 */
@Throws(ArrayNotRectangularException::class,
    NullPointerException::class) protected constructor(elements: Array<Array<E>>,
                                                       orientation: EOrientation,
                                                       protected val createCell: ICreateICell<E>,
                                                       protected val createInfoRow: ICreateAInfoRow<E>,
                                                       protected val createInfoColumn: ICreateAInfoColumn<E>,
                                                       protected val headerInitH: EHeaderInit,
                                                       protected val headerInitV: EHeaderInit,
                                                       protected val headerManipulation: EHeaderManipulation,
                                                       protected val initList: InitList) : IHasListenerCellChanged<E>, IHasListenerAddRemoveRow<E>, IHasListenerAddRemoveColumn<E> {

    /**
     * Main table used for first initialisation. Should make no difference
     * how the array is allocated. Java has no column / row oriented array
     * design.
     * Intern method structure is trying to guarantee containing lists of
     * equal length.
     */
    protected val m_rows: Rows<E>
    /**
     * Table accessing the the columns instead of rows. "Transformed" matrix
     * of m_columns. This is only copying the references of the content, not
     * the content itself.
     * Intern method structure is trying to guarantee containing lists of
     * equal length.
     */
    protected val m_columns: Columns<E>

    protected val headerH: ArrayList<StringContainer>
    protected val headerV: ArrayList<StringContainer>

    /**
     * Shared listener container for table and content.
     */

    /**
     * Returns the listener this table uses.

     * @return Listener.
     */
    val listener: Listener<E> = Listener()

    /**
     * Storage container for the shared indices.
     */
    protected val m_indexColumns: ArrayList<IntegerContainer>
    /**
     * Storage container for the shared indices.
     */
    protected val m_indexRows: ArrayList<IntegerContainer>

    /**
     * I do not know what I planned, but this was intended for concurrent
     * safety. Hehe, safety...
     */
    protected var m_locked = false

    constructor(elements: Array<Array<E>>,
                orientation: EOrientation,
                createCell: ICreateICell<E>,
                createInfoRow: ICreateAInfoRow<E>,
                createInfoColumn: ICreateAInfoColumn<E>,
                headerH: EHeaderInit,
                headerV: EHeaderInit,
                headerManipulation: EHeaderManipulation) : this(elements,
        orientation, createCell, createInfoRow, createInfoColumn, headerH,
        headerV, headerManipulation, InitList())

    init {

        val l_elements: Array<Array<E>> = initList.validateData(elements,
            orientation)
        val l_data = initList.createHeader(l_elements, this.headerInitH,
            this.headerInitV)
        headerH = initList.createSharedHeader(l_data.headerColumns)
        headerV = initList.createSharedHeader(l_data.headerRows)
        val l_table = initList.createTable(l_data.table, headerH, headerV,
            createCell, createInfoRow, createInfoColumn, this.listener)

        this.m_rows = l_table.m_rows
        this.m_columns = l_table.m_columns
        this.m_indexColumns = l_table.m_indexColumns
        this.m_indexRows = l_table.m_indexRows
    }

    /**
     * Returns a column.

     * @param p_index column to return.
     * *
     * @return Column.
     */
    open fun getColumn(p_index: Int): IColumn<E> {
        return this.m_columns.getColumn(p_index)
    }

    /**
     * Returns a row.

     * @param p_index row to return.
     * *
     * @return Row.
     */
    open fun getRow(p_index: Int): IRow<E> {
        return this.m_rows.getRow(p_index)
    }

    /**
     * Returns all columns.

     * @return Column.
     */
    open val columns: IColumns<E>
        get() = this.m_columns

    /**
     * Returns all rows.

     * @return Row.
     */
    open val rows: IRows<E>
        get() = this.m_rows

    /**
     * Returns a single element.

     * @param p_indexRow row index.
     * *
     * @param p_indexColumn column index.
     * *
     * @return content.
     */
    open fun getColumnRow(p_indexColumn: Int,
                          p_indexRow: Int): ICell<E> {
        return this.m_columns.getColumn(p_indexColumn).getCell(p_indexRow)
    }

    /**
     * Returns a single element.

     * @param p_indexColumn column index.
     * *
     * @param p_indexRow row index.
     * *
     * @return content.
     */
    open fun getRowColumn(p_indexRow: Int,
                          p_indexColumn: Int): ICell<E> {

        return this.m_rows.getRow(p_indexRow).getCell(p_indexColumn)
    }

    /**
     * Returns the read only index container of the element p_index.

     * @param p_index index of index container.
     * *
     * @return index container.
     */
    fun getIndexColumn(p_index: Int): IIntegerRead {
        return this.m_indexColumns[p_index]
    }

    /**
     * Returns the read only index container of the element p_index.

     * @param p_index index of index container.
     * *
     * @return index container.
     */
    fun getIndexRow(p_index: Int): IIntegerRead {
        return this.m_indexRows[p_index]
    }

    /**
     * Returns the length of the rows.

     * @return Length of a row.
     */
    fun lengthRow(): Int {
        return this.sizeColumns()
    }

    /**
     * Returns the amount of columns.

     * @return Columns counted.
     */
    fun sizeColumns(): Int {
        return this.m_columns.size()
    }

    /**
     * Returns the length of the columns.

     * @return Length of a column.
     */
    fun lengthColumn(): Int {
        return this.sizeRows()
    }

    /**
     * Returns the amount of rows.

     * @return Rows counted.
     */
    fun sizeRows(): Int {
        return this.m_rows.size()
    }

    //</editor-fold>
    //<editor-fold desc="Setter">

    /**
     * Changes the content of a single cell.

     * @param p_column Column the cell is in.
     * @param p_row Row the cell is in.
     * @param p_new New content of the cell.
     */
    @API
    fun setColumnRow(p_column: Int,
                     p_row: Int,
                     p_new: E) {
        val l_cell = this.m_columns.getColumn(p_column).getCell(p_row)
        val l_old = l_cell.content
        if (l_old !== p_new) {
            l_cell.content = p_new
            this.listener.fireIListenerCellChanged(l_cell, l_old, p_new)
        }
    }
    //</editor-fold>
    //<editor-fold desc="Add">

    /**
     * Adds a single row at the end ob the table. (row starts at column index
     * 0. Missing elements will be filled with null).
     * @param p_newRow new Row.
     * @param p_header Header for the row. Leave empty if not needed.
     * @return success.
     */
    fun addRow(p_newRow: Array<E>,
               p_header: String?): Boolean {
        return this.addRow(p_newRow, this.m_rows.size(), p_header, null)
    }

    /**
     * Adds a single row at the defined index position. row starts at column
     * index 0.
     * @param newRow new Row.
     * @param index position.
     * @param header Header for the row. Leave empty if not needed.
     * @return success.
     */
    fun addRow(newRow: Array<E>,
               index: Int,
               header: String?): Boolean {
        return addRow(newRow, index, header, null)
    }

    /**
     * Adds a single row at the defined index position. Row starts at column
     * index 0. Internal method.
     * @param newRow New Row.
     * @param index Position.
     * @param header Header for the row. Leave empty if not needed.
     * @param copied Is this a copied row?
     * @return success.
     */
    open protected fun addRow(newRow: Array<E>,
                              index: Int,
                              header: String?,
                              copied: Int?): Boolean {
        var l_header = ""
        if (header != null) {
            l_header = header
        }
        if (newRow.size > this.m_columns.size()) {
            // input data is to big. I do not know what to do with the overflow.
            return false
        }

        val l_indexNewRow = IntegerContainer(index)
        this.m_indexRows.add(index, l_indexNewRow)

        /*
         * Create the new row.
         */
        val l_infoRow = this.createInfoRow.constructInfoRow(l_indexNewRow)
        val l_list = ArrayList<ICellSet<E>>(this.m_columns.size())
        for (l_i in 0 until this.m_columns.size()) {
            val l_elem: E = if (l_i < newRow.size) {
                newRow[l_i]
            } else {
                return false
            }
            l_list.add(this.createCell.constructCell(l_infoRow,
                this.m_columns.getColumn(l_i).info, this.listener, l_elem))
        }
        this.createHeader(newHeader = l_header, index = index,
            headerBH = this.headerInitV, cells = newRow, headerJar = headerV,
            copied = copied)
        val l_newRow = this.initList.createRow(cells = l_list, header = headerV,
            index = l_indexNewRow, shadowedInfo = l_infoRow)

        // TODO lock table with wait!!! Modifying the data of the table.
        this.waitLock()
        this.m_locked = true
        /*
         * shift all indexes on the right hand side of index.
         */
        for (l_i in index + 1 until this.m_indexRows.size) {
            this.m_indexRows[l_i].int = l_i
        }

        /*
         * After successfully modifying the indices insert the new row.
         */
        this.m_rows.addRowIntern(index, l_newRow)

        for (l_i in 0 until this.m_columns.size()) {
            this.m_columns.getColumn(l_i).addCellIntern(index,
                l_newRow.getCell(l_i))
        }

        /*
         * Fire Listener.
         */
        this.listener.fireIListenerAddRemoveRow(l_newRow, index, EStatus.Added)
        return true
    }

    protected open fun createHeader(newHeader: String?,
                                    headerJar: ArrayList<StringContainer>,
                                    cells: Array<E>,
                                    headerBH: EHeaderInit,
                                    index: Int,
                                    copied: Int? = null) {
        var header = ""
        var reassign = false
        if (headerBH != None) {
            when (this.headerManipulation) {
                EHeaderManipulation.CustomHeader -> if (newHeader != null) {
                    header = newHeader
                } else {
                    if (copied != null) {
                        header = createCopyName(headerJar[copied].string)
                    }
                }
                EHeaderManipulation.Reassign -> {
                    reassign = true
                    when (headerBH) {
                        EHeaderInit.Numeric -> {
                            header = ArrayUtilsKt.createNextNumberedArray(
                                penultimate = headerJar[headerJar.lastIndex - 1].string,
                                last = headerJar.last().string, length = 1)[0]
                        }
                        EHeaderInit.NumericOne -> {
                            header = ArrayUtilsKt.createNextNumberedArray(
                                penultimate = headerJar[headerJar.lastIndex - 1].string,
                                last = headerJar.last().string, length = 1)[0]
                        }
                        EHeaderInit.Alphanumeric -> {
                            header = ArrayUtilsKt.createSetArray(
                                p_set = ArrayUtilsKt.BIG_LETTER,
                                incrementBeforeFirstUse = true,
                                neutralElement = false, p_length = 1,
                                startIndexSet = ArrayUtilsKt.createIndexSetFromString(
                                    headerJar.last().string,
                                    ArrayUtilsKt.BIG_LETTER))[0]
                        }
                        EHeaderInit.FirstElems -> {
                            header = newHeader ?: ""
                            reassign = false
                        }
                        EHeaderInit.None -> {
                            header = ""
                            reassign = false
                        }
                    }
                }
                EHeaderManipulation.MarkAsCopy -> {
                    if (copied != null) {
                        header = createCopyName(headerJar[copied].string)
                    } else {
                        header = newHeader ?: ""
                    }
                }
                EHeaderManipulation.Duplicate -> {
                    if (copied != null) {
                        header = headerJar[copied].string
                    } else {
                        header = newHeader ?: ""
                    }
                }
            }
        }
        val headerContainer = StringContainer(header)
        if (reassign) {
            // Index is not growing with the new element. Header will be
            // placed at end.
            headerJar.add(headerContainer)
        } else {
            headerJar.add(element = headerContainer, index = index)
        }
    }

    protected open fun createCopyName(name: String): String {
        val splittedAfter = name.substringAfterLast(delimiter = '\u0000',
            missingDelimiterValue = "0").trim('(', ')', ' ')
        val splittedBefore = name.substringBeforeLast(delimiter = '\u0000',
            missingDelimiterValue = name)
        val copyString: String
        copyString = if (StringUtils.isNumeric(splittedAfter)) {
            val copyCount = Integer.parseInt(splittedAfter) + 1
            "\u0000 ($copyCount)"
        } else {
            "\u0000 (0)"
        }
        return splittedBefore + copyString
    }

    /**
     * TODO I can see nothing here!
     * Yea, this should be used to make all the things safe for usage in
     * concurrency...
     */
    private fun waitLock() {

    }

    /**
     * Copies a row and places it at the specified destination index.

     * @param p_rowToCopy This row will be copied. The new row will have the
     * *                      same header.
     * *
     * @param p_destination This is the destination index the new row will be
     * *                      inserted to.
     * *
     * @return true if row could be copied.
     */
    fun copyRow(p_rowToCopy: Int,
                p_destination: Int): Boolean {
        /*
         * Generate an array and pass it as to addRowIntern.
         */
        val l_oldRow = this.m_rows.getRow(p_rowToCopy)
        val l_newRow = arrayOfNulls<Any>(l_oldRow.size()) as Array<E>
        for (l_i in 0 until l_oldRow.size()) {
            l_newRow[l_i] = l_oldRow.getCell(l_i).content
        }
        return this.addRow(l_newRow, p_destination, null, p_rowToCopy)
    }

    /**
     * Copies a column and places it at the specified destination index.

     * @param p_columnToCopy This column will be copied. The new column will
     * *                       have the same header.
     * *
     * @param p_destination This is the destination index the new column will
     * *                       be inserted to.
     * *
     * @return true if column could be copied.
     */
    fun copyColumn(p_columnToCopy: Int,
                   p_destination: Int): Boolean {
        /*
         * Generate an array and pass it as to addColumn.
         */
        val l_oldColumn = this.m_columns.getColumn(p_columnToCopy)
        val l_newColumn = arrayOfNulls<Any>(l_oldColumn.size()) as Array<E>
        for (l_i in 0 until l_oldColumn.size()) {
            l_newColumn[l_i] = l_oldColumn.getCell(l_i).content
        }
        return this.addColumn(l_newColumn, p_destination, null, p_columnToCopy)
    }

    /**
     * Adds a single column at the end ob the table. (column starts at column
     * index 0. Missing elements will be filled with null).

     * @param p_newColumn new Column.
     * *
     * @param p_header Header for the row. Leave empty if not needed.
     * *
     * @return success.
     */
    fun addColumn(p_newColumn: Array<E>,
                  p_header: String?): Boolean {
        return this.addColumn(p_newColumn, this.m_columns.size(), p_header,
            null)
    }

    /**
     * Adds a single column at the defined position position. Column starts at
     * row position 0.
     * @param newColumn new Column.
     * @param position position.
     * @param header Header for the row. Leave empty if not needed.
     * @return success.
     */
    fun addColumn(newColumn: Array<E>,
                  position: Int,
                  header: String?): Boolean {
        return addColumn(newColumn, position, header, null)
    }

    /**
     * Adds a single column at the defined newPosition position. Column starts at
     * row newPosition 0.
     * @param newColumn new Column.
     * @param newPosition position.
     * @param header Header for the row. Leave empty if not needed.
     * @param copied Is this a copied column?
     * @return success.
     */
    open protected fun addColumn(newColumn: Array<E>,
                  newPosition: Int,
                  header: String?,
                  copied: Int?): Boolean {
        var l_header = ""
        if (header != null) {
            l_header = header
        }
        if (newColumn.size > this.m_rows.size()) {
            // input data is to big. I do not know what to do with the overflow.
            return false
        }

        val l_indexNewColumn = IntegerContainer(newPosition)
        this.m_indexColumns.add(newPosition, l_indexNewColumn)

        /*
         * Create the new column.
         */
        val l_infoColumn = this.createInfoColumn.constructInfoColumn(
            l_indexNewColumn)
        val l_list = ArrayList<ICellSet<E>>(this.m_rows.size())
        for (l_i in 0 until this.m_rows.size()) {
            val l_elem: E = if (l_i < newColumn.size) {
                newColumn[l_i]
            } else {
                return false
            }
            l_list.add(
                this.createCell.constructCell(this.m_rows.getRow(l_i).info,
                    l_infoColumn, this.listener, l_elem))
        }

        this.createHeader(newHeader = l_header, index = newPosition,
            headerBH = this.headerInitH, cells = newColumn, headerJar = headerH,
            copied = copied)
        val l_newColumn = this.initList.createColumn(cells = l_list,
            header = headerH, index = l_indexNewColumn,
            shadowedInfo = l_infoColumn)

        // TODO lock table with wait!!! Modifying data of the table.
        this.waitLock()
        this.m_locked = true
        /*
         * shift all indexes on the right hand side of newPosition.
         */
        for (l_i in newPosition + 1 until this.m_indexColumns.size) {
            this.m_indexColumns[l_i].int = l_i
        }

        /*
         * After successfully modifying the indices insert the new column.
         */
        this.m_columns.addColumnIntern(newPosition, l_newColumn)

        for (l_i in 0 until this.m_rows.size()) {
            this.m_rows.getRow(l_i).addCellIntern(newPosition,
                l_newColumn.getCell(l_i))
        }

        /*
         * Fire Listener.
         */
        this.listener.fireIListenerAddRemoveColumn(l_newColumn, newPosition,
            EStatus.Added)
        return true
    }

    //</editor-fold>
    //<editor-fold desc="Remove">

    /**
     * Removes a single row at the defined index position. (Row starts at
     * column index 0).
     * @param p_row position.
     * @return success.
     */
    fun removeRow(p_row: Int): Boolean {
        /*
         * Fire Listener.
         */
        if (p_row >= this.m_rows.size()) {
            throw ArrayIndexOutOfBoundsException()
        }

        // Remove the row out of the columns table.
        for (l_iColumn in 0 until this.m_columns.size()) {
            this.m_columns.getColumn(l_iColumn).removeCellIntern(p_row)
        }
        val l_rowOld = this.m_rows.getRow(p_row)
        // Remove the row out of the row table.
        this.m_rows.removeRowIntern(p_row)
        // Remove the old row index.
        this.m_indexRows.removeAt(p_row)
        this.removeHeader(this.headerV, p_row)
        // Reset all old indices.
        for (l_iRows in p_row until this.m_indexRows.size) {
            this.m_indexRows[l_iRows].int = l_iRows
        }

        this.listener.fireIListenerAddRemoveRow(l_rowOld, p_row,
            EStatus.Removed)
        return true
    }

    /**
     * Removes a single column at the defined index position. (Column starts at
     * row index 0).

     * @param p_column position.
     * *
     * @return success.
     */
    fun removeColumn(p_column: Int): Boolean {
        /*
         * Fire Listener.
         */
        if (p_column >= this.m_columns.size()) {
            throw ArrayIndexOutOfBoundsException()
        }

        // Remove the row out of the columns table.
        for (l_iRow in 0 until this.m_rows.size()) {
            this.m_rows.getRow(l_iRow).removeCellIntern(p_column)
        }
        val l_columnOld = this.m_columns.getColumn(p_column)
        // Remove the column out of the column table.
        this.m_columns.removeColumnIntern(p_column)
        // Remove the old Column index.
        this.m_indexColumns.removeAt(p_column)
        this.removeHeader(this.headerH, p_column)
        // Reset all old indices.
        for (l_iColumns in p_column until this.m_indexColumns.size) {
            this.m_indexColumns[l_iColumns].int = l_iColumns
        }

        this.listener.fireIListenerAddRemoveColumn(l_columnOld, p_column,
            EStatus.Removed)
        return true
    }
    protected open fun removeHeader(headerJar: ArrayList<StringContainer>,
                                    index: Int) {
        when(this.headerManipulation) {
            EHeaderManipulation.Reassign -> {
                headerJar.removeAt(headerJar.lastIndex)
            }
            else -> {
                headerJar.removeAt(index)
            }
        }
    }

    //</editor-fold>
    //<editor-fold desc="Listener">
    //<editor-fold desc="IHasListenerCellChanged">
    /**
     * Adds a new Cell changed listener that will be thrown when the content
     * of the cell is changed. Listeners can be added multiple times and will
     * then be called multiple times.
     * @param p_listener Listener
     */
    override fun addListenerCellChanged(p_listener: IListenerCellChanged<E>) {
        this.listener.addListenerCellChanged(p_listener)
    }

    /**
     * Removes the first occurrence of the previously added Listener. If it
     * was added multiple times, only the first added Listener will be removed.
     * @param p_listener Listener
     */
    override fun removeListenerCellChanged(p_listener: IListenerCellChanged<E>) {
        this.listener.removeListenerCellChanged(p_listener)
    }

    //</editor-fold>
    //<editor-fold desc="IHasListenerAddRemoveRow">
    /**
     * Adds a new Row added or removed listener that will be thrown when a
     * new row was added or an existing row deleted. Listeners can be added
     * multiple times and will then be called multiple times.
     * @param p_listener Listener
     */
    override fun addListenerAddRemoveRow(p_listener: IListenerAddRemoveRow<E>) {
        this.listener.addListenerAddRemoveRow(p_listener)
    }

    /**
     * Removes the first occurrence of the previously added Listener. If it
     * was added multiple times, only the first added Listener will be removed.
     * @param p_listener Listener
     */
    override fun removeListenerAddRemoveRow(p_listener: IListenerAddRemoveRow<E>) {
        this.listener.removeListenerAddRemoveRow(p_listener)
    }

    //</editor-fold>
    //<editor-fold desc="IHasListenerAddRemoveColumn">
    /**
     * Adds a new Column added or removed listener that will be thrown when a
     * new column was added or an existing row deleted. Listeners can be added
     * multiple times and will then be called multiple times.
     * @param p_listener Listener
     */
    override fun addListenerAddRemoveColumn(p_listener: IListenerAddRemoveColumn<E>) {
        this.listener.addListenerAddRemoveColumn(p_listener)
    }

    /**
     * Removes the first occurrence of the previously added Listener. If it
     * was added multiple times, only the first added Listener will be removed.
     * @param p_listener Listener
     */
    override fun removeListenerAddRemoveColumn(p_listener: IListenerAddRemoveColumn<E>) {
        this.listener.removeListenerAddRemoveColumn(p_listener)
    }

    //</editor-fold>
    //</editor-fold>

    //<editor-fold desc="Utility">

    /**
     * Saves the raw data back into a two dimensional array. The headers were
     * converted to strings, so they will be returned as String[] instead of
     * being a part of the table array.
     * This is only intended for simple use. All the data of the user defined
     * Objects are lost. The Cell content will be accessed via
     * ICellG.getContent().
     *
     * @return Data of the table and both headers. If the header was not
     * existent it will be represented by an array of the length 0.
     */
    fun serializeRawData(): SerializedData<E> {
        val l_headerColumns: Array<String>
        val l_headerRows: Array<String>
        val l_table: Array<Array<E>>
        val l_sizeRows = this.sizeRows()
        val l_sizeColumns = this.sizeColumns()

        if (this.headerInitH === EHeaderInit.None) {
            l_headerColumns = NO_HEADER
        } else {
            l_headerColumns = Array(l_sizeColumns, { i ->
                this.m_columns.getColumn(i).header
            })
            for (l_iColumn in 0 until l_sizeColumns) {
                l_headerColumns[l_iColumn] = this.m_columns.getColumn(
                    l_iColumn).header
            }
        }
        l_headerRows = if (this.headerInitV === EHeaderInit.None) {
            NO_HEADER
        } else {
            Array(l_sizeRows, { i -> this.m_rows.getRow(i).header })
        }

        l_table = Array(l_sizeRows, { iRow ->
            Array(l_sizeColumns, { iCol ->
                this.m_rows.getRow(iRow).getCell(iCol).content as Any
            })
        }) as Array<Array<E>>
        return SerializedData(l_headerColumns, l_headerRows, l_table)
    }

    //</editor-fold>
}
