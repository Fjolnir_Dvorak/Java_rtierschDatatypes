/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists;

import de.rtiersch.datatypes.notnull.observables.Listener.EListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IDoubleIndexedAddListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IDoubleIndexedChangeListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .IDoubleIndexedRemoveListener;
import de.rtiersch.datatypes.notnull.observables.Listener.IListAddListener;
import de.rtiersch.datatypes.notnull.observables.Listener.IListRemoveListener;
import de.rtiersch.datatypes.notnull.observables.Listener
    .ListenerContainerEXTColumn;
import de.rtiersch.datatypes.notnull.observables.ObservableValue2D;
import de.rtiersch.datatypes.notnull.shared.IntegerContainer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * Creates a wrapper for two dimensional ObservableArrayLists.
 *
 * @param <E> Type.
 */
public class ObservableArrayList2DColumn<E>
    implements IList2DColumnFixedSize<E>
{

    /**
     * Table accessing the the columns instead of rows. "Transformed" matrix of
     * m_columns. This is only copying the references of the content, not the
     * content itself. Intern method structure is trying to guarantee containing
     * lists of equal length.
     */
    protected final @NotNull
    ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
        m_columnTable;

    /**
     * Shared listener container for table and content.
     */
    protected final @NotNull ListenerContainerEXTColumn<E> m_listener;

    protected final @NotNull ArrayList<IntegerContainer> m_index;

    /**
     * Creates a new two dimensional arrayList with normed row- and column
     * length. Missing fields will be filled with null.
     *
     * @param p_elements array to observe (do not observe the array, only the
     *                   objects itself).
     */
    public ObservableArrayList2DColumn(final @NotNull E[][] p_elements)
    {
        this.m_listener =
            new ListenerContainerEXTColumn<>(EListener.DOUBLE_INDEXED_CHANGE);

        final int l_maxX = p_elements.length;
        final ArrayList<IntegerContainer> l_indexX = new ArrayList<>(l_maxX);
        int l_x = 0;
        int l_y = 0;

        /*
         * Initialize l_indexX.
         */
        for (int l_i = 0; l_i < l_maxX; l_i++) {
            l_indexX.add(new IntegerContainer(l_i));
        }
        /*
         * Initialize l_listA.
         */
        final ObservableArrayList<ObservableArrayList<ObservableValue2D<E>>>
            l_listA;
        l_listA = new ObservableArrayList<>(l_maxX);
        for (final E[] l_eFixX : p_elements) {
            final ObservableArrayList<ObservableValue2D<E>> l_fixX;
            l_fixX = new ObservableArrayList<>(p_elements.length);
            l_listA.add(l_fixX);
            for (final E l_eFixXY : l_eFixX) {
                final IntegerContainer l_indexY = new IntegerContainer(l_y);
                l_fixX.add(new ObservableValue2D<>(l_indexX.get(l_x), l_indexY,
                                                   l_eFixXY, this.m_listener));
                l_y++;
            }
            l_y = 0;
            l_x++;
        }

        this.m_columnTable = l_listA;
        this.m_index = l_indexX;
    }

    //<editor-fold desc="Getter">

    /**
     * Returns a column.
     *
     * @param p_index column to return.
     * @return Column.
     */
    @Override
    public IListFixedSize<ObservableValue2D<E>> getColumn(
        final int p_index)
    {
        return this.m_columnTable.get(p_index);
    }

    /**
     * Returns all columns.
     *
     * @return Column.
     */
    @Override
    public @NotNull IListFixedSize<IListFixedSize<ObservableValue2D<E>>>
    getColumns()
    {
        return (IListFixedSize<IListFixedSize<ObservableValue2D<E>>>)
            (Object) this.m_columnTable;
    }

    public final @NotNull ListenerContainerEXTColumn<E> getListener()
    {
        return this.m_listener;
    }

    //</editor-fold>
    //<editor-fold desc="Add">

    /**
     * Adds a single column at the end ob the table. (column starts at column
     * index 0. Missing elements will be filled with null).
     *
     * @param p_newColumn new Column.
     * @return success.
     */
    public boolean addColumn(final @NotNull E[] p_newColumn)
    {
        return this.addColumn(p_newColumn, this.m_columnTable.size());
    }

    /**
     * Adds a single column at the defined index position. Column starts at row
     * index 0. Missing elements will be filled with null).
     *
     * @param p_newColumn new Column.
     * @param p_index     position.
     * @return success.
     */
    public boolean addColumn(
        final @NotNull E[] p_newColumn, final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_newList;
        l_newList = this.addA(this.m_columnTable, p_newColumn, p_index);
        /*
         * Fire Listener.
         */
        this.m_listener.fireColumnAddedListener(l_newList, p_index);
        return true;
    }

    protected @NotNull ObservableArrayList<ObservableValue2D<E>> addA(
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listA,
        final @NotNull E[] p_newA,
        final int p_index)
    {
        final int l_length = p_newA.length;
        final int l_a = p_listA.size() + 1;
        final int l_lengthInput = p_newA.length;
        int l_iBElement = 0;
        /*
         * At first create the single A.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_newList;
        l_newList = new ObservableArrayList<>(l_length);
        final IntegerContainer l_aIndex = new IntegerContainer(p_index);
        /*
         * Add existing elements.
         */
        for (; l_iBElement < l_lengthInput; l_iBElement++) {
            final IntegerContainer l_columnIndex;
            l_columnIndex = new IntegerContainer(l_iBElement);
            l_newList.add(new ObservableValue2D<>(l_aIndex, l_columnIndex,
                                                  p_newA[l_iBElement]));
        }
        /*
         * At third insert the new A.
         */
        p_listA.add(p_index, l_newList);
        this.m_index.add(p_index, l_aIndex);
        /*
         * At last update all wrong A Indices.
         */
        for (int l_i = p_index + 1; l_i < l_a; l_i++) {
            this.m_index.get(l_i).setInt(l_i);
        }
        return l_newList;
    }

    public void addElement(final @NotNull E p_element, final int p_column)
    {
        final IntegerContainer l_columnIndex;
        l_columnIndex =
            new IntegerContainer(this.m_columnTable.get(p_column).size());
        final ObservableValue2D<E> l_e =
            new ObservableValue2D<E>(this.m_index.get(p_column), l_columnIndex,
                                     p_element, this.m_listener);
        this.m_columnTable.get(p_column).add(l_e);
        this.m_listener.fireDoubleIndexedAddedListener(l_e);
    }

    //</editor-fold>
    //<editor-fold desc="Remove">

    /**
     * Removes a single column at the defined index position. (Column starts at
     * row index 0).
     *
     * @param p_index position.
     * @return success.
     */
    public boolean removeColumn(final int p_index)
    {
        final ObservableArrayList<ObservableValue2D<E>> l_oldList;
        l_oldList = this.removeA(this.m_columnTable, p_index);
        /*
         * Fire Listener.
         */
        this.m_listener.fireColumnRemovedListener(l_oldList, p_index);
        return true;
    }

    protected @NotNull ObservableArrayList<ObservableValue2D<E>> removeA(
        final @NotNull ObservableArrayList<ObservableArrayList
            <ObservableValue2D<E>>> p_listA,
        final int p_index)
    {
        /*
         * At second remove A out of p_listA.
         */
        final ObservableArrayList<ObservableValue2D<E>> l_oldList;
        l_oldList = p_listA.get(p_index);
        p_listA.remove(p_index);
        this.m_index.remove(p_index);
        /*
         * At last update all wrong column Indices.
         */
        for (int l_i = p_index; l_i < p_listA.size(); l_i++) {
            this.m_index.get(l_i).setInt(l_i);
        }

        return l_oldList;
    }

    public @NotNull ObservableValue2D<E> removeElement(
        final int p_x, final int p_y)
    {
        final ObservableValue2D<E> l_old =
            this.m_columnTable.get(p_x).remove(p_y);

        /*
         * resetting observableValues
         */
        final int l_size = this.m_columnTable.get(p_x).size();
        for (int l_y = 0; l_y < l_size; l_y++) {
            this.m_columnTable.get(p_x).get(l_y).resetY(l_y);
        }

        this.m_listener.fireDoubleIndexedRemovedListener(l_old);
        return l_old;
    }

    //</editor-fold>
    //<editor-fold desc="IHasDoubleIndexedChangeListener">
    @Override
    public void addChangeListener(
        final IDoubleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.addChangeListener(p_listener);
    }

    @Override
    public void removeChangeListener(
        final IDoubleIndexedChangeListener<E> p_listener)
    {
        this.m_listener.removeChangeListener(p_listener);
    }

    //</editor-fold>
    //<editor-fold desc="IHasTableChangeListener">

    @Override
    public void addDoubleIndexedAddedListener(
        final IDoubleIndexedAddListener<E> p_listener)
    {
        this.m_listener.addDoubleIndexedAddedListener(p_listener);
    }

    @Override
    public void removeDoubleIndexedAddedListener(
        final IDoubleIndexedAddListener<E> p_listener)
    {
        this.m_listener.removeDoubleIndexedAddedListener(p_listener);
    }

    @Override
    public void addDoubleIndexedRemovedListener(
        final IDoubleIndexedRemoveListener<E> p_listener)
    {
        this.m_listener.addDoubleIndexedRemovedListener(p_listener);
    }

    @Override
    public void removeDoubleIndexedRemovedListener(
        final IDoubleIndexedRemoveListener<E> p_listener)
    {
        this.m_listener.removeDoubleIndexedRemovedListener(p_listener);
    }

    @Override
    public void addColumnAddedListener(final IListAddListener<E> p_listener)
    {
        this.m_listener.addColumnAddedListener(p_listener);
    }

    @Override
    public void removeColumnAddedListener(
        final IListAddListener<E> p_listener)
    {
        this.m_listener.removeColumnAddedListener(p_listener);
    }

    @Override
    public void addColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_listener.addColumnRemovedListener(p_listener);
    }

    @Override
    public void removeColumnRemovedListener(
        final IListRemoveListener<E> p_listener)
    {
        this.m_listener.removeColumnRemovedListener(p_listener);
    }

    //</editor-fold>
}
