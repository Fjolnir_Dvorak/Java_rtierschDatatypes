/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows

import java.util.*

/**
 * This is a row based access of the cells with an additional row based
 * Information/Validation container inside the single row container.

 * @param <E> The content type of the rows.
</E> */
open class Rows<E>
(
        /**
         * All the rows.  Also accessible over Columns.java
         */
        protected val m_rows: ArrayList<Row<E>>) : IRows<E> {

    override fun getRow(p_row: Int): Row<E> {
        return this.m_rows[p_row]
    }

    override fun size(): Int {
        return this.m_rows.size
    }

    /**
     * Dangerous! This is not intended for use, but visible for extensibility.
     * Please do not use, use the functionality provided in InformationList
     * instead. There is no functionality like package-private and protected
     * and protected-package private, so to keep it simple: it is public.
     * TODO could be solved via a wrapper object shadowing the container.
     *
     *
     * Adds a new row to the row container

     * @param p_index The index where to insert the new row.
     * *
     * @param p_row The new row.
     */
    fun addRowIntern(p_index: Int,
                     p_row: Row<E>) {
        this.m_rows.add(p_index, p_row)
    }

    /**
     * Dangerous! This is not intended for use, but visible for extensibility.
     * Please do not use, use the functionality provided in InformationList
     * instead. There is no functionality like package-private and protected
     * and protected-package private, so to keep it simple: it is public.
     * TODO could be solved via a wrapper object shadowing the container.
     *
     *
     * Removes a row from the row container.

     * @param p_index the index of the row that will be removed.
     */
    fun removeRowIntern(p_index: Int) {
        this.m_rows.removeAt(p_index)
    }

    override fun iterator(): Iterator<IRow<E>> {
        return RowsIterator(this)
    }

    /**
     * Do never make this private! I had to handle private iterators when I
     * wanted to extend the functionality of an ArrayList. Never make
     * anything private!
     * This is the iteration manager for the rows in the row-container.
     * Please use a ForI whenever possible.
     */
    protected open inner class RowsIterator
    (
            /**
             * Parent the rows of this iterator belongs to.
             * Needed for sizeModification checks.
             */
            protected val m_iterateThrough: Rows<E>) : Iterator<IRow<E>> {
        /**
         * The size of Rows<E> this iterator was created with. If this
         * diverges with the actual size a ConcurrentModificationException
         * will be thrown. It is ugly, yes, very ugly. But it is kind of
         * standard. Better would be to let the user decide on how he want to
         * react to changes.
        </E> */
        protected val m_initSize: Int = m_iterateThrough.size()
        /**
         * Iterator counter. Saves the actual position.
         */
        protected var m_counter: Int = 0

        override fun hasNext(): Boolean {
            this.isModified()
            return this.m_counter < this.m_initSize
        }

        override fun next(): IRow<E> {
            this.isModified()
            if (!this.hasNext()) {
                throw IndexOutOfBoundsException("Index has grown to big.")
            }
            this.m_counter++
            return this.m_iterateThrough.getRow(this.m_counter - 1)
        }

        /**
         * Hmm, this is very ugly. Default size check for concurrent
         * modification. But concurrency is a really beautiful thing. Need to
         * find a beatiful nice way to enable concurrentModifucation.
         * TODO make concurrentModification work.
         */
        protected fun isModified() {
            if (this.m_iterateThrough.size() != this.m_initSize) {
                throw ConcurrentModificationException(
                        "The size of the " + "list is no " + "longer valid" + ". Do not " + "know where to " + "continue...")
            }
        }
    }
}
