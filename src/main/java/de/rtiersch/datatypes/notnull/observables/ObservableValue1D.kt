/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables

import de.rtiersch.datatypes.notnull.observables.Listener.EListener
import de.rtiersch.datatypes.notnull.observables.Listener.ISingleIndexedChangeListener
import de.rtiersch.datatypes.notnull.observables.Listener.ListenerContainer
import de.rtiersch.datatypes.notnull.shared.IntegerContainer
import de.rtiersch.util.annotation.API

/**
 * Observable Value with single fixed Indices. Intended for structures like
 * arrays with fixed boundaries.
 *
 * @param E Content type.
 */
class ObservableValue1D<E> : IObservableValue<E, ISingleIndexedChangeListener<E>> {

    /**
     * Contains all listeners. Not initialized listeners are null-values to
     * save RAM. And yes, in classes that can be used by graphical interfaces
     * or in tables effiency is more precious than readability. To help with
     * readability there are comments.
     */
    private val listener: ListenerContainer<E>
    /**
     * The Index of X
     * Warning: Write permissions granted.
     * Interfacing does not provide any safety, only a slightly longer
     * runtime to execute.
     */
    @API
    var indexX: IntegerContainer
        private set

    /**
     * Observed Element.
     */
    override var value: E
        set(newValue) {
            val l_old = field
            if (l_old == newValue) {
                return
            }
            field = newValue
            this.listener.fireChangeListener(this.indexX.int,
                l_old, newValue)
        }

    /**
     * Creates a new observable Value with shared indices.
     *
     * @param x       Shared Index.
     * @param element Element to observe.
     */
    constructor(x: IntegerContainer,
                element: E) {
        this.indexX = x
        this.value = element
        this.listener = ListenerContainer(EListener.SINGLE_INDEXED_CHANGE)
    }

    /**
     * Creates a new observable Value with shared indices.
     *
     * @param x        Shared Index.
     * @param element  Element to observe.
     * @param listener Shared Listener Container.
     */
    constructor(x: IntegerContainer,
                element: E,
                listener: ListenerContainer<E>) {
        this.indexX = x
        this.value = element
        this.listener = listener
    }

    /**
     * Resets the index to p_newX.
     *
     * @param p_newX new Index for X.
     */
    @API
    fun resetX(p_newX: Int) {
        this.indexX.int = p_newX
    }

    /**
     * Adds a changeListener. If added more than once, it will be added more
     * than once and called more than once.
     *
     * @param p_listener Listener to be added.
     */
    override fun addListener(p_listener: ISingleIndexedChangeListener<E>) {
        this.listener.addChangeListener(p_listener)
    }

    /**
     * Removes the previously added ChangeListener. If added more than once,
     * only the first occurance will be removed.
     *
     * @param p_listener Listener to be removed.
     */
    override fun removeListener(p_listener: ISingleIndexedChangeListener<E>) {
        this.listener.addChangeListener(p_listener)
    }
}
