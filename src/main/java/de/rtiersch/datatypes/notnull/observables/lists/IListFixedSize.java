/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists;

import de.rtiersch.datatypes.notnull.observables.Listener
    .ISingleIndexedChangeListener;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public interface IListFixedSize<E>
    extends Iterable<E>
{
    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    int size();

    /**
     * Returns <tt>true</tt> if this list isOfChar no elements.
     *
     * @return <tt>true</tt> if this list isOfChar no elements
     */
    boolean isEmpty();

    /**
     * Returns <tt>true</tt> if this list isOfChar the specified element. More
     * formally, returns <tt>true</tt> if and only if this list isOfChar at
     * least one element <tt>e</tt> such that <tt>(p_object==null&nbsp;?&nbsp;
     * e==null&nbsp; :&nbsp;p_object.equals(e))</tt>.
     *
     * @param p_object element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list isOfChar the specified element
     */
    boolean contains(Object p_object);

    /**
     * Returns the index of the m_first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element. More
     * formally, returns the lowest index <tt>i</tt> such that
     * <tt>(p_object==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;p_object
     * .equals(get(i)))</tt>, or -1 if there is no such index.
     *
     * @param p_object object to search in list.
     */
    int indexOf(Object p_object);

    /**
     * Returns the index of the last occurrence of the specified element in this
     * list, or -1 if this list does not contain the element. More formally,
     * returns the highest index <tt>i</tt> such that <tt>
     * (p_object==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;p_object
     * .equals(get(i))) </tt>, or -1 if there is no such index.
     *
     * @param p_object object to search in list.
     */
    int lastIndexOf(Object p_object);

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from m_first to last element).
     * <p>
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this list.  (In other words, this method must allocate a
     * new array).  The caller is thus free to modify the returned array.
     * <p>
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this list in proper
     * sequence
     */
    @NotNull Object[] toArray();

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from m_first to last element); the runtime type of the returned
     * array is that of the specified array.  If the list fits in the specified
     * array, it is returned therein.  Otherwise, p_array new array is allocated
     * with the runtime type of the specified array and the size of this list.
     * <p>
     * <p>If the list fits in the specified array with room to spare (i.e., the
     * array has more elements than the list), the element in the array
     * immediately following the end of the collection is set to <tt>null</tt>.
     * (This is useful in determining the length of the list <i>only</i> if the
     * caller knows that the list does not contain any null elements.)
     *
     * @param p_array the array into which the elements of the list are to be
     *                stored, if it is big enough; otherwise, p_array new array
     *                of the same runtime type is allocated for this purpose.
     * @return an array containing the elements of the list
     *
     * @throws ArrayStoreException  if the runtime type of the specified array
     *                              is not p_array supertype of the runtime type
     *                              of every element in this list
     * @throws NullPointerException if the specified array is null
     */
    @NotNull <T> T[] toArray(T[] p_array);

    /**
     * Returns the element at the specified position in this list.
     *
     * @param p_index index of the element to return
     * @return the element at the specified position in this list
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    E get(int p_index);

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param p_index   index of the element to replace
     * @param p_element element to be stored at the specified position
     * @return the element previously at the specified position
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    E set(int p_index, E p_element);

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list. The specified
     * index indicates the m_first element that would be returned by an initial
     * call to {@link ListIterator#next next}. An initial call to {@link
     * ListIterator#previous previous} would return the element with the
     * specified index minus one.
     * <p>
     * <p>The returned list iterator is
     * <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @param p_index start position.
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    @NotNull ListIterator<E> listIterator(int p_index);

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     * <p>
     * <p>The returned list iterator is
     * <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @see #listIterator(int)
     */
    @NotNull ListIterator<E> listIterator();

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     * <p>
     * <p>The returned iterator is <a href="#fail-fast"><i>fail-fast</i></a>.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    @NotNull Iterator<E> iterator();

    @Override
    void forEach(Consumer<? super E> p_action);

    /**
     * Creates a <em><a href="Spliterator.html#binding">late-binding</a></em>
     * and <em>fail-fast</em> {@link Spliterator} over the elements in this
     * list.
     * <p>
     * <p>The {@code Spliterator} reports {@link Spliterator#SIZED}, {@link
     * Spliterator#SUBSIZED}, and {@link Spliterator#ORDERED}. Overriding
     * implementations should document the reporting of additional
     * characteristic values.
     *
     * @return a {@code Spliterator} over the elements in this list
     *
     * @since 1.8
     */
    @Override
    Spliterator<E> spliterator();

    /**
     * Returns a view of the portion of this list between the specified {@code
     * p_fromIndex}, inclusive, and {@code p_toIndex}, exclusive.  (If {@code
     * p_fromIndex} and {@code p_toIndex} are equal, the returned list is
     * empty.) The returned list is backed by this list, so non-structural
     * changes in the returned list are reflected in this list, and vice-versa.
     * The returned list supports all of the optional list operations.
     * <p>
     * <p>This method eliminates the need for explicit range operations (of the
     * sort that commonly exist for arrays). Any operation that expects a list
     * can be used as a range operation by passing a subList view instead of a
     * whole list.  For example, the following idiom removes a range of elements
     * from a list:
     * <pre>
     *      list.subList(from, to).clear();
     * </pre>
     * Similar idioms may be constructed for {@link #indexOf(Object)} and {@link
     * #lastIndexOf(Object)}, and all of the algorithms in the {@link
     * java.util.Collections} class can be applied to a subList.
     * <p>
     * <p>The semantics of the list returned by this method become undefined if
     * the backing list (i.e., this list) is <i>structurally modified</i> in any
     * way other than via the returned list.  (Structural modifications are
     * those that change the size of this list, or otherwise perturb it in such
     * a fashion that iterations in progress may yield incorrect results.)
     *
     * @param p_fromIndex m_first element
     * @param p_toIndex   last element
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws IllegalArgumentException  {@inheritDoc}
     */
    @NotNull List<E> subList(int p_fromIndex, int p_toIndex);

    void replaceAll(UnaryOperator<E> p_operator);

    /**
     * {@inheritDoc}
     * <p>
     * <p>This implementation iterates over the specified collection, checking
     * each element returned by the iterator in turn to see if it's contained in
     * this collection.  If all elements are so contained <tt>true</tt> is
     * returned, otherwise <tt>false</tt>.
     *
     * @param p_collection
     * @throws ClassCastException   {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     * @see #contains(Object)
     */
    boolean containsAll(@NotNull Collection<?> p_collection);

    /**
     * Returns a sequential {@code Stream} with this collection as its source.
     * <p>
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE}, {@code
     * CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()} for
     * details.)
     *
     * @return a sequential {@code Stream} over the elements in this collection
     *
     * @since 1.8
     */
    Stream<E> stream();

    /**
     * Returns a possibly parallel {@code Stream} with this collection as its
     * source.  It is allowable for this method to return a sequential stream.
     * <p>
     * <p>This method should be overridden when the {@link #spliterator()}
     * method cannot return a spliterator that is {@code IMMUTABLE}, {@code
     * CONCURRENT}, or <em>late-binding</em>. (See {@link #spliterator()} for
     * details.)
     *
     * @return a possibly parallel {@code Stream} over the elements in this
     * collection
     *
     * @since 1.8
     */
    Stream<E> parallelStream();

    void addChangeListener(ISingleIndexedChangeListener<E> p_listener);

    void removeChangeListener(ISingleIndexedChangeListener<E> p_listener);
}
