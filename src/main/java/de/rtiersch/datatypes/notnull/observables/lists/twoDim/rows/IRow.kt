/*******************************************************************************
 * Copyright (c) 2017 by Raphael.
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.observables.lists.twoDim.rows

import de.rtiersch.datatypes.notnull.observables.lists.twoDim.header.IHeader
import de.rtiersch.datatypes.notnull.observables.lists.twoDim.cell.ICell
import de.rtiersch.datatypes.notnull.shared.IIntegerRead

/**
 * Access interface without public private methods like lazy constructors. Do
 * not dare do ever use package privacy to prevent that!!!

 * @param <E> The content type of the row.
</E> */
interface IRow<E> : Iterable<ICell<E>>, IHeader {
    /**
     * Returns the cell on the position p_column.

     * @param p_column Position of the wished Cell.
     * *
     * @return Cell on position p_column.
     */
    fun getCell(p_column: Int): ICell<E>

    /**
     * Returns the Row Information and validation container.

     * @return Row information.
     */
    val info: IInfoRowRead<E>

    /**
     * Returns the row index.

     * @return Index of the row.
     */
    val index: IIntegerRead

    /**
     * Returns the size of the row, alias the amount of columns.

     * @return Cells inside the row.
     */
    fun size(): Int
}
