/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.container

import de.rtiersch.datatypes.enums.EOrientation
import de.rtiersch.datatypes.enums.EOrientation.ColumnRow
import de.rtiersch.datatypes.enums.EOrientation.RowColumn
import de.rtiersch.datatypes.notnull.shared.IIntegerWrite
import de.rtiersch.util.annotation.API

/**
 * The same as IIndexedSetDoubleGet, but with other index names.
 * Contains two indices, row and column.
 */
@API
interface IIndexRowColumnSet: IIndexRowColumnChange {
    /**
     * Column index container.
     */
    override var indexColumn: IIntegerWrite
    /**
     * Row index container.
     */
    override var indexRow: IIntegerWrite

    /**
     * Sets a new IIntegerWrite as indices.
     * @param row New row index.
     * @param column New column index.
     */
    @API
    fun setIndices(row: IIntegerWrite,
                   column: IIntegerWrite) {
        this.indexRow = row
        this.indexColumn = column
    }

    /**
     * Sets a new IIntegerWrite as indices.
     * @param first New index for first defined in orientation.
     * @param second New index for second defined in orientation
     * @param orientation Orientation of first and second.
     */
    @API
    fun setIndices(first: IIntegerWrite,
                   second: IIntegerWrite,
                   orientation: EOrientation) {
        when(orientation) {
            RowColumn -> {
                this.indexRow = first
                this.indexColumn = second
            }
            ColumnRow -> {
                this.indexColumn = first
                this.indexRow = second
            }
        }
    }
}
