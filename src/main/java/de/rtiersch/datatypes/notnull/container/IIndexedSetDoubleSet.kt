/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.container

import de.rtiersch.datatypes.enums.EDirection
import de.rtiersch.datatypes.enums.EDirection.X
import de.rtiersch.datatypes.enums.EDirection.Y
import de.rtiersch.datatypes.notnull.shared.IIntegerWrite
import de.rtiersch.util.annotation.API

/**
 * A doubled IndexSet for anything or for nothing.
 */
@API
interface IIndexedSetDoubleSet : IIndexedSetDoubleChange, IIndexedSetSingleSet {
    /**
     * Second Index.
     */
    override var y: IIntegerWrite

    /**
     * Set both indices at the same time knowing which is which.
     * @param x The index called x.
     * @param y The index called y.
     */
    @API
    fun setXY(x: IIntegerWrite,
              y: IIntegerWrite) {
        this.x = x
        this.y = y
    }

    /**
     * Set both indices at the same time knowing which is which.
     * @param first The index defined with directionFirst.
     * @param second The index not defined with directionFirst.
     * @param directionFirst the first Index.
     */
    @API
    fun setIndices(first: IIntegerWrite,
                   second: IIntegerWrite,
                   directionFirst: EDirection) {
        when (directionFirst) {
            X -> {
                this.x = first
                this.y = second
            }
            Y -> {
                this.x = second
                this.y = first
            }
        }
    }
}
