/******************************************************************************
 * Copyright (c) 2017 by Raphael.                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            *
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            *
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                   *
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT                *
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,               *
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING               *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE                 *
 * OR OTHER DEALINGS IN THE SOFTWARE.                                         *
 ******************************************************************************/

package de.rtiersch.datatypes.notnull.container

import de.rtiersch.datatypes.enums.EDirection
import de.rtiersch.datatypes.enums.EDirection.X
import de.rtiersch.datatypes.enums.EDirection.Y
import de.rtiersch.datatypes.notnull.shared.IIntegerWrite
import de.rtiersch.util.annotation.API

/**
 * A doubled IndexSet for anything or for nothing.
 */
@API
interface IIndexedSetDoubleChange : IIndexedSetDoubleGet, IIndexedSetSingleChange {
    override val y: IIntegerWrite
    /**
     * Changes the value of the IntegerContainer.
     */
    @API
    fun changeY(y: Int) {
        this.y.int = y
    }

    /**
     * Changes the value of the specified IntegerContainer.
     */
    @API
    fun changeVar(value: Int,
                  index: EDirection) {
        when (index) {
            X -> x.int = value
            Y -> y.int = value
        }
    }
}
